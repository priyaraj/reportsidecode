<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | customer Information</title>

<script type="text/javascript" src="js/excellentexport.js"></script>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<body>
	
 <form name = "Provisional" action="WriteToXml" method="post" >
 <input type="hidden" name="Provisional" value="Provisional"/>
	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Equipment Provisioning Form</h1>	   	
       <hr>
       <!--  <table id="parent"> -->
        <table  id="provisiontable" style="width:100%;">
        <tr>
   		 <td id="align_new"><h4> Equipment Name</h4></td>
    	<td  align="left" id="EquipmentName" name="EquipmentName"><select name="EquipmentName">
    		 <option value="Equipment 1">Equipment 1</option>
    		 <option value="Equipment 2">Equipment 2</option>
     		</select>
     	</td>
      </tr> 
      <tr>
  <td id="align_new"><h4>Store Mileage</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="StoreMileage" id="StoreMileage"/></td>
  </tr>
  <tr>
  <td id="align_new"><h4>Set Alarm/Notification</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Alarm" id="Alarm"/></td>
  </tr>
  <tr>
  <td id="align_new"><h4>Set location & Geofence</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Location" id="Location"/></td>
  </tr>
   <td id="align_new"><h4>Set On/Off time</h4></td>
    	<td align="left" id="ONOFFtime"><select name="TimeLimit">
     <option value="24 hours">24 hours</option>
     <option value="8 hours/day">8 hours/day</option>
     <option value="Weekdays only">Weekdays only</option>
     <option value="Weekend only">Weekend only</option>
     <option value="24 hours">4 hours/day</option>
     <option value="no limit">no limit</option>
     <option value="monthly anytime">monthly anytime</option>
     		</select>
     	</td>
      </tr> 
      
   <tr>
  <td id="align_new"><h4>Off Time</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="OffTime" id="parent"/></td>
  </tr>         
     <tr>
  <td id="align_new"><h4>On Time</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="OnTime" id="parent"/></td>
  </tr> 
  <tr>
   		 <td id="align_new"><h4> Automatic On/Off </h4></td>
    	<td id="parent" align="left" id="resize"><select name="AutomaticONOFF">
    		 <option value="Yes">Yes</option>
    		 <option value="No">No</option>
     		</select>
     	</td>
      </tr> 
      <tr>
   		 <td id="align_new"><h4> Notification to customer </h4></td>
    	<td id="parent" align="left" id="resize"><select name="Notification">
    		 <option value="Yes">Yes</option>
    		 <option value="No">No</option>
     		</select>
     	</td>
      </tr> 
       <tr>
  <td id="align_new"><h4>Notification to our person</h4></td>
<td id="parent" align="left" id="resize"><select name="Notificationmode">
    		 <option value="email address">email address</option>
    		 <option value="Phone number">Phone number</option>
     		</select>
     	</td>
  </tr>
  <tr>
  <td id="align_new"><h4>Additional notifications</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Addnotifications" id="Addnotifications"/></td>
  </tr>
  <tr>
   		 <td id="align_new"><h4>Usage Reports </h4></td>
    	<td id="parent" align="left" id="resize"><select name="Usageofreport">
    		 <option value="Yes">Yes</option>
    		 <option value="No">No</option>
     		</select>
     	</td>
      </tr> 
     <!--  </table>  -->
      
    <!--  <table style="width:80%;"> -->
  <tr>
            <td><input name="Signin" type="submit" value="Provision"  class="sgncls_12" /></td>
            <td> <input name="Signin" type="submit" value="cancel" class="sgncls_2" /></td>
            </tr>
<!--    </table>  -->
    </table>
    
     </form>
</body>
</html>