<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | customer Information</title>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script type="text/javascript" src="js/excellentexport.js"></script>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"  />


</head>
<script>

$(document).ready(function() { 
	var onofftime = document.getElementById("TimeLimit").value; 
	var OffTime = document.getElementById("OffTime").value;
	var OnTime = document.getElementById("OnTime").value;
	 alert("onofftime"+onofftime);
	 if(onofftime=="24 hours" || onofftime=="8 hours/day" || onofftime=="4 hours/day" ){
				$('#OffTime').datetimepicker({
					dayOfWeekStart : 1,
					lang:'en',
					startDate:	new Date(),
					format:'d/m/Y H:i',
					/* yearRange: "0:+10",
					yearStart: 0,
					yearEnd: 2050,
					monthStart: 0,
					monthEnd: 11,
					yearOffset: 0, */
					yearStart: new Date(),
					monthStart: new Date(),
					minDate: new Date(),
					maxDate: new Date(),
					changeYear: false,
					scrollMonth:false,
					scrollTime:false,
					scrollInput:false
					});
				$('#OnTime').datetimepicker({
					dayOfWeekStart : 1,
					lang:'en',
					startDate:	new Date(),
					format:'d/m/Y H:i',
					yearStart: new Date(),
					monthStart: new Date(),
					minDate: new Date(),
					maxDate: new Date(),
					scrollMonth:false,
					scrollTime:false,
					scrollInput:false
					});
				$('#img').click(function(){$('#OffTime').datetimepicker('show');
				});
				$('#img1').click(function(){$('#OnTime').datetimepicker('show');
				});
	 }
	 
	 else if(onofftime=="Weekdays only"){
		 function DisableWeekDays(date) {

			 var weekenddate = $.OffTime.noWeekends(date);

			// In order to disable weekdays, we will invert the value returned by noWeekends functions.
			// noWeekends return an array with the first element being true/false.. So we will invert the first element

			 var disableweek = [!weekenddate[0]]; 
			 return disableweek;
			}
		 function onlyWeekends(date) {
		      var day = date.getDay();
		      return [(day == 0 || day == 6), ''];
		}
			$('#OffTime').datetimepicker({
				dayOfWeekStart : 1,
				lang:'en',
				startDate:	new Date(),
				format:'d/m/Y H:i',
				/* yearRange: "0:+10",
				yearStart: 0,
				yearEnd: 2050,
				monthStart: 0,
				monthEnd: 11,
				yearOffset: 0, */
				minDate: 0,
				scrollMonth:false,
				scrollTime:false,
				scrollInput:false,
				beforeShowDay: onlyWeekends
				/* beforeShowDay:
				     function(dt)
				     {
				        return [dt.getDay() == 0 || dt.getDay() == 6, ""];
				     } */
				
			});
			$('#OnTime').datetimepicker({
				dayOfWeekStart : 1,
				lang:'en',
				startDate:	new Date(),
				format:'d/m/Y H:i',
				minDate: 0,
				scrollMonth:false,
				scrollTime:false,
				scrollInput:false,
				onGenerate:function( ct ){
					$(this).find('.xdsoft_date.xdsoft_weekend')
						.addClass('xdsoft_disabled');
				}
				});
			$('#img').click(function(){$('#OffTime').datetimepicker('show');
			});
			$('#img1').click(function(){$('#OnTime').datetimepicker('show');
			});
		}
	 else if(onofftime=="Weekend only"){
		 
			$('#OffTime').datetimepicker({
				dayOfWeekStart : 1,
				lang:'en',
				startDate:	new Date(),
				format:'d/m/Y H:i',
				/* yearRange: "0:+10",
				yearStart: 0,
				yearEnd: 2050,
				monthStart: 0,
				monthEnd: 11,
				yearOffset: 0, */
				minDate: 0,
				scrollMonth:false,
				scrollTime:false,
				scrollInput:false,
				onGenerate:function( ct ){
					$(this).find('.xdsoft_date.xdsoft_weekday')
						.addClass('xdsoft_disabled');
				},
			});
			$('#OnTime').datetimepicker({
				dayOfWeekStart : 1,
				lang:'en',
				startDate:	new Date(),
				format:'d/m/Y H:i',
				minDate: 0,
				scrollMonth:false,
				scrollTime:false,
				scrollInput:false
				});
			$('#img').click(function(){$('#OffTime').datetimepicker('show');
			});
			$('#img1').click(function(){$('#OnTime').datetimepicker('show');
			});
		}

});
function validateForm()
{

var EquipmentName = document.getElementById("Equipmentvalue").value; 
var StoreMileage = document.getElementById("StoreMileage").value;
var Location = document.getElementById("Location").value;	
var onofftime = document.getElementById("TimeLimit").value; 
var OffTime = document.getElementById("OffTime").value;
var OnTime = document.getElementById("OnTime").value;
var AutomaticONOFF = document.getElementById("AutomaticONOFF").value; 
var Notification = document.getElementById("Notification").value;
var Notificationmode = document.getElementById("Notificationmode").value;
var Addnotifications = document.getElementById("Addnotifications").value;
var Usageofreport = document.getElementById("Usageofreport").value;

if (EquipmentName==null || EquipmentName=="")
{
	alert("EquipmentName should not be empty");
	return false;
}

else if (StoreMileage==null || StoreMileage=="")
{
	alert("Store Mileage should not be empty");
	return false;
}
else if (Location==null || Location=="")
{
	alert("Set location & Geofence should not be empty");
	return false;
}else if (onofftime==null || onofftime=="")
{
	alert("Set On/Off time should not be empty");
	return false;
}else if (OffTime==null || OffTime=="")
{
	alert("OffTime should not be empty");
	return false;
}else if (OnTime==null || OnTime=="")
{
	alert("OnTime should not be empty");
	return false;
}else if(OffTime <= OnTime)
{
	alert("OffTime value should be greater than OnTime value");
  	return false;
}
else if (AutomaticONOFF==null || AutomaticONOFF=="")
{
	alert("Automatic On/Off should not be empty");
	return false;
}else if (Notification==null || Notification=="")
{
	alert("Notification to Customer should not be empty");
	return false;
}else if (Notificationmode==null || Notificationmode=="")
{
	alert("Notification to our person should not be empty");
	return false;
}
else if (Addnotifications==null || Addnotifications=="")
{
	alert("Additional notifications should not be empty");
	return false;
}
else if (Usageofreport==null || Usageofreport=="")
{
	alert("Usage of report should not be empty");
	return false;
}
 
}  



</script>
<%
	String OrderId = request.getParameter("OrderId");
    session.setAttribute("OrderId", OrderId);
	System.out.println("OrderId.."+OrderId);
	
	
%>

<body>
	
  <form name = "Provisional" action="WriteToXml" method="post" >
 <input type="hidden" name="Provisional" value="Provisional"/>
	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Equipment Provisioning Form</h1>	   	
       <hr>
       <!--  <table id="parent"> -->
        <table  id="provisiontable" style="width:100%;border-collapse: collapse;">
        <tr id="equipment">
   		 <td id="align_new"><h4> Equipment Name</h4></td>
    	<td  align="left" id="EquipmentName" name="EquipmentName"><select id="Equipmentvalue" name="EquipmentName">
    		 <option value="Equipment 1">Equipment 1</option>
    		 <option value="Equipment 2">Equipment 2</option>
     		</select>
     	</td>
      </tr> 
      <tr id="mileage">
  <td id="align_new"><h4>Store Mileage</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="StoreMileage" id="StoreMileage"/></td>
  </tr>
  <tr id="alarm">
  <td id="align_new"><h4>Set Alarm/Notification</h4></td>
    <td style="float: left;margin-top: 20px;"><!-- <input type="text"  name="Alarm" id="Alarm"/> --></td> 
  </tr>
  <tr id="alarm">
  <td id="align_new"><h4>Set location & Geofence</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Location" id="Location"/></td>
  </tr>
  <tr id="setonoff">
   <td id="align_new"><h4>Set On/Off time</h4></td>
    	<td align="left" id="ONOFFtime"><select name="TimeLimit" id="TimeLimit">
     <option value="Weekend only">24 hours</option>
     <option value="8 hours/day">8 hours/day</option>
     <option value="Weekdays only">Weekdays only</option>
     <option value="Weekend only">Weekend only</option>
     <option value="24 hours">4 hours/day</option>
     <option value="no limit">no limit</option>
     <option value="monthly anytime">monthly anytime</option>
     		</select>
     	</td>
      </tr> 
      
   <tr id="setonoff">
  <td id="align_new"><h4>Off Time</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text" readonly="readonly" name="OffTime" id="OffTime"/>
    <img id="img" src="images/calendar-icon-mac.jpg" style="vertical-align: middle;float:right;"/></td>
  </tr>         
     <tr id="setonoff">
  <td id="align_new"><h4>On Time</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  readonly="readonly" name="OnTime" id="OnTime"/>
    <img id="img1" src="images/calendar-icon-mac.jpg" style="vertical-align: middle;float:right;"/></td>
  </tr> 
  <tr id="autoonoff" >
   		 <td id="align_new"><h4> Automatic On/Off </h4></td>
    	<td id="parent" align="left" id="resize"><select name="AutomaticONOFF" id="AutomaticONOFF">
    		 <option value="Yes">Yes</option>
    		 <option value="No">No</option>
     		</select>
     	</td>
      </tr> 
      <tr id="notification">
   		 <td id="align_new"><h4> Notification to customer </h4></td>
    	<td id="parent" align="left" id="resize"><select name="Notification" id="Notification">
    		 <option value="Yes">Yes</option>
    		 <option value="No">No</option>
     		</select>
     	</td>
      </tr> 
       <tr id="notification">
  <td id="align_new"><h4>Notification to our person</h4></td>
<td id="parent" align="left" id="resize"><select name="Notificationmode" id="Notificationmode">
    		 <option value="email address">email address</option>
    		 <option value="Phone number">Phone number</option>
     		</select>
     	</td>
  </tr>
  <tr id="notification">
  <td id="align_new"><h4>Additional notifications</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Addnotifications" id="Addnotifications"/></td>
  </tr>
  <tr id="report">
   		 <td id="align_new"><h4>Usage Reports </h4></td>
    	<td id="parent" align="left" id="resize"><select name="Usageofreport" id="Usageofreport">
    		 <option value="Yes">Yes</option>
    		 <option value="No">No</option>
     		</select>
     	</td>
      </tr> 
      
     <!--  </table>  -->
      
    <!--  <table style="width:80%;"> -->
  <tr>
            <td><input name="Signin" type="submit" value="Provision" onclick="return(validateForm());" class="sgncls_12" /></td>
            <td> <input name="Signin" type="submit" value="cancel" class="sgncls_2" /></td>
            </tr>
<!--    </table>  -->
    </table>
    
     </form>
</body>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.full.js"></script>
</html>