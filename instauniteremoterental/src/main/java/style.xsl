<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" >
<xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/">
AssetName,StoreMileage,Location,Usage,OffTime,OnTime,AutomaticONOFF,Notification,Notificationmode,Addnotifications,Usageofreport
<xsl:for-each select="//ROOT">
<xsl:value-of select="concat(AssetName,',',StoreMileage,',',Location,',',Usage,',',OffTime,',',OnTime,',',AutomaticONOFF,',',Notification,',',Notificationmode,',',Addnotifications,',',Usageofreport,'&#xA;')"/>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
