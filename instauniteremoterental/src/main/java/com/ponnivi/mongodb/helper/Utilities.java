package com.ponnivi.mongodb.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.Part;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@WebListener
public class Utilities  {
	public static final String TAG_Login = "Login";
	public static final String TAG_USERNAME = "UserName";
	public static final String TAG_PASSWORD = "Password";
	// Extract image name from content-disposition header of part
	public static String getFileName(Part part) {
		final String partHeader = part.getHeader("content-disposition");
		System.out.println("***** partHeader: " + partHeader);
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim()
						.replace("\"", "");
			}
		}
		return null;
	}
	
	public static long copy(InputStream input, OutputStream output) throws IOException {
	    byte[] buffer = new byte[4096];
	    System.out.println(".... in copy...");
	    long count = 0L;
	    int n = 0;       

	    while (-1 != (n = input.read(buffer))) {
	        output.write(buffer, 0, n);
	        count += n;
	    }
	    return count;
	}
	
	public static String removeAndCreateAptData(String dataFile,String type) {
		File contentRemovedFile = null;
		try {
			File file = new File(dataFile);
			
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			String dataJsonFileName="";
			while ((line = bufferedReader.readLine()) != null) {
				if (line.startsWith("Content-Disposition")) {
					dataJsonFileName = line.substring(line.indexOf("filename=")+10,line.length()-6);
				}
				if ( ( !line.startsWith("Content") && !line.startsWith("Contents") && !line.startsWith("--")) ) {
				stringBuffer.append(line);
				}
			}
			fileReader.close();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
			Calendar calobj = Calendar.getInstance();
			int  month = new Date().getMonth();
			month += 1;
			String fileNameAppender = new Date().getDate() + "_" + month + "_" + calobj.get(Calendar.YEAR) + "_" + new Date().getHours() + "_" + new Date().getMinutes() + "_" + new Date().getSeconds();
			//String dataJsonFileName = "datajson_rotatingassets_" + fileNameAppender + ".json";
			dataJsonFileName = dataJsonFileName + "_" + fileNameAppender + ".json";
			String osName = Utilities.getUserOperatingSystem();    
			if (osName.startsWith("Windows")) {
				if (type.equalsIgnoreCase("json")) {
					//dataFile = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\" + dataJsonFileName;
					//dataFile = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\" + dataJsonFileName;
					dataFile = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\" + dataJsonFileName;
				}
				else {
					//dataFile = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\newimage.jpg";
					dataFile = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\newimage.jpg";
				}
				
	        }
	        else {
	        	if (type.equalsIgnoreCase("json")) {
	        	dataFile = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/" + dataJsonFileName;
	        	}
	        	else {
	        		dataFile = "/home/ubuntu/instaunite/androidapp/telecomassets/images/newimage.jpg";
	        	}
	        }
			//write to file
			contentRemovedFile = new File(dataFile);
		      // creates the file
			if (!contentRemovedFile.exists()) {
				contentRemovedFile.createNewFile();
			}
		      // creates a FileWriter Object
		      FileWriter writer = new FileWriter(contentRemovedFile); 
		      // Writes the content to the file
		      writer.write(stringBuffer.toString()); 
		      writer.flush();
		      writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dataFile;
	}

	public static String getUserOperatingSystem() {
		return System.getProperty("os.name");
	}
	public static String getJsonFileNames(String type) {
		String jsonFileName = "";
		String userOperatingSystem = getUserOperatingSystem();
		if (type.equalsIgnoreCase("meta")) {
			if (userOperatingSystem.startsWith("Windows")) {
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\metajson_rotatingassets.json";	
			}
			else {
			jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/metajson_rotatingassets.json";
			}			
		}
		if (type.equalsIgnoreCase("data")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\datajson_rotatingassets.json";
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\datajson_rotatingassets.json";				
			}
			else {
			jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/datajson_rotatingassets.json";
			}
		}
		if (type.equalsIgnoreCase("login")) {
			if (userOperatingSystem.startsWith("Windows")) {
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\userdetails.json";
			}
			else {
			jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/userdetails.json";
			}
		}
		if (type.equalsIgnoreCase("options_array")) {
			if (userOperatingSystem.startsWith("Windows")) {
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\options_array.json";				
			}
			else {
				jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/options_array.json";
			}
		}
		return jsonFileName;		
	}
	public static String getImageFileName() {
		String imageFileName = "";
		//	imageFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/metajson_rotatingassets.json";
			imageFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\images\\instaunite_logo.png";
		return imageFileName;		
	}
	//Read and parse the json data
	public static String getJsonDetails(String filePath) {
		FileReader reader;
		JSONObject jsonObject = null;
	
		try {
			
			reader = new FileReader(filePath);	 	 
         JSONParser jsonParser = new JSONParser();
         jsonObject = (JSONObject) jsonParser.parse(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         return jsonObject.toString();
	}
	public static boolean validateUserDetails(String JsonFileName,String userEmail,String userPassword) {
		org.json.JSONObject obj;
		org.json.JSONArray m_jArry;
		boolean userExists = false;
		String userCategory = null;
		try {
			obj = new org.json.JSONObject(getJsonDetails(JsonFileName));
			m_jArry = obj.getJSONArray(Utilities.TAG_Login);

			for (int i = 0; i < m_jArry.length(); i++) {

				org.json.JSONObject jo_inside = m_jArry.getJSONObject(i);
				String userName = jo_inside
						.getString(Utilities.TAG_USERNAME);
				String password = jo_inside
						.getString(Utilities.TAG_PASSWORD);
				if (userEmail.equalsIgnoreCase(userName)
						&& userPassword.equalsIgnoreCase(password)) {
					userExists = true;
					return true;
				}
	
	}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
return userExists;

}
}
