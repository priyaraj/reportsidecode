package com.ponnivi.mongodb.model;

public class Orders {

	// id will be used for primary key in MongoDB
	// We could use ObjectId, but I am keeping it
	// independent of MongoDB API classes
	private String orderId;

	private String endCustomerId;
	
	private boolean advancePaymentMade;

	private Double advancePaymentAmount;

	private Double netPaymentOfAllOrderDetails;

	private Double invoiceAmount;
	
	private String billingAddressSameAsEndCustomerAddress;

	private String invoiceAddress1;
	
	private String invoiceAddress2;

	private String invoiceCity;

	private String invoicePostalCode;

	private String orderAttribute1;
	
	private String orderAttribute2;
	
	private String orderAttribute3;
	
	private String orderAttribute4;
	
	private String orderAttribute5;
	
	private String orderAttribute6;
	
	private String orderAttribute7;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getEndCustomerId() {
		return endCustomerId;
	}

	public void setEndCustomerId(String endCustomerId) {
		this.endCustomerId = endCustomerId;
	}

	public boolean isAdvancePaymentMade() {
		return advancePaymentMade;
	}

	public void setAdvancePaymentMade(boolean advancePaymentMade) {
		this.advancePaymentMade = advancePaymentMade;
	}

	public Double getAdvancePaymentAmount() {
		return advancePaymentAmount;
	}

	public void setAdvancePaymentAmount(Double advancePaymentAmount) {
		this.advancePaymentAmount = advancePaymentAmount;
	}

	public Double getNetPaymentOfAllOrderDetails() {
		return netPaymentOfAllOrderDetails;
	}

	public void setNetPaymentOfAllOrderDetails(Double netPaymentOfAllOrderDetails) {
		this.netPaymentOfAllOrderDetails = netPaymentOfAllOrderDetails;
	}

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getBillingAddressSameAsEndCustomerAddress() {
		return billingAddressSameAsEndCustomerAddress;
	}

	public void setBillingAddressSameAsEndCustomerAddress(
			String billingAddressSameAsEndCustomerAddress) {
		this.billingAddressSameAsEndCustomerAddress = billingAddressSameAsEndCustomerAddress;
	}

	public String getInvoiceAddress1() {
		return invoiceAddress1;
	}

	public void setInvoiceAddress1(String invoiceAddress1) {
		this.invoiceAddress1 = invoiceAddress1;
	}

	public String getInvoiceAddress2() {
		return invoiceAddress2;
	}

	public void setInvoiceAddress2(String invoiceAddress2) {
		this.invoiceAddress2 = invoiceAddress2;
	}

	public String getInvoiceCity() {
		return invoiceCity;
	}

	public void setInvoiceCity(String invoiceCity) {
		this.invoiceCity = invoiceCity;
	}

	public String getInvoicePostalCode() {
		return invoicePostalCode;
	}

	public void setInvoicePostalCode(String invoicePostalCode) {
		this.invoicePostalCode = invoicePostalCode;
	}

	public String getOrderAttribute1() {
		return orderAttribute1;
	}

	public void setOrderAttribute1(String orderAttribute1) {
		this.orderAttribute1 = orderAttribute1;
	}

	public String getOrderAttribute2() {
		return orderAttribute2;
	}

	public void setOrderAttribute2(String orderAttribute2) {
		this.orderAttribute2 = orderAttribute2;
	}

	public String getOrderAttribute3() {
		return orderAttribute3;
	}

	public void setOrderAttribute3(String orderAttribute3) {
		this.orderAttribute3 = orderAttribute3;
	}

	public String getOrderAttribute4() {
		return orderAttribute4;
	}

	public void setOrderAttribute4(String orderAttribute4) {
		this.orderAttribute4 = orderAttribute4;
	}

	public String getOrderAttribute5() {
		return orderAttribute5;
	}

	public void setOrderAttribute5(String orderAttribute5) {
		this.orderAttribute5 = orderAttribute5;
	}

	public String getOrderAttribute6() {
		return orderAttribute6;
	}

	public void setOrderAttribute6(String orderAttribute6) {
		this.orderAttribute6 = orderAttribute6;
	}

	public String getOrderAttribute7() {
		return orderAttribute7;
	}

	public void setOrderAttribute7(String orderAttribute7) {
		this.orderAttribute7 = orderAttribute7;
	}	
}
