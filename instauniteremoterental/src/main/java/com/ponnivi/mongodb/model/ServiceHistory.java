package com.ponnivi.mongodb.model;

public class ServiceHistory {

	// id will be used for primary key in MongoDB
	// We could use ObjectId, but I am keeping it
	// independent of MongoDB API classes
	private String id;

	private String history;

	private String additionalinfo;

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getAdditionalinfo() {
		return additionalinfo;
	}

	public void setAdditionalInfo(String additionalinfo) {
		this.additionalinfo = additionalinfo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
