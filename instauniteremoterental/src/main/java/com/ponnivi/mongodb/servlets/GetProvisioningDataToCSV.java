package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;

@WebServlet("/getProvisioningDatatocsv")
public class GetProvisioningDataToCSV extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//response.setContentType("text/csv");
		response.setContentType("text/csv; charset=Cp1252");
		//response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		
		String customerId = (String)request.getParameter("customerId");
		String databaseName = (String) request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		DB ponniviDb = mongo.getDB(databaseName);
		boolean isValid = false;
		List validCustomerIdList = MongoDBDAO.getDistinct(ponniviDb, "customer_definition_collection", "CustomerId");
		for (int i = 0; i < validCustomerIdList.size(); i++) {
			if (validCustomerIdList.get(i).toString().equalsIgnoreCase(customerId)) {
				isValid = true;
				break;
			}
		}
		if (isValid) {
			databaseName = "Ponnivi_"+customerId;
		String orderDetailId = (String)request.getParameter("orderDetailId");
		DB remoteRentalDb = mongo.getDB(databaseName);
		DBCollection collection = remoteRentalDb.getCollection("order_detail");
		response.setDateHeader("Expires", 0);

		StringBuffer fileNameFormat = new StringBuffer();
		fileNameFormat.append( "attachment; filename= Provisioning");
		fileNameFormat.append(".csv");
		response.setHeader("Content-disposition",fileNameFormat.toString());
		
		StringBuffer totalString =new StringBuffer();
		StringBuffer header=new StringBuffer();
		header.append("Header" + "\n");
		header.append("Orderdetailid," + "\t");
		header.append("Orderid,"+ "\t");
		header.append("AssetName,"+ "\t");
		header.append("Quantity,"+ "\t");
		header.append("Usagedescription,"+ "\t");
		header.append("Fromdate,"+ "\t");
		header.append("Enddate,"+ "\t");
		header.append("Noofdays,"+ "\t");
		header.append("Amount"+ "\r");
		header.append("Details");
		
		StringBuffer body=new StringBuffer();

		  List<DBObject> orderDetailsList = MongoDBDAO.getDetails(collection, "orderdetailid", "text", orderDetailId);
		  List<DBObject> readableList = new ArrayList<DBObject>();
		  JSONObject jsonObject = new JSONObject(),wholeJsonObject = new JSONObject();
		  String output = "Orderdetailid, Orderid,AssetName,Quantity,Usagedescription,Fromdate,Enddate,Noofdays,amount\n";
		  StringBuffer sb = new StringBuffer();
		  for (int i = 0; i < orderDetailsList.size();i++) {
				/*jsonObject.put("orderdetailid",orderDetailsList.get(i).get("orderdetailid"));
				jsonObject.put("orderid",orderDetailsList.get(i).get("orderid"));
				jsonObject.put("assetname",orderDetailsList.get(i).get("assetname"));
				jsonObject.put("quantity",orderDetailsList.get(i).get("quantity"));
				jsonObject.put("usagedescription",orderDetailsList.get(i).get("usagedescription"));
				jsonObject.put("fromdate",orderDetailsList.get(i).get("fromdate"));
				jsonObject.put("enddate",orderDetailsList.get(i).get("enddate"));
				jsonObject.put("noofdays",orderDetailsList.get(i).get("noofdays"));
				jsonObject.put("amount",orderDetailsList.get(i).get("amount"));*/
			  
			  //sb.append(output)
			 body.append(orderDetailsList.get(i).get("orderdetailid")+"," + "\t")
			  .append(orderDetailsList.get(i).get("orderid")+"," + "\t")
			  .append(orderDetailsList.get(i).get("assetname")+"," + "\t")
			  .append(orderDetailsList.get(i).get("quantity")+"," + "\t")
			  .append(orderDetailsList.get(i).get("usagedescription")+"," + "\t")
			  .append(orderDetailsList.get(i).get("fromdate")+"," + "\t")
			  .append(orderDetailsList.get(i).get("enddate")+"," + "\t")
			  .append(orderDetailsList.get(i).get("noofdays")+"," + "\t")
			  .append(orderDetailsList.get(i).get("amount"));
			  
		  }
		  totalString.append(header.toString());
		  totalString.append("\n");
		  totalString.append(body.toString());
		  //wholeJsonObject.put("provision", jsonObject);
		  out.write(totalString.toString());
	}	
	else {
		out.println("No data available for " + customerId);
	}
	}
}
