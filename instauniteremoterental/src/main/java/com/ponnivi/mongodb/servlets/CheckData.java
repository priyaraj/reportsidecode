package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;

@WebServlet("/checkdata")
public class CheckData extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		//response.setContentType("application/csv");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		
		String id = (String)request.getParameter("id");
		String databaseName = (String) request.getServletContext()
				.getAttribute("PONNIVI_REMOTERENTAL_DATABASE");
		DB ponniviDb = mongo.getDB(databaseName);
		boolean isValid = false;
		List validRfidList = MongoDBDAO.getDistinct(ponniviDb, "RFID", "RFID_Number");
		for (int i = 0; i < validRfidList.size(); i++) {
			if (validRfidList.get(i).toString().equalsIgnoreCase(id)) {
				isValid = true;
				break;
			}
		}
		if (isValid) {			
		  JSONObject jsonObject = new JSONObject(),wholeJsonObject = new JSONObject();
		  try {
				jsonObject.put("RFID_Number",id);
		  //wholeJsonObject.put("provision", jsonObject);
		  } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		Gson gson = new Gson();
		if (jsonObject != null && jsonObject.length() > 0) { 
        String rfidDetails = gson.toJson(id);
        
        out.println(rfidDetails);
		}
	}	
	else {
		out.println("No data available for " + id);
	}
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		String redirectingJsp = "";
		PrintWriter out = response.getWriter();
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		HttpSession session = request.getSession();
		String id = (String)request.getParameter("id");
		String databaseName = (String) request.getServletContext()
				.getAttribute("PONNIVI_REMOTERENTAL_DATABASE");
		DB ponniviDb = mongo.getDB(databaseName);
		boolean isValid = false;
		List validRfidList = MongoDBDAO.getDistinct(ponniviDb, "RFID", "RFID_Number");
		for (int i = 0; i < validRfidList.size(); i++) {
			if (validRfidList.get(i).toString().equalsIgnoreCase(id)) {
				isValid = true;
				break;
			}
		}
		if (isValid) {			
		  JSONObject jsonObject = new JSONObject(),wholeJsonObject = new JSONObject();
		  try {
				jsonObject.put("RFID_Number",id);
		  //wholeJsonObject.put("provision", jsonObject);
		  } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		Gson gson = new Gson();
		if (jsonObject != null && jsonObject.length() > 0) { 
        String rfidDetails = gson.toJson(id);
        //session.setAttribute("id", id);
        //redirectingJsp = "/checkRFIDOrNFCUsingPost.jsp";
        out.println(rfidDetails);
		}
	}	
	else {
		out.println("No data available for " + id);
	}
	}
}
