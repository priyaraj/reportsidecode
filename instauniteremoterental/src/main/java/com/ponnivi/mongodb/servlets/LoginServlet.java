package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	final static Logger logger = Logger.getLogger(LoginServlet.class);
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String customerId = request.getParameter("customerId");
		logger.debug("customerId.."+customerId);
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String redirectingJsp = "";
		boolean isValid = validateCredentials(customerId,userName, password, request);
		if (isValid) {
			/*String loginJsonFileName = Utilities.getJsonFileNames("login");

			// Validate username and password
			isValid = Utilities.validateUserDetails(loginJsonFileName,
					userName, password);*/
			MongoClient mongo = (MongoClient) request.getServletContext()
					.getAttribute("MONGO_CLIENT");
			String databaseName = (String) request.getServletContext()
					.getAttribute("PONNIVI_DATABASE");
			DB ponniviDb = mongo.getDB(databaseName);
			isValid = false; //Check whether customerId exists in customer_definition_collection
			List validCustomerIdList = MongoDBDAO.getDistinct(ponniviDb, "customer_definition_collection", "CustomerId");
			for (int i = 0; i < validCustomerIdList.size(); i++) {
				if (validCustomerIdList.get(i).toString().equalsIgnoreCase(customerId)) {
					isValid = true;
					break;
				}
			}
			if (isValid) {
				String customerDbName = MongoDBDAO.getCollectionSpecificDetails(ponniviDb,"customer_definition_collection","CustomerId",customerId);
				//request.setAttribute("customerDbName", customerDbName);
				HttpSession session = request.getSession();
				ponniviDb = mongo.getDB(customerDbName);
				session.setAttribute("customerDb", ponniviDb);
				session.setAttribute("customerId", customerId);
				//Get equipment type and equipmentname from Ponnivi_Remote_Rental
				DB remoteRentalDb = mongo.getDB("Ponnivi_RemoteRentaldb");
				session.setAttribute("remoterentaldb", remoteRentalDb);
				DBCollection assetCategoryType = remoteRentalDb.getCollection("Asset_Category_Type");
				List equipmentTypeList = MongoDBDAO.getEquipmentDetails(assetCategoryType,0,"Asset_Category_Id","Asset_Category_Type", "");
				if (equipmentTypeList != null) {
				session.setAttribute("equipmentTypes", equipmentTypeList);
				

				DBCollection assets = remoteRentalDb.getCollection("Assets");
				List assetsList = MongoDBDAO.getEquipmentDetails(assets,1,"Asset_Id","Asset_Name", "Asset_Id");
				if (assetsList != null) {
				session.setAttribute("assetsNames", assetsList);
				List wholeAssetsList = new ArrayList();
				List totalAssetsList = new ArrayList();
				//Get all equipmenttype and their corresponding assets (whole) should be included
				try {
				for (int i = 0; i < equipmentTypeList.size();i++) {
					JSONObject jsonObject = (JSONObject)equipmentTypeList.get(i);
					int categoryId;			
						categoryId = (int)Double.parseDouble(jsonObject.getString("id").toString());
						wholeAssetsList = MongoDBDAO.getEquipmentDetails(assets,categoryId,"Asset_Category_Id","Asset_Name", "Asset_Id");
						if (wholeAssetsList != null && wholeAssetsList.size() > 0) {
						for (int j = 0; j < wholeAssetsList.size();j++) {
							totalAssetsList.add(wholeAssetsList.get(j));
						}
						}
					
				}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				session.setAttribute("wholeassets", totalAssetsList);
				redirectingJsp = "/Subscriber_information.jsp";
				}
				else {
					session.setAttribute("error","Assets are not yet defined properly");
					redirectingJsp = "/nodata.jsp";
				}
				}
				
				else {
					session.setAttribute("error","Asset Categories are not yet defined properly");
					redirectingJsp = "/nodata.jsp";
				}
			} else {
				request.setAttribute("error", "CustomerId does not exist");
				redirectingJsp = "/login.jsp";
			}

		} else {
			redirectingJsp = "/login.jsp"; 
			 
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				  redirectingJsp); 
				  rd.forward(request, response);


	}

	private boolean validateCredentials(String customerId,String userName, String password,
			HttpServletRequest request) {
		if ((customerId == null || customerId.equals(""))) {
			request.setAttribute("error", "CustomerId is Missing");
			return false;
		}
		/*if ((userName == null || userName.equals(""))) {
			request.setAttribute("error", "User Name is Missing");
			return false;
		} else if ((password == null || password.equals(""))) {
			request.setAttribute("error", "Password is Missing");
			return false;
		}*/
		return true;
	}
}
