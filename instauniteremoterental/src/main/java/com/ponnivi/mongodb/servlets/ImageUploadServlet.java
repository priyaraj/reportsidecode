package com.ponnivi.mongodb.servlets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * @author javatutorials.co.in
 */
@MultipartConfig          // indicates that request MIME type is �multipart/form-data�
@WebServlet("/upload")
public class ImageUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String responseMessage = "Image Upload Successfull!!";

		// Get part using HttpServletRequest�s getPart() method
		Part filePart = request.getPart("image");
		// Extract image name from content-disposition header of part
		String imageName = getFileName(filePart);
		String workingDirectory = "/home/instaunite/androidapp/telecomassets/images";
		//String workingDirectory = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\images";

		System.out.println("***** imageName: " + imageName);

		// Copy input file to destination path
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			File outputFilePath = new File(workingDirectory + "/" + imageName);
			System.out.println("***** outputFilePath: " + outputFilePath.getAbsolutePath());			
			inputStream = filePart.getInputStream();
			outputStream = new FileOutputStream(outputFilePath);

			int read = 0;
			final byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

		} catch (FileNotFoundException fne) {
			fne.printStackTrace();
			responseMessage = "Image Upload Failed!!";
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		}

		// create and return response to client web browser
		response.setContentType("text/html;UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write("<html>");
		writer.write("<head>");
		writer.write("<title>Servlet 3.0 File Upload Example </title>");
		writer.write("</head>");
		writer.write("<body>");
		writer.write("<h3>" + responseMessage + "</h3>");
		writer.write("</body>");
		writer.write("</html>");
		writer.close();
	}

	// Extract image name from content-disposition header of part
	private String getFileName(Part part) {
		final String partHeader = part.getHeader("content-disposition");
		System.out.println("***** partHeader: " + partHeader);
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim()
						.replace("\"", "");
			}
		}
		return null;
	}
}

