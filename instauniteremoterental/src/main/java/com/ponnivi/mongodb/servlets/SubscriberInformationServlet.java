package com.ponnivi.mongodb.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.model.SubscriberPersonalDetails;

@WebServlet("/subscriber")
public class SubscriberInformationServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String  customerId = (String)session.getAttribute("customerId");
		String endCustomerName = request.getParameter("Name");
	
		String firstName = request.getParameter("FirstName");		
		
		String email = request.getParameter("Email");	
		
		String phoneNumber = request.getParameter("PhoneNumber");
		String addressLine1 = request.getParameter("AddressLine1");
		String addressLine2 = request.getParameter("AddressLine2");
		String city = request.getParameter("City");
		String postalCode = request.getParameter("PostalCode");
		String otherRemark = request.getParameter("Otherremark");
		String redirectingJsp = "/order_information1.jsp";
		System.out.println("Name.." + endCustomerName);	
		//saving the details under subscriber collection. It is having necessary attributes, but as of now collected information is alone sending in the collection
		SubscriberPersonalDetails subscriber = new SubscriberPersonalDetails();
		subscriber.setOwnerCustomerId(customerId);
		subscriber.setEndCustomerName(endCustomerName);
		subscriber.setEndCustomerFirstName(firstName);
		subscriber.setEndCustomerEmail(email);
		
		subscriber.setEndCustomerPhoneNumber(phoneNumber);
		subscriber.setEndCustomerAddressLine1(addressLine1);
		subscriber.setEndCustomerAddressLine2(addressLine2);
		
		subscriber.setEndCustomerCity(city);
		subscriber.setEndCustomerPostalCode(postalCode);
		subscriber.setEndCustomerOtherRemarks(otherRemark);

		MongoDBDAO.saveSubscriberDetails(subscriber, customerDatabase, session);
		session.setAttribute("newOrder", "yes");
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
