package com.ponnivi.mongodb.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.ponnivi.mongodb.model.SubscriberPersonalDetails;

@WebServlet("/finalorderinformation")
public class FinalOrderInformationServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String submitValue = request.getParameter("submit");
		
		System.out.println("submitValue.."+submitValue);
		HttpSession session = request.getSession();
		long orderCollectionCount = 0;
		String redirectingJsp = "";
		String endCustomerId = (String)session.getAttribute("endCustomerId");
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String orderId = (String)session.getAttribute("orderId");

		//Creating orders collection when initiating for order_detail
		DBCollection ordersCollection = customerDatabase.getCollection("orders");
		BasicDBObject ordersDocument = new BasicDBObject();
		ordersDocument.put("orderid",orderId);
		ordersDocument.put("endcustomerid",endCustomerId);
		String advancepayment = (String)request.getParameter("advancepayment");
		if (advancepayment != null && advancepayment.equalsIgnoreCase("on")) {
			ordersDocument.put("advancepaymentamount",request.getParameter("advancepaymentamount"));	
		}
		else {
			ordersDocument.put("advancepaymentamount",0);
		}

		
		//Get subscriber information details as address details  -- Need to do
		String paymentOnDelivery = (String)request.getParameter("POD");
		String billingAddressDifferent = (String)request.getParameter("check");
		if (paymentOnDelivery != null && paymentOnDelivery.equalsIgnoreCase("on")) {
			ordersDocument.put("paymentOnDelivery", "yes");
		}
		else {
			ordersDocument.put("paymentOnDelivery", "no");  //Here some changes are required
		}
		//If different, getting the address details from form or else getting from subscriber details itself
		SubscriberPersonalDetails subscriberDetails = (SubscriberPersonalDetails)session.getAttribute("subscriberdetails");
		if (billingAddressDifferent != null && billingAddressDifferent.equalsIgnoreCase("on")) {
			ordersDocument.put("deliveryaddress1", request.getParameter("AddressLine1"));
			ordersDocument.put("deliveryaddress2", request.getParameter("AddressLine2"));
			ordersDocument.put("deliverycity", request.getParameter("City"));
			ordersDocument.put("deliverypincode", request.getParameter("Pincode"));
		}
		else {
			
			ordersDocument.put("deliveryaddress1", subscriberDetails.getEndCustomerAddressLine1());
			ordersDocument.put("deliveryaddress2", subscriberDetails.getEndCustomerAddressLine2());
			ordersDocument.put("deliverycity", subscriberDetails.getEndCustomerCity());
			ordersDocument.put("deliverypincode", subscriberDetails.getEndCustomerPostalCode());
		}
		ordersDocument.put("deliveryto",subscriberDetails.getEndCustomerEmail());
		ordersCollection.insert(ordersDocument);
		if (submitValue.equalsIgnoreCase("Submit")) {			
			redirectingJsp = "/thankyou.jsp";
		}
		else if (submitValue.equalsIgnoreCase("Provision Order")) {	
			redirectingJsp = "/Equipment_provisioning.jsp";
		}
		else {
			redirectingJsp = "/revieworder.jsp";
		}
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				  redirectingJsp); 
				  rd.forward(request, response);
		
	}
}
