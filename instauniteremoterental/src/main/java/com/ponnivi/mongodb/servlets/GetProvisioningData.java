package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;

@WebServlet("/getProvisioningData")
public class GetProvisioningData extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		
		String customerId = (String)request.getParameter("customerId");
		String databaseName = (String) request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		DB ponniviDb = mongo.getDB(databaseName);
		boolean isValid = false;
		List validCustomerIdList = MongoDBDAO.getDistinct(ponniviDb, "customer_definition_collection", "CustomerId");
		for (int i = 0; i < validCustomerIdList.size(); i++) {
			if (validCustomerIdList.get(i).toString().equalsIgnoreCase(customerId)) {
				isValid = true;
				break;
			}
		}
		if (isValid) {
			databaseName = "Ponnivi_"+customerId;
		String orderDetailId = (String)request.getParameter("orderDetailId");
		DB remoteRentalDb = mongo.getDB(databaseName);
		DBCollection collection = remoteRentalDb.getCollection("order_detail");
		
		  List<DBObject> orderDetailsList = MongoDBDAO.getDetails(collection, "orderdetailid", "text", orderDetailId);
		  List<DBObject> readableList = new ArrayList<DBObject>();
		  JSONObject jsonObject = new JSONObject(),wholeJsonObject = new JSONObject();
		  try {
		  for (int i = 0; i < orderDetailsList.size();i++) {
				jsonObject.put("orderdetailid",orderDetailsList.get(i).get("orderdetailid"));
				jsonObject.put("orderid",orderDetailsList.get(i).get("orderid"));
				jsonObject.put("assetname",orderDetailsList.get(i).get("assetname"));
				jsonObject.put("quantity",orderDetailsList.get(i).get("quantity"));
				jsonObject.put("usagedescription",orderDetailsList.get(i).get("usagedescription"));
				jsonObject.put("fromdate",orderDetailsList.get(i).get("fromdate"));
				jsonObject.put("enddate",orderDetailsList.get(i).get("enddate"));
				jsonObject.put("noofdays",orderDetailsList.get(i).get("noofdays"));
				jsonObject.put("amount",orderDetailsList.get(i).get("amount"));
		  }
		  //wholeJsonObject.put("provision", jsonObject);
		  } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		Gson gson = new Gson();
		if (jsonObject != null && jsonObject.length() > 0) { 
        String provisionalDetails = gson.toJson(jsonObject);
        
        out.println(provisionalDetails);
		}
		else {
			out.println("No data available for " + orderDetailId);
		}
	}	
	else {
		out.println("No data available for " + customerId);
	}
	}
}
