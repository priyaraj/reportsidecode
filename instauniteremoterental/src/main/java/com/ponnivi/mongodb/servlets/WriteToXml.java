package com.ponnivi.mongodb.servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;




/**
 * Servlet implementation class WriteToXml
 */
@WebServlet("/WriteToXml")
public class WriteToXml extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(WriteToXml.class);
	static String XmlFileName="", osName="",dataFileName="", csvFileName="";
	OutputStream out;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String redirectingJsp = "";
		HttpSession session = request.getSession(true);
		String orderDetailId = (String) session.getAttribute("orderDetailId");
		
		
		String Formname = request.getParameter("Provisional");
		//String EquipmentName = (String)request.getParameter("EquipmentName");
		String AssetName = (String)session.getAttribute("assetName");
		String StoreMileage = (String)request.getParameter("StoreMileage");
		String Alarm = (String)request.getParameter("Alarm");
		String Location = (String)request.getParameter("Location");
		//String TimeLimit = (String)request.getParameter("TimeLimit");
		String Usage = (String)session.getAttribute("usage");
		String OffTime = (String)request.getParameter("OffTime");
		String OnTime = (String)request.getParameter("OnTime");
		String AutomaticONOFF = (String)request.getParameter("AutomaticONOFF");
		String Notification = (String)request.getParameter("Notification");
		String Notificationmode = (String)request.getParameter("Notificationmode");
		String Addnotifications = (String)request.getParameter("Addnotifications");
		String Usageofreport = (String)request.getParameter("Usageofreport");
		
		
		System.out.println(orderDetailId);
		
		
		
		try {
			// Get Jsp Elements
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element rootElement = doc.createElement("ROOT");
			doc.appendChild(rootElement);
			
			
			Element equipmentName = doc.createElement("AssetName");
			equipmentName.appendChild(doc.createTextNode(AssetName));
			rootElement.appendChild(equipmentName);
			
			Element mileage = doc.createElement("StoreMileage");
			mileage.appendChild(doc.createTextNode(StoreMileage));
			rootElement.appendChild(mileage);
			
			/*Element alarm = doc.createElement("Alarm");
			alarm.appendChild(doc.createTextNode(Alarm));
			rootElement.appendChild(alarm);*/
			
			Element location = doc.createElement("Location");
			location.appendChild(doc.createTextNode(Location));
			rootElement.appendChild(location);
			
			Element usageLimit = doc.createElement("Usage");
			usageLimit.appendChild(doc.createTextNode(Usage));
			rootElement.appendChild(usageLimit);
			
			Element offtime = doc.createElement("OffTime");
			offtime.appendChild(doc.createTextNode(OffTime));
			rootElement.appendChild(offtime);
			
			Element ontime = doc.createElement("OnTime");
			ontime.appendChild(doc.createTextNode(OnTime));
			rootElement.appendChild(ontime);
			
			Element automaticONOFF = doc.createElement("AutomaticONOFF");
			automaticONOFF.appendChild(doc.createTextNode(AutomaticONOFF));
			rootElement.appendChild(automaticONOFF);
			
			Element notification = doc.createElement("Notification");
			notification.appendChild(doc.createTextNode(Notification));
			rootElement.appendChild(notification);
			
			Element notificationmode = doc.createElement("Notificationmode");
			notificationmode.appendChild(doc.createTextNode(Notificationmode));
			rootElement.appendChild(notificationmode);
			
			Element addnotifications = doc.createElement("Addnotifications");
			addnotifications.appendChild(doc.createTextNode(Addnotifications));
			rootElement.appendChild(addnotifications);
			
			Element usageofreport = doc.createElement("Usageofreport");
			usageofreport.appendChild(doc.createTextNode(Usageofreport));
			rootElement.appendChild(usageofreport);
			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			

			
			
			
			
	        InputStream in = request.getInputStream();
	        osName = getUserOperatingSystem();   
	        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
			Calendar calobj = Calendar.getInstance();
			int  month = new Date().getMonth();
			month += 1;
			String fileNameAppender = new Date().getDate() + "_" + month + "_" + calobj.get(Calendar.YEAR) + "_" + new Date().getHours() + "_" + new Date().getMinutes() + "_" + new Date().getSeconds();

	        dataFileName = orderDetailId + "_xmlfile_" + fileNameAppender + ".xml";
	        File folder = new File("/home/ubuntu/instaunite/serverside/Remote_Rental_Assets/" + orderDetailId );
	        
	        if (folder.exists() && folder.isDirectory()) {
	        } else {
	            folder.mkdir();
	        }
	        System.out.println(folder);
	        String filePath = folder + File.separator;
	        System.out.println(filePath);
	        
	        File folder1 = new File(folder + "/xmls" );
	        if (folder1.exists() && folder1.isDirectory()) {
	        } else {
	            folder1.mkdir();
	        }
	        System.out.println(folder);
	        String filePath1 = folder1 + File.separator;
	        System.out.println(filePath1);
	        
     		logger.debug("folder : " + folder);
     		
	        
	        
	        
	        File file = new File(filePath1 + dataFileName);
	        System.out.println(file);
	        
	        if (osName.startsWith("Windows")) {
	        	//dataJsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\" + dataFileName;
	        	//XmlFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\" + dataFileName;
	        	//dataFileName = XmlFileName;
	        	StreamResult result = new StreamResult(file);
		        transformer.transform(source, result);
	        	System.out.println("File saved!");
	        	System.out.println(dataFileName);
	        	 Xml2Csv(dataFileName,fileNameAppender,Formname,filePath1,orderDetailId);
	        }
	        else {
	        	StreamResult result = new StreamResult(file);
		        transformer.transform(source, result);
	        	System.out.println("File saved!");
	        	System.out.println(dataFileName);
	        	 Xml2Csv(dataFileName,fileNameAppender,Formname,filePath1,orderDetailId);
	        	//XmlFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/" + dataFileName;
	        	
	        	// dataFileName = XmlFileName;
	        	 
	        }
	       
	        
			
			
		}
		
				  
		 catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			  } catch (TransformerException tfe) {
				tfe.printStackTrace();
			  } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		ServletContext context = getServletContext();
        RequestDispatcher dispatcher = context.getRequestDispatcher("/submit.jsp");
        dispatcher.forward(request,response);
        
		
	
	
	}
/*	public static long copy(InputStream input, OutputStream output) throws IOException {
	    byte[] buffer = new byte[4096];
	    System.out.println(".... in copy...");
	    long count = 0L;
	    int n = 0;       

	    while (-1 != (n = input.read(buffer))) {
	        output.write(buffer, 0, n);
	        count += n;
	    }
	    return count;
	}*/
	public static String getUserOperatingSystem() {
		return System.getProperty("os.name");
	}
	public static void Xml2Csv(String dataFileName, String fileNameAppender, String Formname, String filePath1, String orderDetailId) throws Exception {
		
		File stylesheet = null;
		if (Formname.equalsIgnoreCase("Provisional")) {
			File folder = new File("/home/ubuntu/instaunite/serverside/Remote_Rental_Assets/provisioning/xsl/");
	        if (folder.exists() && folder.isDirectory()) {
	        } else {
	            folder.mkdir();
	        }
	        String filePath = folder + File.separator;
	        stylesheet = new File(filePath + "style.xsl");
		
		}
        File xmlSource = new File(filePath1 + dataFileName);

        csvFileName = orderDetailId + "_csvfile_" + fileNameAppender + ".csv" ;
        
        File folderid = new File("/home/ubuntu/instaunite/serverside/Remote_Rental_Assets/" + orderDetailId);
        if (folderid.exists() && folderid.isDirectory()) {
        } else {
            folderid.mkdir();
        }
        System.out.println(folderid);
        String filePath = folderid + File.separator;
        System.out.println(filePath);
        
        File foldercsv = new File(folderid + "/csvs" );
        if (foldercsv.exists() && foldercsv.isDirectory()) {
        } else {
            foldercsv.mkdir();
        }
        System.out.println(foldercsv);
        String filePathcsv = foldercsv + File.separator;
        System.out.println(filePathcsv);
        
        

        
        File file = new File(filePathcsv + csvFileName);
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(xmlSource);

        
		StreamSource stylesource = new StreamSource(stylesheet);
        Transformer transformer = TransformerFactory.newInstance()
                .newTransformer(stylesource);
        Source source = new DOMSource(document);
        Result outputTarget = new StreamResult(file);
        transformer.transform(source, outputTarget);
    	System.out.println("File saved as CSV!");
    	System.out.println(csvFileName);
    }
}



