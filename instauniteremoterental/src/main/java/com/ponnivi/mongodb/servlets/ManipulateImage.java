package com.ponnivi.mongodb.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import com.ponnivi.mongodb.helper.Utilities;



public class ManipulateImage {

	// Decode String into an Image
	public static void convertStringtoImage(String encodedImageStr,	String fileName) {

		try {
			// Decode String using Base64 Class
			System.out.println("fileName.." + fileName);
	//		System.out.println("encodedImageStr.." + encodedImageStr);
			byte[] imageByteArray = Base64.decodeBase64(encodedImageStr); 

			// Write Image into File system - Make sure you update the path
			String osName = Utilities.getUserOperatingSystem();
			String outputFileLocation = "";
			if (osName.startsWith("Windows")) {
				outputFileLocation = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\";
			}
			else {
			outputFileLocation = "/home/ubuntu/instaunite/androidapp/telecomassets/images/";
			}
			
			FileOutputStream imageOutFile = new FileOutputStream(outputFileLocation + fileName);
			imageOutFile.write(imageByteArray);

			imageOutFile.close();

			System.out.println("Image Successfully Stored");
		} catch (FileNotFoundException fnfe) {
			System.out.println("Image Path not found" + fnfe);
		} catch (IOException ioe) {
			System.out.println("Exception while converting the Image " + ioe);
		}

	}
	
	public static void uploadZipFileToServer(String fileName,HttpServletRequest request) {

		try {
			// Decode String using Base64 Class
			System.out.println("fileName.." + fileName);
			// Write Image into File system - Make sure you update the path
			String osName = Utilities.getUserOperatingSystem();
			String outputFileLocation = "";
			if (osName.startsWith("Windows")) {
				outputFileLocation = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\";
			}
			else {
			outputFileLocation = "/home/ubuntu/instaunite/androidapp/telecomassets/images/";
			}
		//	unzip(fileName, outputFileLocation);
			 InputStream in = request.getInputStream();
			 int zipFileNameSeperator = fileName.lastIndexOf("CU");
			 fileName = fileName.substring(zipFileNameSeperator, fileName.length());
			 System.out.println("fileName.." + fileName);
			 File dataFile = new File(fileName);
		        if (!dataFile.exists()) {
		        	dataFile.createNewFile();
		        }
			 OutputStream zipFile = new FileOutputStream(outputFileLocation + dataFile);
			 //Trying to unzip from here
			 Utilities.copy(in, zipFile); //The function is below
			 zipFile.flush();
			 zipFile.close();
			 
			 

			System.out.println("Image Zip Successfully Stored");
		} catch (FileNotFoundException fnfe) {
			System.out.println("Image Path not found" + fnfe);
		} catch (IOException ioe) {
			System.out.println("Exception while converting the Image " + ioe);
		}

	}
	
public static void unzip(String _zipFile, String _targetLocation) {	

		
		try {
			FileInputStream fin = new FileInputStream(_targetLocation + "\\" + _zipFile);
			ZipInputStream zin = new ZipInputStream(fin);
			ZipEntry ze = null;
			while ((ze = zin.getNextEntry()) != null) {
				System.out.println("inside while loop");
				//create dir if required while unzipping
				if (ze.isDirectory()) {
					
				} else {
					FileOutputStream fout = new FileOutputStream(_targetLocation + ze.getName());
					for (int c = zin.read(); c != -1; c = zin.read()) {
						fout.write(c);
					}

					zin.closeEntry();
					fout.close();
				}

			}
			zin.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}	

}
