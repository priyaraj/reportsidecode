package com.ponnivi.mongodb.listener;

import java.io.File;
import java.net.UnknownHostException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.PropertyConfigurator;

import com.mongodb.MongoClient;

@WebListener
public class MongoDBContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent sce) {
		MongoClient mongo = (MongoClient) sce.getServletContext()
							.getAttribute("MONGO_CLIENT");
		mongo.close();
		System.out.println("MongoClient closed successfully");
	}

	public void contextInitialized(ServletContextEvent sce) {
		try {
			ServletContext ctx = sce.getServletContext();
			MongoClient mongo = new MongoClient(
					ctx.getInitParameter("MONGODB_HOST"), 
					Integer.parseInt(ctx.getInitParameter("MONGODB_PORT")));
			System.out.println("MongoClient initialized successfully");
			sce.getServletContext().setAttribute("MONGO_CLIENT", mongo);
			sce.getServletContext().setAttribute("PONNIVI_DATABASE", ctx.getInitParameter("MONGODB_DATABASE_NAME"));			
			sce.getServletContext().setAttribute("PONNIVI_REMOTERENTAL_DATABASE", ctx.getInitParameter("MONGODB_REMOTERENTAL_DATABASE_NAME"));
			//Added log4j -- Oct 19
			String log4jConfigFile = ctx.getInitParameter("log4j-config-location");
	        String fullPath = ctx.getRealPath("") + File.separator + log4jConfigFile;
	         
	        PropertyConfigurator.configure(fullPath);
		} catch (UnknownHostException e) {
			throw new RuntimeException("MongoClient init failed");
		}
	}

}
