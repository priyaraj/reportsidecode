package com.ponnivi.mongodb.converter;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.model.ServiceHistory;

public class ServiceHistoryConverter {

	// convert ServiceHistory Object to MongoDB DBObject
	// take special note of converting id String to ObjectId
	public static DBObject toDBObject(ServiceHistory serviceHistory) {

		BasicDBObjectBuilder builder = BasicDBObjectBuilder.start()
				.append("history", serviceHistory.getHistory()).append("additionalinfo", serviceHistory.getAdditionalinfo());
		if (serviceHistory.getId() != null)
			builder = builder.append("_id", new ObjectId(serviceHistory.getId()));
		return builder.get();
	}

	// convert DBObject Object to ServiceHistory
	// take special note of converting ObjectId to String
	public static ServiceHistory toServiceHistory(DBObject doc) {
		ServiceHistory serviceHistory = new ServiceHistory();
		serviceHistory.setHistory((String) doc.get("history"));
		serviceHistory.setAdditionalInfo((String) doc.get("additionalinfo"));
		ObjectId id = (ObjectId) doc.get("_id");
		serviceHistory.setId(id.toString());
		return serviceHistory;

	}
	
}
