<%@page import="com.ponnivi.mongodb.model.SubscriberPersonalDetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mongodb.DB"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%!
%>
<%
            response.setContentType("text/xml");
            String emailId =request.getParameter("emailId");
            System.out.println("emailId.."+emailId);
            DB customerDatabase = (DB)session.getAttribute("customerDb");
            
           SubscriberPersonalDetails subscriberPersonalDetails = MongoDBDAO.getSubscriberDetails(emailId, customerDatabase);
           if (subscriberPersonalDetails != null) {
           session.setAttribute("subscriberdetails", subscriberPersonalDetails);
           System.out.println("subscriberPersonalDetails.getEndCustomerCity().." + subscriberPersonalDetails.getEndCustomerCity());
           System.out.println("subscriberPersonalDetails.getEndCustomerPostalCode().." + subscriberPersonalDetails.getEndCustomerPostalCode());
           out.println("<subscriber>");
           
 		  out.println("<endcustomername>"+ subscriberPersonalDetails.getEndCustomerName() + "</endcustomername>");
 		  out.println("<endcustomerfirstname>"+ subscriberPersonalDetails.getEndCustomerFirstName() + "</endcustomerfirstname>");
 		  
 		 out.println("<endcustomerphonenumber>"+ subscriberPersonalDetails.getEndCustomerPhoneNumber() + "</endcustomerphonenumber>");
 		 out.println("<endcustomeraddress1>"+ subscriberPersonalDetails.getEndCustomerAddressLine1()+ "</endcustomeraddress1>");
 		if (!subscriberPersonalDetails.getEndCustomerAddressLine2().equalsIgnoreCase("")) {
 			out.println("<endcustomeraddress2>"+ subscriberPersonalDetails.getEndCustomerAddressLine2() + "</endcustomeraddress2>");
 		}
 		else {
 			out.println("<endcustomeraddress2>"+ " " + "</endcustomeraddress2>");
 		}
		 out.println("<endcustomercity>"+ subscriberPersonalDetails.getEndCustomerCity() + "</endcustomercity>");
		 
		 out.println("<endcustomerpostalcode>"+ subscriberPersonalDetails.getEndCustomerPostalCode() + "</endcustomerpostalcode>");
		 if (!subscriberPersonalDetails.getEndCustomerOtherRemarks().equalsIgnoreCase("")) {
 		 	out.println("<endcustomerotherremarks>"+ subscriberPersonalDetails.getEndCustomerOtherRemarks() + "</endcustomerotherremarks>");
		 }
		 else {
			 out.println("<endcustomerotherremarks>"+ " " + "</endcustomerotherremarks>");
		 }
 		 
 		  
 		  out.println("</subscriber>");
           //response.getWriter().print(subscriberPersonalDetails);
           }
/*            else {
        	   out.println("<subscriber>NULL</subscriber>");
           } */
           
			
 %>