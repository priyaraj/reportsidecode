<%@page import="org.json.JSONObject"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | Order Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />	
</head>
<script type="text/javascript" src="js/reusable.js"></script>
<script type="text/javascript" src="js/ts_picker.js"></script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>


	
<script type="text/javascript">
/* var nHist = window.history.length;
if(window.history[nHist] != window.location)
  window.history.forward(); */
$(document).ready(function() { 
	
	$("#startingdDate").datepicker( {
	    inline: true,
	    minDate: 0,
		//nextText: '&rarr;',
		//prevText: '&larr;',
		showOtherMonths: true,
		dateFormat: 'dd/mm/yy',
		dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
		showOn: "button",
		buttonImage: "images/calendar-icon-mac.jpg",
		buttonImageOnly: true,	
	});
	$("#endingdate").datepicker( {
		    inline: true,
		    minDate: 0,
			//nextText: '&rarr;',
			//prevText: '&larr;',
			showOtherMonths: true,
			dateFormat: 'dd/mm/yy',
			dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			showOn: "button",
			buttonImage: "images/calendar-icon-mac.jpg",
			buttonImageOnly: true,	
		});
	
});
  
var dateformatcheck = true;
function validateForm()
{
var quantity=document.getElementById("Quantity").value;
var startingdate=document.getElementById("startingdDate").value;
var endingdate=document.getElementById("endingdate").value;

if (quantity==null || quantity=="")
{
	alert("Quantity should not be empty");
	return false;
}
else {
	if (isNaN(quantity)) 
	 {
	   alert("Must input numbers for Quantity");
	   return false;
	 }
}
if (startingdate==null || startingdate=="")
{
	alert("Starting date  should not be empty");
	return false;
}
/* else {
	dateformatcheck = validatedate(startingdate);
	if (dateformatcheck == false) {
		return false;
	}
} */
if (endingdate==null || endingdate=="")
{
	alert("Ending date should not be empty");
	return false;
}
if( endingdate < startingdate  ){
	alert("Ending date should not be less than starting date");
	return false;
}


}


	function loadInitialFieldValues() {
<%
	
	List updatableFieldList 	= new ArrayList();
	
	
	updatableFieldList = (ArrayList)session.getAttribute("equipmentTypes");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableFieldListOptions = '<select id="equipmenttype" name="equipmenttype" onchange="changeexistingvalues(document.orderdetailform.equipmenttype.value,document.orderdetailform.equipmentname);"  >';
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	    		 JSONObject jsonObject = (JSONObject)updatableFieldList.get(i);
	    		 
	    		 
	     %>
	     updatableFieldListOptions += '<option><%= jsonObject.get("description") %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('equipmenttype').outerHTML = updatableFieldListOptions;
	<%
	updatableFieldList = (ArrayList)session.getAttribute("assetsNames");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableFieldListOptions = '<select id="equipmentname" name="equipmentname"  >';
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	    		 JSONObject jsonObject = (JSONObject)updatableFieldList.get(i);
	     %>
	     updatableFieldListOptions += '<option><%= jsonObject.get("description") %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('equipmentname').outerHTML = updatableFieldListOptions;
	
	}

</script>
<body onload="loadInitialFieldValues()">
	

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subscriber Self Order Form</h1>	   	
       <hr>
       <form name = "orderdetailform" action="orderinformation" method="post">
        <table id="orderdetail" style="padding-top: 10px;float:left;">
       <tr>
       <td id="left"><h10><u> Order Information</u></h10></td>
       </tr>
       <tr>
    <td id="align_new"><h4> Equipment Type</h4></td>
    <td  align="left" id="resize"><select name="equipmenttype" id="equipmenttype">   
     
     </select></td>
      </tr>

  <tr>
  <td id="align_new"><h4> Equipment Name</h4></td>
    <td style="float: left;margin-top: 20px;"><select name="equipmentname" id="equipmentname"></select>   </td>
  </tr>
  <tr>
  <td id="align_new"><h4> *Quantity </h4></td>
    <td style="float: left;margin-top: 20px;"><input type="number"  name="Quantity" id="Quantity" min="1"/></td>
  </tr>
   <tr>
  <td id="align_new"><h4> *Starting Date</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text" readonly="readonly" name="startingdDate" id="startingdDate"/>
    </td>
  </tr> 
  <tr>
  <td id="align_new"><h4> *Ending Date</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text" readonly="readonly" name="endingdate" id="endingdate"/>
    </td>
  </tr>
  <tr>
    <td id="align_new"><h4> Usage </h4></td>
    <td  align="left" id="resize"><select name="usage" id = "usage">
     <option value="24 hours">24 hours</option>
     <option value="8 hours per day">8 hours per day</option>
     <option value="Weekdays only">Weekdays only</option>
     <option value="Weekend only">Weekend only</option>
     <option value="4 hours per day">4 hours per day</option>
     <option value="no limit">no limit</option>
     <option value="monthly anytime">monthly anytime</option>
     </select></td>
      </tr>
  </table>
   <table style="width:80%;padding-top: 10%;float:left;">
  <tr>
            <td><input name="submit" id="submit" type="submit" value="Add other equipments" class="sgncls1"  onclick="return(validateForm());"/></td>
            <td><input name="submit" id="submit" type="submit" value="Provision" class="sgncls_prov"  onclick="return(validateForm());"/></td>
            <td> <input name="submit" id="submit" type="submit" value="Next" class="sgncls_new1" onclick="return(validateForm());" /></td>
            </tr>
    </table> 
</form>
</body>
</html>
