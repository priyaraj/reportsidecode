<%@page import="com.ponnivi.mongodb.model.SubscriberPersonalDetails"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBCollection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | Subscriber Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


<script type="text/javascript" src="js/reusable.js"></script>
<script>
var emailCheck = false;
function getEndCustomerDetails(e){
    if(e.keyCode == 13 || e.keyCode == 9){
    	var emailValue = document.getElementById("Email").value;   	    	
   	    getalldetails(emailValue,document.subscriber_form);
    }
}
jQuery(document).ready(function($){
    $(document).on("keypress", "form", function(event) { 
        return event.keyCode != 13;
    });
    $(document).keydown(function (e) {

 	   if (e.which == 9){
 	            //alert("tab press");
 		  var emailValue = document.getElementById("Email").value;   	    	
 	   	    getalldetails(emailValue,document.subscriber_form);
 	   }
 	     });
    $("#Name").mousedown(function(e) {
    	var emailValue = document.getElementById("Email").value;   	    	
	   	 getalldetails(emailValue,document.subscriber_form);
        });  
    });

/* var nHist = window.history.length;
if(window.history[nHist] != window.location)
  window.history.forward(); */

</script>
<script type="text/javascript">
function validateForm()
{
var x=document.getElementById("Name").value;
var y=document.getElementById("Email").value;
var z=document.getElementById("PhoneNumber").value;
var a=document.getElementById("AddressLine1").value;
var b=document.getElementById("City").value;
var c=document.getElementById("PostalCode").value;
var d=document.getElementById("FirstName").value;
  if (y==null || y=="")
{
	alert("Email Should not be empty");
	return false;
}
else {
	emailCheck = checkEmail(y);
	//alert("emailCheck.." + emailCheck);
	if (emailCheck == false) {
		return false;
	}
} 
 if (x==null || x=="")
{
	alert("Name Should not be empty");
	return false;
}
if (d==null || d=="")
{
	alert("First Name Should not be empty");
	return false;
}
else if (z==null || z=="")
{
	alert("Phone Number Should not be empty");
	return false;
}
else if (a==null || a=="")
{
	alert("Address Line 1 Should not be empty");
	return false;
}
else if (b==null || b=="")
{
	alert("City Should not be empty");
	return false;
}
else if (c==null || c=="")
{
	alert("Postal Code Should not be empty");
	return false;
}   
}
</script>
</head>
<%
	String customerId = request.getParameter("customerId");
    session.setAttribute("customerId", customerId);
	System.out.println("customerId.."+customerId);
	if ((customerId == null || customerId.equals(""))) {
		request.setAttribute("error", "CustomerId is Missing");
//		return false;
	}
	else {

		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		String databaseName = (String) request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		DB ponniviDb = mongo.getDB(databaseName);
		boolean isValid = false; //Check whether customerId exists in customer_definition_collection
		List validCustomerIdList = MongoDBDAO.getDistinct(ponniviDb, "customer_definition_collection", "CustomerId");
		for (int i = 0; i < validCustomerIdList.size(); i++) {
			if (validCustomerIdList.get(i).toString().equalsIgnoreCase(customerId)) {
				isValid = true;
				break;
			}
		}
		if (isValid) {
			String customerDbName = MongoDBDAO.getCollectionSpecificDetails(ponniviDb,"customer_definition_collection","CustomerId",customerId);
			//request.setAttribute("customerDbName", customerDbName);
			ponniviDb = mongo.getDB(customerDbName);
			session.setAttribute("customerDb", ponniviDb);
			session.setAttribute("customerId", customerId);
			//Get equipment type and equipmentname from Ponnivi_Remote_Rental
			DB remoteRentalDb = mongo.getDB("Ponnivi_RemoteRentaldb");
			session.setAttribute("remoterentaldb", remoteRentalDb);
			DBCollection assetCategoryType = remoteRentalDb.getCollection("Asset_Category_Type");
			List equipmentTypeList = MongoDBDAO.getEquipmentDetails(assetCategoryType,0,"Asset_Category_Id","Asset_Category_Type", "");
			if (equipmentTypeList != null) {
			session.setAttribute("equipmentTypes", equipmentTypeList);
			

			DBCollection assets = remoteRentalDb.getCollection("Assets");
			List assetsList = MongoDBDAO.getEquipmentDetails(assets,1,"Asset_Id","Asset_Name", "Asset_Id");
			if (assetsList != null) {
			session.setAttribute("assetsNames", assetsList);
			List wholeAssetsList = new ArrayList();
			List totalAssetsList = new ArrayList();
			//Get all equipmenttype and their corresponding assets (whole) should be included
			try {
			for (int i = 0; i < equipmentTypeList.size();i++) {
				JSONObject jsonObject = (JSONObject)equipmentTypeList.get(i);
				int categoryId;			
					categoryId = (int)Double.parseDouble(jsonObject.getString("id").toString());
					wholeAssetsList = MongoDBDAO.getEquipmentDetails(assets,categoryId,"Asset_Category_Id","Asset_Name", "Asset_Id");
					if (wholeAssetsList != null && wholeAssetsList.size() > 0) {
					for (int j = 0; j < wholeAssetsList.size();j++) {
						totalAssetsList.add(wholeAssetsList.get(j));
					}
					}
				
			}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			session.setAttribute("wholeassets", totalAssetsList);
			//redirectingJsp = "/Subscriber_information.jsp";
			}
			else {
				session.setAttribute("error","Assets are not yet defined properly");
				//redirectingJsp = "/nodata.jsp";
			}
			}
			
			else {
				session.setAttribute("error","Asset Categories are not yet defined properly");
				//redirectingJsp = "/nodata.jsp";
			}
		} else {
			request.setAttribute("error", "CustomerId does not exist");
			//redirectingJsp = "/login.jsp";
		}

	} 
	
	
%>
<body>


	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subscriber Self Order Form</h1>	   	
       <hr>
       <form name="subscriber_form" action="subscriber" method="post">
      <table id="endcust_name"style="padding-top: 10px;float:left">
       <tr>
       <td id="left"><h10><u> End Customer Information</u></h10></td>
       </tr>
       <tr>
    <td  id="align" ><h4> * Email</h4></td>
<td><input type="text"   name="Email" id="Email" style="float: left;" onkeypress="getEndCustomerDetails(event)"  /></td>
  </tr>
       <tr>
    <td id="align"><h4> * Name </h4>
    </td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Name" id="Name"/></td>
  </tr>
  <tr>
  <tr>
    <td  id="align" ><h4> * First Name</h4></td>
    <td><input type="text"   name="FirstName" id="FirstName" style="float: left;"/></td>
  </tr>
  
  <tr>
    <td  id="align" ><h4> * Phone Number</h4></td>
    <td><input type="text"   name="PhoneNumber" id="PhoneNumber" style="float: left;"/></td>
  </tr>
  </table>
   <table id="endcust_addr" style="padding-top: 10px;float:left">
       <tr>
     <td id="left">  <h10><u> Address Information</u></h10></td>
       </tr>
       <tr>
    <td id="align" > <h4> * Delivery Address Line 1</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="AddressLine1" id="AddressLine1"/></td>
  </tr>
  <tr>
  <tr>
    <td  id="align" ><h4> Delivery Address Line 2</h4></td>
    <td><input type="text"   name="AddressLine2" id="AddressLine2" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" > <h4> * City</h4></td>
    <td><input type="text"   name="City" id="City" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" > <h4> * Postal Code</h4></td>
    <td><input type="text"   name="PostalCode" id="PostalCode" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" ><h4> Other remarks</h4></td>
    <td><input type="text"   name="Otherremark" id="Otherremark" style="float: left;"/></td>
  </tr>
  </table>   
   <table id="addbtn"style="padding-top: 30px;float:left">
  <tr>
            <td><input name="Signin" id="clear" type="button" value="Clear Fields" class="sgncls_1"  onclick="clearFields(document.subscriber_form);"/></a></td>
             <td> <input name="Signin" type="submit" value="Next" onclick="return(validateForm());" class="sgncls_2" /></a></td> 
            <!-- <td><input name="Signin" type="submit" value="Next"  class="sgncls_2" /></td> --> 
            </tr>
    </table>        	
    </form>
</body>
</html>
