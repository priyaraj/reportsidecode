<%@page import="com.mongodb.BasicDBObject"%>
<%@page import="com.mongodb.DBCursor"%>
<%@page import="com.mongodb.DBCollection"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="com.ponnivi.mongodb.model.SubscriberPersonalDetails"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | customer Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/supportive.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<body>
	

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subscriber Self Order Form</h1>	   	
       <hr>
       <form name = "orderform" action="finalorderinformation" method="post">
        <table style="width:100%;padding-top: 10px;float:left;">
       <tr>
       <td id="left"><h13><u>THANK YOU.</u></h13></td>
       </tr>
       <tr>
       <td>
       <h14> Your Order ID <%= session.getAttribute("orderId") %>. Please use this reference for future purpose</h14></td>
      <div id="orderdetail">
       <table align="center" class="description_new" >
 <tr >
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">S.no</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Equipment_name</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Usage_description</FONT></td> 
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No_of_days</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Quantity</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Amount</FONT></td>
  </tr>
       <%
  String orderId = (String)session.getAttribute("orderId");
    		   SubscriberPersonalDetails subscriberDetails = (SubscriberPersonalDetails)session.getAttribute("subscriberdetails");
  DB customerDatabase = (DB)session.getAttribute("customerDb");
  DBCollection collection = customerDatabase.getCollection("order_detail");
  List<DBObject> orderDetailsList = MongoDBDAO.getDetails(collection, "orderid", "text", orderId);
  int srNo = 0;
  for (int i = 0; i < orderDetailsList.size(); i++) {
	  DBObject object = (DBObject)orderDetailsList.get(i);	  
	  System.out.println("orderId.." + object.get("orderdetailid") + ".." + object.get("orderid"));
	  srNo += 1;
	  %>
	  <tr>
	  <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= srNo %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("assetname") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("usagedescription") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("noofdays") %></FONT></td> 
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("quantity") %></FONT></td> 
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("amount") %></FONT></td>
	  </tr>
	  <%
  }
  %>
 </table>
</div>
 <table align="left" class="description_addr" >
 <tr >
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Delivery Address1</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Delivery Address2</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">City</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Pincode</FONT></td>
   </tr>
 		<%
       collection = customerDatabase.getCollection("orders");
 		BasicDBObject searchObject = new BasicDBObject();
 		searchObject.put("deliveryto",subscriberDetails.getEndCustomerEmail());
 		DBCursor cursor = collection.find(searchObject);
 		while (cursor.hasNext()) {
 			DBObject object = cursor.next();
 			%>
 			<tr>
	  <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("deliveryaddress1") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("deliveryaddress2") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("deliverycity") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("deliverypincode") %></FONT></td> 
 <%	} 		 %>
  </table>
 
    </form>
 
</body>
</html>