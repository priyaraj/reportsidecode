<%@page import="com.mongodb.DBCollection"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | customer Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/supportive.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>

<script type="text/javascript">
		var checkDisplay = function(check, form) { //check ID, form ID
			form = document.getElementById(form), check = document.getElementById(check);
			check.onclick = function(){
				form.style.display = (this.checked) ? "block" : "none";
				form.reset();
			};
			check.onclick();
		};
		/* var nHist = window.history.length;
		if(window.history[nHist] != window.location)
		  window.history.forward(); */
	</script>

</head>
<body>
	

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subscriber Self Order Form</h1>	   	
       <hr>
       <form name = "orderform" action="finalorderinformation" method="post">
        <table id="detailreview" style="padding-top: 10px;float:left;">
       <tr>
       <td id="align_left" style="float:left;margin-left:0px;"><h10 style="margin-left:0px;"><u>Confirm and Pay</u></h10></td>
       </tr>
       <tr>
       <td>
       <h12> Review order with all details</h12></td>
       </tr>
       </table>
       <table align="center" class="description_new" >
 <tr >
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">S.no</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Equipment_name</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Usage_description</FONT></td> 
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No_of_days</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Quantity</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Amount</FONT></td>
  </tr>
  <%
  String orderId = (String)session.getAttribute("orderId");
  
  DB customerDatabase = (DB)session.getAttribute("customerDb");
  DBCollection collection = customerDatabase.getCollection("order_detail");
  List<DBObject> orderDetailsList = MongoDBDAO.getDetails(collection, "orderid", "text", orderId);
  int srNo = 0;
  for (int i = 0; i < orderDetailsList.size(); i++) {
	  DBObject object = (DBObject)orderDetailsList.get(i);	  
	  System.out.println("orderId.." + object.get("orderdetailid") + ".." + object.get("orderid"));
	  srNo += 1;
	  %>
	  <tr>
	  <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= srNo %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("assetname") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("usagedescription") %></FONT></td>
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("noofdays") %></FONT></td> 
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("quantity") %></FONT></td> 
 <td BGCOLOR="#F1F1F1"><FONT COLOR="#333333"><%= object.get("amount") %></FONT></td>
	  </tr>
	  <%
  }
  %>
 </table>
 <table id="pay_option" style="padding-top: 10px;float:left;">
<tr >
<td id="align_left"> <h11> Payment on Delivery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h11><input type="checkbox" name="POD" id="POD">
</td>
</tr>
<tr >
<td id="align_left"><h11> Advance Payment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h11> 
<input type="checkbox" style="float:left" id="advancepayment" name="advancepayment" />
<input type="text" value="Enter advance amount"  id="advancepaymentamount" name="advancepaymentamount" /></td>
</tr>
<tr >
<td id="align_left"><h11> Billing address Different &nbsp;&nbsp;&nbsp;&nbsp;</h11> 
<input type="checkbox" style="float:left" id="check" name="check" />
</td>
</tr>
<table id="form">
<tr>
  <td id="align_left"><h11> Address Line 1</h11></td>
    <td id="align_left" ><input type="textbox"  name="AddressLine1" id="AddressLine1"/></td>
 </tr>
 <tr>
  <td id="align_left"><h11> Address Line 2</h11></td>
    <td id="align_left"><input type="textbox"  name="AddressLine2" id="AddressLine2"/></td>
 </tr>
  <tr>
  <td id="align_left"><h11> City</h11></td>
    <td id="align_left"><input type="textbox"  name="City" id="City"/></td>
 </tr>
 <tr>
  <td id="align_left"><h11> Pincode</h11></td>
    <td id="align_left"><input type="textbox"  name="Pincode" id="Pincode"/></td>
 </tr>
 </table>
</table>
<table style="width:80%;padding-top: 20px;float:left;">
  <tr>
            <td><input name="submit" id="submit" type="submit" value="Review Order" class="sgncls_12" /></a></td>
            
            <td> <input name="submit" id="submit"  type="submit" value="Submit" class="sgncls_3" /></a></td>
            </tr>
    </table> 
 <script type="text/javascript">
	checkDisplay("check", "form");
	</script>
</body>
</html>