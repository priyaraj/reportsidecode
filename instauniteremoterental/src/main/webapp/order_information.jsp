<%@page import="org.json.JSONObject"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | Order Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script type="text/javascript" src="js/reusable.js"></script>
<script type="text/javascript" src="js/ts_picker.js"></script>
<script type="text/javascript">
var dateformatcheck = true;
function validateForm()
{
var quantity=document.getElementById("Quantity").value;
var startingdate=document.getElementById("startingdDate").value;
var endingdate=document.getElementById("endingdate").value;
if (quantity==null || quantity=="")
{
	alert("Quantity should not be empty");
	return false;
}
else {
	if (isNaN(quantity)) 
	 {
	   alert("Must input numbers for Quantity");
	   return false;
	 }
}
if (startingdate==null || startingdate=="")
{
	alert("Starting date  should not be empty");
	return false;
}
/* else {
	dateformatcheck = validatedate(startingdate);
	if (dateformatcheck == false) {
		return false;
	}
} */
if (endingdate==null || endingdate=="")
{
	alert("Ending date should not be empty");
	return false;
}
/* else {
	var dateformatcheck = true; //initializing to true and check in validatedate
	dateformatcheck = validatedate(endingdate);
	
	if (dateformatcheck == false) {
		return false;
	}
	
		var today = new Date();
		alert("today.." + today);
		if (endingdate > today) {
			alert("Ending date cannot be greater than today date");
            return false;
		}
	
} */
}
function validatedate(inputText)  
{  
	//alert("inputText.." + inputText);
	var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;  
// Match the date format through regular expression  
	if(inputText.match(dateformat))  
	{  
	//alert("inputText match.." + inputText);
//document.form1.text1.focus();  
//Test which seperator is used '/' or '-'  

			var opera1 = inputText.split('/');
			var opera2 = inputText.split('-');
			lopera1 = opera1.length;
			lopera2 = opera2.length;
			// Extract the string into month, date and year  
			if (lopera1 > 1) {
				var pdate = inputText.split('/');
			} else if (lopera2 > 1) {
				var pdate = inputText.split('-');
			}
			var dd = parseInt(pdate[0]);
			var mm = parseInt(pdate[1]);
			var yy = parseInt(pdate[2]);
			//alert(dd + ".." + mm + ".." + yy);
			// Create list of days of a month [assume there is no leap year by default]  
			var ListofDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
			if (mm == 1 || mm > 2) {
				if (dd > ListofDays[mm - 1]) {
					alert('Invalid date format!. Date format should be in dd-mm-yyyy format');
					dateformatcheck = false;
					return false;
				}
			}
			if (mm == 2) {
				var lyear = false;
				if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
					lyear = true;
				}
				if ((lyear == false) && (dd >= 29)) {
					alert('Invalid date format! Date format should be in dd-mm-yyyy format');
					dateformatcheck = false;
					return false;
				}
				if ((lyear == true) && (dd > 29)) {
					alert('Invalid date format! Date format should be in dd-mm-yyyy format');
					dateformatcheck = false;
					return false;
				}
			}
		} else {
			alert("Invalid date format! Date format should be in dd-mm-yyyy format");
			dateformatcheck = false; 
			return false;
		}
	}

	function loadInitialFieldValues() {
<%
	
	List updatableFieldList 	= new ArrayList();
	
	
	updatableFieldList = (ArrayList)session.getAttribute("equipmentTypes");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableFieldListOptions = '<select id="equipmenttype" name="equipmenttype" onchange="changeexistingvalues(document.orderdetailform.equipmenttype.value,document.orderdetailform.equipmentname);"  >';
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	    		 JSONObject jsonObject = (JSONObject)updatableFieldList.get(i);
	    		 
	    		 
	     %>
	     updatableFieldListOptions += '<option><%= jsonObject.get("description") %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('equipmenttype').outerHTML = updatableFieldListOptions;
	<%
	updatableFieldList = (ArrayList)session.getAttribute("assetsNames");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableFieldListOptions = '<select id="equipmentname" name="equipmentname"  >';
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	    		 JSONObject jsonObject = (JSONObject)updatableFieldList.get(i);
	     %>
	     updatableFieldListOptions += '<option><%= jsonObject.get("description") %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('equipmentname').outerHTML = updatableFieldListOptions;
	
	}

</script>
<body onload="loadInitialFieldValues()">
	

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subscriber Self Order Form</h1>	   	
       <hr>
       <form name = "orderdetailform" action="orderinformation" method="post">
        <table style="width:100%;padding-top: 10px;float:left;">
       <tr>
       <td id="left"><h10><u> Order Information</u></h10></td>
       </tr>
       <tr>
    <td id="align_new"><h4> Equipment Type</h4></td>
    <td  align="left" id="resize"><select name="equipmenttype" id="equipmenttype">   
     
     </select></td>
      </tr>

  <tr>
  <td id="align_new"><h4> Equipment Name</h4></td>
    <td style="float: left;margin-top: 20px;"><select name="equipmentname" id="equipmentname"></select>   </td>
  </tr>
  <tr>
  <td id="align_new"><h4> *Quantity </h4></td>
    <td style="float: left;margin-top: 20px;"><input type="number"  name="Quantity" id="Quantity" min="1"/></td>
  </tr>
   <tr>
  <td id="align_new"><h4> *Starting Date</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="startingdDate" id="startingdDate"/>
    <a href="javascript:show_calendar('document.orderdetailform.startingdDate', document.orderdetailform.startingdDate.value);"><img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the timestamp"></a></td>
  </tr>
  <tr>
  <td id="align_new"><h4> *Ending Date</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="endingdate" id="endingdate"/>
    <a href="javascript:show_calendar('document.orderdetailform.endingdate', document.orderdetailform.endingdate.value);"><img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the timestamp"></a></td>
  </tr>
  <tr>
    <td id="align_new"><h4> Usage </h4></td>
    <td  align="left" id="resize"><select name="usage" id = "usage">
     <option value="24 hours">24 hours</option>
     <option value="8 hours per day">8 hours per day</option>
     <option value="Weekdays only">Weekdays only</option>
     <option value="Weekend only">Weekend only</option>
     <option value="4 hours per day">4 hours per day</option>
     <option value="no limit">no limit</option>
     <option value="monthly anytime">monthly anytime</option>
     </select></td>
      </tr>
  </table>
   <table style="width:80%;padding-top: 10%;float:left;">
  <tr>
            <td><input name="submit" id="submit" type="submit" value="Add other equipments" class="sgncls1"  onclick="return(validateForm());"/></a></td>
            <td> <input name="submit" id="submit" type="submit" value="Next" class="sgncls_new1" onclick="return(validateForm());" /></a></td>
            </tr>
    </table> 
</form>
</body>
</html>
