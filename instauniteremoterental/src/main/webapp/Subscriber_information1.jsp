<%@page import="com.ponnivi.mongodb.model.SubscriberPersonalDetails"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | Subscriber Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


<script type="text/javascript" src="js/reusable.js"></script>
<script>
var emailCheck = false;
function getEndCustomerDetails(e){
    if(e.keyCode == 13 || e.keyCode == 9){
    	var emailValue = document.getElementById("Email").value;   	    	
   	    getalldetails(emailValue,document.subscriber_form);
    }
}
jQuery(document).ready(function($){
    $(document).on("keypress", "form", function(event) { 
        return event.keyCode != 13;
    });
    $(document).keydown(function (e) {

 	   if (e.which == 9){
 	            //alert("tab press");
 		  var emailValue = document.getElementById("Email").value;   	    	
 	   	    getalldetails(emailValue,document.subscriber_form);
 	   }
 	     });
    $("#Name").mousedown(function(e) {
    	var emailValue = document.getElementById("Email").value;   	    	
	   	 getalldetails(emailValue,document.subscriber_form);
        });  
    });

/* var nHist = window.history.length;
if(window.history[nHist] != window.location)
  window.history.forward(); */

</script>
<script type="text/javascript">
function validateForm()
{
var x=document.getElementById("Name").value;
var y=document.getElementById("Email").value;
var z=document.getElementById("PhoneNumber").value;
var a=document.getElementById("AddressLine1").value;
var b=document.getElementById("City").value;
var c=document.getElementById("PostalCode").value;
var d=document.getElementById("FirstName").value;
  if (y==null || y=="")
{
	alert("Email Should not be empty");
	return false;
}
else {
	emailCheck = checkEmail(y);
	//alert("emailCheck.." + emailCheck);
	if (emailCheck == false) {
		return false;
	}
} 
 if (x==null || x=="")
{
	alert("Name Should not be empty");
	return false;
}
if (d==null || d=="")
{
	alert("First Name Should not be empty");
	return false;
}
else if (z==null || z=="")
{
	alert("Phone Number Should not be empty");
	return false;
}
else if (a==null || a=="")
{
	alert("Address Line 1 Should not be empty");
	return false;
}
else if (b==null || b=="")
{
	alert("City Should not be empty");
	return false;
}
else if (c==null || c=="")
{
	alert("Postal Code Should not be empty");
	return false;
}   
}
</script>
</head>
<%
	String customerId = request.getParameter("customerId");
    session.setAttribute("customerId", customerId);
	System.out.println("customerId.."+customerId);
	
	
%>
<body>


	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subscriber Self Order Form</h1>	   	
       <hr>
       <form name="subscriber_form" action="subscriber" method="post">
      <table id="endcust_name"style="padding-top: 10px;float:left">
       <tr>
       <td id="left"><h10><u> End Customer Information</u></h10></td>
       </tr>
       <tr>
    <td  id="align" ><h4> * Email</h4></td>
<td><input type="text"   name="Email" id="Email" style="float: left;" onkeypress="getEndCustomerDetails(event)"  /></td>
  </tr>
       <tr>
    <td id="align"><h4> * Name </h4>
    </td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Name" id="Name"/></td>
  </tr>
  <tr>
  <tr>
    <td  id="align" ><h4> * First Name</h4></td>
    <td><input type="text"   name="FirstName" id="FirstName" style="float: left;"/></td>
  </tr>
  
  <tr>
    <td  id="align" ><h4> * Phone Number</h4></td>
    <td><input type="text"   name="PhoneNumber" id="PhoneNumber" style="float: left;"/></td>
  </tr>
  </table>
   <table id="endcust_addr" style="padding-top: 10px;float:left">
       <tr>
     <td id="left">  <h10><u> Address Information</u></h10></td>
       </tr>
       <tr>
    <td id="align" > <h4> * Delivery Address Line 1</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="AddressLine1" id="AddressLine1"/></td>
  </tr>
  <tr>
  <tr>
    <td  id="align" ><h4> Delivery Address Line 2</h4></td>
    <td><input type="text"   name="AddressLine2" id="AddressLine2" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" > <h4> * City</h4></td>
    <td><input type="text"   name="City" id="City" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" > <h4> * Postal Code</h4></td>
    <td><input type="text"   name="PostalCode" id="PostalCode" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" ><h4> Other remarks</h4></td>
    <td><input type="text"   name="Otherremark" id="Otherremark" style="float: left;"/></td>
  </tr>
  </table>   
   <table id="addbtn"style="padding-top: 30px;float:left">
  <tr>
            <td><input name="Signin" id="clear" type="button" value="Clear Fields" class="sgncls_1"  onclick="clearFields(document.subscriber_form);"/></a></td>
             <td> <input name="Signin" type="submit" value="Next" onclick="return(validateForm());" class="sgncls_2" /></a></td> 
            <!-- <td><input name="Signin" type="submit" value="Next"  class="sgncls_2" /></td> --> 
            </tr>
    </table>        	
    </form>
</body>
</html>
