/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014, 2015  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> //RotatingAssets Standard Report Preparation code

 * */
package com.ponnivi.mongodb.report;

import static net.sf.dynamicreports.report.builder.DynamicReports.cht;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;

import java.awt.Color;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.chart.BarChartBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.FillerBuilder;
import net.sf.dynamicreports.report.builder.component.ImageBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;
import com.ponnivi.mongodb.helper.Utilities;
import com.ponnivi.mongodb.model.ItemCounts;


public class RotatingAssetsStandardReportCode {

	TextColumnBuilder<Integer> count1Column, count2Column, count3Column;
	TextColumnBuilder<BigDecimal> count1PriceColumn, count2PriceColumn,
			count3PriceColumn;

	BarChartBuilder barChartForCount, barChartForCost;
	TextColumnBuilder<String> missingColumn, missingColumn1;
	static String plumColorCode = "#871F78";
	static String colorName = "orangeColor";
	static String orangeColorCode = "#ff9326";
	static String whiteColor="whiteColor";
	static String whiteColorCode = "#FFFFFF";
	ArrayList<JSONObject> defectiveList = new ArrayList<JSONObject>();
	ArrayList<JSONObject> missingList = new ArrayList<JSONObject>();
	ArrayList<ItemCounts> itemCounts = new ArrayList<ItemCounts>();
	
	

	public RotatingAssetsStandardReportCode(MongoClient mongo,String databaseName) {
		
		build(mongo,databaseName);
	}


	//chart preparation 
	private void prepareBarCharts() {
		
		Map<String, Color> seriesColors = new HashMap<String, Color>();
		
	      seriesColors.put("Missing Count", Color.getColor("orangeColor"));
	
	      seriesColors.put("Defective Count", Color.getColor("blue_color"));
	
	      seriesColors.put("Added Count", Color.MAGENTA);
	      
		barChartForCount = cht
				.barChart()
			//	.setShowPercentages(false)
			//	.setShowValues(false)
				//.setPercentValuePattern("#,##0")
				//.setShowPercentages(true)
				.setValueAxisFormat(cht.axisFormat().setLabel("Tool Count"))
				.setCategory(missingColumn)
			//	.seriesColorsByName(seriesColors)
				.series(cht.serie(count1Column), cht.serie(count2Column),
						cht.serie(count3Column));
		

		Map<String, Color> seriesColorsByPrice = new HashMap<String, Color>();
		
		seriesColorsByPrice.put("Missing Price", Color.getColor("orangeColor"));
	
		seriesColorsByPrice.put("Defective Price", Color.getColor("blue_color"));
	
		seriesColorsByPrice.put("Added Price", Color.MAGENTA);

		barChartForCost = cht
				.barChart()
		/*		.setShowPercentages(false)
				.setShowValues(false)
				.setPercentValuePattern("#,##0") */
				.setValueAxisFormat(cht.axisFormat().setLabel("Tool Cost"))
				.setCategory(missingColumn1)
				
			//	.seriesColorsByName(seriesColorsByPrice)
				.series(cht.serie(count1PriceColumn),
						cht.serie(count2PriceColumn),
						cht.serie(count3PriceColumn));
	}

	//Get the missinglist, defectivelist and addedlist and get the count & cost 
	private ArrayList<ItemCounts> prepareReport(
			ArrayList<JSONObject> missingList,
			ArrayList<JSONObject> defectiveList) {
		int count1 = 0, count2 = 0, count3 = 0;
		BigDecimal cost1 = new BigDecimal(0), cost2 = new BigDecimal(0), cost3 = new BigDecimal(
				0);
		for (int i = 0; i < missingList.size(); i++) {
			count1 += 1;
			cost1 = getTotalCost(missingList.get(i), cost1);
		}
		for (int i = 0; i < defectiveList.size(); i++) {
			count2 += 1;
			cost2 = getTotalCost(defectiveList.get(i), cost2);
		}
		ItemCounts itemCount = new ItemCounts(count1, count2, count3, cost1,
				cost2, cost3, "ToolBox", "ToolBox");
		itemCounts.add(itemCount);
		return itemCounts;
	}

	//Gettotalcost of missing, defective etc.,
	private BigDecimal getTotalCost(JSONObject jsonObject, BigDecimal cost) {
		try {
			JSONObject specificDetails = jsonObject
					.getJSONObject("Specific_Details");
			cost = cost.add(new BigDecimal(specificDetails.getString("C")));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cost;
	}

	//Build the report
	public void build(MongoClient mongo,String dbName) {
		
		/*//Create MetaJson collection  -- Required for the first time execution
		MongoDBContentReportCode.createJsonCollection(MongoDBContentReportCode.metaJsonFileName,"evidence_description_collection"); //evidence_description_collection
		
		//Create DataJson collection -- Required for the first time execution
		MongoDBContentReportCode.createJsonCollection(MongoDBContentReportCode.dataJsonFileName,"evidences_collection"); //evidences_collection
*/
		StaticContentReportCode.setStyles();

		getHeader();

		// get dataset values here
		// defectiveList -- Get the details of defective items from
		// DataJsonRotatingAssetsDetails
		defectiveList = MongoDBReportDAO
				.getDataJsonDetails("evidences_collection",
						"defective", "Rotating_assets",mongo,dbName);

		// missingList -- Get the details of defective items from
		// DataJsonRotatingAssetsDetails
		missingList = MongoDBReportDAO.getDataJsonDetails(
				"evidences_collection", "missing", "Rotating_assets",mongo,dbName);

		// prepare missing counts, defective counts and added counts
		itemCounts = prepareReport(missingList, defectiveList);
		
		prepareBarCharts();

		try {

			InputStream stream;
			stream = RotatingAssetsStandardReportCode.class
					//.getResourceAsStream("/com/example/images/emsicon.png");
					.getResourceAsStream(Utilities.getImageFileName());
			ImageBuilder img = DynamicReports.cmp
					.image(stream)
					.setFixedDimension(80, 80)
					.setStyle(
							DynamicReports.stl.style().setHorizontalAlignment(
									HorizontalAlignment.LEFT));
			// Set the header and footer line in orange color. If there is change
			// we need to change color code here
			FillerBuilder filler = StaticContentReportCode
					.getHeaderAndFooterLine(orangeColorCode, colorName);
			
			//To get empty white space, let us follow like below
			FillerBuilder whiteFiller = StaticContentReportCode
					.getEmptySpace(whiteColorCode, whiteColor);
			//

			/*
			 * JasperHtmlExporterBuilder htmlExporter = export
			 * .htmlExporter("RotatingAssetsItems.html")
			 * .setImagesDirName(imageDirectoryName) //
			 * .setImagesURI("image?image=") .setOutputImagesToDir(true);
			 */

			report()// create new report design
			.setColumnTitleStyle(StaticContentReportCode.columnHeaderStyle)
					.highlightDetailEvenRows()
					.columns(count1Column, count1PriceColumn, count2Column,
							count2PriceColumn, count3Column, count3PriceColumn)
					.title(DynamicReports.cmp
							.horizontalFlowList(
									img,
									cmp.text("Toolbox Inventory Summary")
											.setStyle(
													StaticContentReportCode.headingBoldCenteredStyle))
							.newRow().add(filler))
					.summary(
							cmp.verticalGap(40),
							cmp.horizontalList(
									barChartForCount,
									barChartForCost)
									).pageFooter(filler)
					.setDataSource(itemCounts)// set datasource
					.show();// create and show report

		} catch (DRException e) {
			e.printStackTrace();
		}

	}

	//Header part generation
	private void getHeader() {
		// Header part begin

		missingColumn = col.column("Missing", "count4", type.stringType());
		missingColumn1 = col.column("Missing", "cost4", type.stringType());

		// count1, price1, count2, price2, count3, price3
		count1Column = col
				.column("Missing Count", "count1", type.integerType())
				.setHorizontalAlignment(HorizontalAlignment.CENTER);
		count1PriceColumn = col.column("Missing Price", "cost1",
				type.bigDecimalType()).setHorizontalAlignment(
				HorizontalAlignment.CENTER);

		count2Column = col.column("Defective Count", "count2",
				type.integerType()).setHorizontalAlignment(
				HorizontalAlignment.CENTER);
		count2PriceColumn = col.column("Defective Price", "cost2",
				type.bigDecimalType()).setHorizontalAlignment(
				HorizontalAlignment.CENTER);

		count3Column = col.column("Added Count", "count3", type.integerType())
				.setHorizontalAlignment(HorizontalAlignment.CENTER);
		count3PriceColumn = col.column("Added Price", "cost3",
				type.bigDecimalType()).setHorizontalAlignment(
				HorizontalAlignment.CENTER);

		TextColumnBuilder<Integer> rowNumberColumn = col.reportRowNumberColumn(
				"No.")
		// sets the fixed width of a column, width = 2 * character width
				.setFixedColumns(2);

		// header part end
	}



}