/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> //Header and footer style preparation

 * */
package com.ponnivi.mongodb.report;

import static net.sf.dynamicreports.report.builder.DynamicReports.stl;

import java.awt.Color;

import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.component.FillerBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;


public class StaticContentReportCode {
	
	static StyleBuilder footerStyle;
	static StyleBuilder columnHeaderStyle;
	static StyleBuilder headingBoldCenteredStyle;
	
	
	
	// Used to draw line in required color. Here colorCode is Hex color code and colorName is user-defined and we can use that color
	public static FillerBuilder getHeaderAndFooterLine(String colorCode, String colorName){
		Color color = setColor(colorCode,colorName);
		FillerBuilder filler = DynamicReports.cmp.filler().setStyle(DynamicReports.stl.style().setTopBorder(DynamicReports.stl.pen2Point()).setForegroudColor(color)).setFixedHeight(40);
		return filler;
	}
	
	public static FillerBuilder getEmptySpace(String colorCode, String colorName){
		Color color = setColor(colorCode,colorName);
		FillerBuilder filler = DynamicReports.cmp.filler().setStyle(DynamicReports.stl.style().setTopBorder(DynamicReports.stl.pen2Point()).setForegroudColor(color)).setFixedHeight(50);
		return filler;
	}
	
	private static Color setColor(String colorCode,String colorName) {
		System.setProperty(colorName, colorCode);
		return Color.getColor(colorName);  
	}
	
	
	
	public static void setStyles() {
		System.setProperty("plumColor", "#871F78");
		System.setProperty("orangeColor", "#ff9326");
		System.setProperty("light_blue_color", "#a7d2ff");
		System.setProperty("blue_color","#2692FF");
		System.setProperty("whiteColor","#FFFFFF");
		StyleBuilder plainStyle = stl.style()				
				.setForegroudColor(Color.getColor("blue_color"))
				.setFontSize(15);
		StyleBuilder headerStyle = stl.style()				
				.setForegroudColor(Color.BLACK)
				.setFontSize(10)
				.setHorizontalAlignment(HorizontalAlignment.CENTER);

		
/*		footerStyle = stl.style().bold()				
				.setForegroundColor(Color.getColor("plumColor"));*/
		footerStyle = stl.style().bold()				
				.setForegroudColor(Color.getColor("orangeColor"));
				
		columnHeaderStyle = DynamicReports.stl.style(headerStyle).setBorder(DynamicReports.stl.pen1Point()).setBackgroundColor(Color.getColor("light_blue_color"));
		
		StyleBuilder boldStyle = stl.style().bold();
		headingBoldCenteredStyle = stl.style(plainStyle).bold()
				.setHorizontalAlignment(HorizontalAlignment.CENTER);
	
		/*
		 * StyleBuilder headerBoldCenteredStyle = stl.style(plainStyle).bold()
		 * .setHorizontalAlignment(HorizontalAlignment.CENTER); StyleBuilder
		 * boldRightAlignedStyle = stl.style(boldStyle)
		 * .setHorizontalAlignment(HorizontalAlignment.RIGHT); StyleBuilder
		 * columnTitleStyle = stl.style(headingBoldCenteredStyle)
		 * .setBorder(stl.
		 * pen1Point()).setBackgroundColor(Color.ORANGE).setForegroundColor
		 * (Color.WHITE);
		 */
		
		
	}
}