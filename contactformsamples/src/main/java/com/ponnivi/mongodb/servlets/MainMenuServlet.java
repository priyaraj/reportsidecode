package com.ponnivi.mongodb.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/mainMenu")
public class MainMenuServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reportName = request.getParameter("report");
		String redirectingJsp = "";
		System.out.println("reportName.." + reportName);	
		

		if (reportName.equalsIgnoreCase("Reports InstaUnite")) {
			redirectingJsp = "/reportsmenu.jsp";
		}
		else if (reportName.equalsIgnoreCase("Admin InstaUnite")) {
			redirectingJsp = "/instaunite_admin.jsp";
		}
		else if (reportName.equalsIgnoreCase("Customer Order Processing")) {
			redirectingJsp = "/customer_information.jsp";
		}
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
