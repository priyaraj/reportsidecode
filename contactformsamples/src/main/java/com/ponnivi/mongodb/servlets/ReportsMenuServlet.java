package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.AggregationOutput;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;
import com.ponnivi.mongodb.model.ReportOptionsDetails;
import com.ponnivi.mongodb.model.ReportParameterNames;

@WebServlet("/reportsMenu")
public class ReportsMenuServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	private String itemCollectionName = "", evidenceCollectionName = "";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reportName = request.getParameter("report");
		String redirectingJsp = "";
		System.out.println("reportName.." + reportName);
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		HttpSession session = request.getSession();
		//Here we will be getting customer databasename and we should always refer for that customer id
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		
		String contextType = (String)session.getAttribute("contextType");
		
		if (contextType.equalsIgnoreCase("Rotating_assets")) { 
		

		if (reportName.equalsIgnoreCase("Your predefined Reports")) {
						
			//RotatingAssetsStandardReportCode reportCode = new RotatingAssetsStandardReportCode(mongo,databaseName);
			redirectingJsp = "/standardreport.jsp";
		}
		if ( reportName.equalsIgnoreCase("Custom Report Toolboxes") || 
				reportName.equalsIgnoreCase("Custom Report Equipments") )
				 {
					if ( reportName.equalsIgnoreCase("Custom Report Toolboxes")) {
						itemCollectionName = "items_collection";
					}
					else {
						itemCollectionName = "items_equipments_collection";
					}
					
			
			//'Get unique incharge values for "Rotating Assets"
					//Changes done on July 2
			List uniqueInchargeList = MongoDBReportDAO.getDistinct(customerDatabase,itemCollectionName,"D");
			List uniqueEvidenceIdentifierList = MongoDBReportDAO.getDistinct(customerDatabase,itemCollectionName,"Evidence_Identifier");
			//If there is no data we need to say no data available -- Sep 25
			request.setAttribute("evidenceIdentifierList", uniqueEvidenceIdentifierList);
			session.setAttribute("evidenceIdentifierListItems", uniqueEvidenceIdentifierList);
			request.setAttribute("inchargelist", uniqueInchargeList);	
			List<ReportParameterNames> reportParameterList = MongoDBReportDAO.getReportParameters("evidence_description_collection", "Evidence_Record", "Items", customerDatabase);
			System.out.println("reportParameterList size" + reportParameterList.size());
			 request.setAttribute("reportparameterlist", reportParameterList);		 
			 
			 

			session.setAttribute("itemCollectionName", itemCollectionName); 
			redirectingJsp = "/customreports.jsp";
		}
		
		

		}
		else if (contextType.equalsIgnoreCase("machine_uptime")){
			System.out.println("customerDatabase.." + customerDatabase);
			AggregationOutput aggregationOutput = MongoDBReportDAO.getAggregateFunctionsForMachineUptime(customerDatabase, "items_collection", "Evidence_2");
			session.setAttribute("reasonOutput", aggregationOutput);
			
			aggregationOutput = MongoDBReportDAO.getAggregateFunctionsForMachineUptime(customerDatabase, "items_collection", "Evidence_3");
			
			session.setAttribute("machinedowntimeOutput", aggregationOutput);
			
			MongoDBReportDAO.computeTotalDownTimeAndAverageDowntime(aggregationOutput,session);
			
			aggregationOutput = MongoDBReportDAO.getAggregateFunctionsForMachineUptime(customerDatabase, "items_collection", "Evidence_4");
			session.setAttribute("actionOutput", aggregationOutput);
		
			if (reportName.equalsIgnoreCase("Your predefined Reports")) {
				redirectingJsp = "/userdefinedreport.jsp";
			}
			if ( reportName.equalsIgnoreCase("Custom Report Machine") || 
					reportName.equalsIgnoreCase("Custom Report Others") )
					 {
				
						if ( reportName.equalsIgnoreCase("Custom Report Machine")) {
							itemCollectionName = "items_collection";
						}
					
				
				 contextType = MongoDBReportDAO.getConnectingLinkNames(
						"evidence_description_collection", "Evidence_Record",
						"Context_Reference_Example", "", customerDatabase);
				session.setAttribute("contextType", contextType);
				
				ReportOptionsDetails uniqueEvidenceIdentifierList = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions","remoteunit",customerDatabase);
				List evidenceIdentifierListItems = uniqueEvidenceIdentifierList.getValuesList();
						
				request.setAttribute("evidenceIdentifierList", evidenceIdentifierListItems);
				session.setAttribute("evidenceIdentifierListItems", evidenceIdentifierListItems);
			//	request.setAttribute("inchargelist", uniqueInchargeList);	
				List<ReportParameterNames> reportParameterList = MongoDBReportDAO.getReportParameters("evidence_description_collection", "Evidence_Record", "Items", customerDatabase);
				System.out.println("reportParameterList size" + reportParameterList.size());
				List<ReportParameterNames> reportParameterList1 = MongoDBReportDAO.getReportParameters("evidence_description_collection", "Evidence_Record", "Asset_Xxx", customerDatabase);
				reportParameterList.addAll(reportParameterList1);
				List<ReportParameterNames>reportParameterList2 = MongoDBReportDAO.getReportParameters("evidence_description_collection", "Evidence_Record", "Asset_Yyy", customerDatabase);
				reportParameterList.addAll(reportParameterList2);
				 request.setAttribute("reportparameterlist", reportParameterList);		
				session.setAttribute("itemCollectionName", itemCollectionName); 
				redirectingJsp = "/customreports_machine.jsp";
			}
			if ( reportName.equalsIgnoreCase("Custom Reports - Graph Format")) {
				redirectingJsp = "/userdefinedreportwithgraphformat.jsp";
			}
			if ( reportName.equalsIgnoreCase("Different Reports - AwesomeChartJs")) {
				redirectingJsp = "/awesomechart.jsp";
			}
			

		}

		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);	
		
		
		}


}
