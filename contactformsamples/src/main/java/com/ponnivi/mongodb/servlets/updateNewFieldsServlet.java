package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/updateNewFields")
public class updateNewFieldsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	String convertedFromDate = "",convertedToDate="";
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String contextType = (String)session.getAttribute("contextType");
		List updatableColumns = (ArrayList)session.getAttribute("updatableColumns");
		
		int beforeUpdateCount = 0, afterUpdateCount = 0;
		String fieldThatIsGettingAdded = (String)request.getParameter("addnewfield");
		String[] collectionNames = (String[])request.getParameterValues("itemcollectionnames");
		String[] evidenceIdentifier = (String[])request.getParameterValues("evidenceIdentifier");
		
		
		
		String collectionName = "",evidence_Identifier = "";
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject updateQuery = new BasicDBObject();
		DBCollection collection = null;
		List newlyAddedValues = new ArrayList(); //To hold the values of newly added field
		if (collectionNames != null) {
			for (int i = 0; i < collectionNames.length; i++) {
				collectionName = collectionNames[i];
				for (int identifierIndex = 0; identifierIndex < evidenceIdentifier.length; identifierIndex++) {
					evidence_Identifier = evidenceIdentifier[identifierIndex];
					BasicDBObject searchObject = new BasicDBObject();
					searchObject
							.put("Evidence_Identifier", evidence_Identifier);
					// Basically we can give numeric value as initial startup
					// and when it is executed for second time, since field is
					// already available, it is incremented and not created
					// newly
					collection = customerDatabase.getCollection(collectionName);
					DBCursor cursor = collection.find(searchObject);
					if (cursor.count() > 0) {
						// We need to check whether that field is already added,
						// if so, we need to skip the below step.
						List checkDistinctElements = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,fieldThatIsGettingAdded,"Evidence_Identifier", evidence_Identifier);
						/*boolean checkIfFieldExisting = MongoDBReportDAO
								.checkIfExisting(fieldThatIsGettingAdded,
										updatableColumns);*/
						if (checkDistinctElements.size() == 0) { // field is not there, so
														// we can add
							DBObject modifiedObject = new BasicDBObject();
							modifiedObject.put("$inc", new BasicDBObject()
									.append(fieldThatIsGettingAdded, 1));

							collection.update(searchObject, modifiedObject,
									true, true);
							DBCursor cursor1 = collection.find(searchObject);
							// cursor = Collection.find(searchObject);
							// As we have added the column, we need to get its
							// value and store in session to show them during
							// existing fields, delete rows etc.,

							while (cursor1.hasNext()) {
								DBObject o = cursor1.next();
								System.out.println("updated field of "
										+ fieldThatIsGettingAdded + ".."
										+ o.get(fieldThatIsGettingAdded)
										+ "..." + o.get("Evidence_Identifier"));
								newlyAddedValues.add(o
										.get(fieldThatIsGettingAdded));
							}
							if (cursor1.count() > 0) {
								request.setAttribute("success"
										+ identifierIndex + i,
										fieldThatIsGettingAdded
												+ " is added/updated to '"
												+ evidence_Identifier
												+ " ' of '" + collectionName
												+ "'");
								session.setAttribute(fieldThatIsGettingAdded,
										newlyAddedValues);
								// Appending the newly added field in json
								MongoDBReportDAO
										.updateJsonAndUpdatableColumnValues(
												contextType,
												fieldThatIsGettingAdded,
												session);
							} else {
								request.setAttribute("success"
										+ identifierIndex + i,
										fieldThatIsGettingAdded
												+ " is not added/updated to '"
												+ evidence_Identifier
												+ " ' of '" + collectionName
												+ "'");
							}
						} else {
							request.setAttribute("success" + identifierIndex
									+ i, fieldThatIsGettingAdded
									+ " is not added/updated to '"
									+ evidence_Identifier + " ' of '"
									+ collectionName + "'");
						}

					}
					else {
						request.setAttribute("success" + identifierIndex
								+ i, fieldThatIsGettingAdded
								+ " is not added/updated to '"
								+ evidence_Identifier + " ' of '"
								+ collectionName + "'");
					}

				}
			}
			request.setAttribute("collectionCount",
					String.valueOf(collectionNames.length));
			request.setAttribute("evidenceIdentifierCount",
					String.valueOf(evidenceIdentifier.length));
		}
		
		String redirectingJsp = "/resultpage.jsp";
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);
		}
		


	}

	




