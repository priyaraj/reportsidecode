package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/getDistinctToolsOrEquipments")
public class GetDistinctToolsOrEquipments extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		String customerDbName = (String)request.getParameter("customerDbName");
		DB customerDatabase = mongo.getDB(customerDbName);
		String itemCollectionName = (String)request.getParameter("itemCollectionName");
		
		List evidenceIdentifierList = MongoDBReportDAO.getDistinct(customerDatabase,itemCollectionName,"Evidence_Identifier");
		
		
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();

		Gson gson = new Gson();
	//	if (evidenceIdentifierList.size() > 0) {
        String evidenceIdentifierListStr = gson.toJson(evidenceIdentifierList);
        
        out.println(evidenceIdentifierListStr);
		/*}
		else {
			out.println("No evidences are seen");
		}*/
		}
	}

