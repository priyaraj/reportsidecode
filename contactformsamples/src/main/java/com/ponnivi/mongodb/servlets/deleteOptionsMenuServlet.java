package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.mongodb.MongoClient;

@WebServlet("/deleteOptionsMenu")
public class deleteOptionsMenuServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	private String itemCollectionName = "", evidenceCollectionName = "";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reportName = request.getParameter("report");
		String redirectingJsp = "";
		System.out.println("reportName.." + reportName);
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		HttpSession session = request.getSession();
		//Here we will be getting customer databasename and we should always refer for that customer id
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		List collectionNamesList 	= new ArrayList();
		
		collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
		
		if (collectionNamesList != null && collectionNamesList.size() > 0) {
		
		

		if (reportName.equalsIgnoreCase("Delete Screen (Rows)")) {
			redirectingJsp = "/delete_rows_using_ajax.jsp";
		}
		if ( reportName.equalsIgnoreCase("Delete Screen (fields/columns)") )			 {					
			redirectingJsp = "/delete_fields_or_columns.jsp";
		}
		} else {
			redirectingJsp = "/nodata.jsp";
		}
		
		session.setAttribute("resultpagevalue", reportName);
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
