package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/updateExistingFields")
public class updateExistingFieldsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	String convertedFromDate = "",convertedToDate="";
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String contextType = (String)session.getAttribute("contextType");
		String updatableColumnName = (String)request.getParameter("updatablecolumns");
		int beforeUpdateCount = 0, afterUpdateCount = 0;
		String[] collectionNames = (String[])request.getParameterValues("itemcollectionnames");
		String existingValue = (String)request.getParameter("existingvalue");
		String newValue = (String)request.getParameter("newvalue");
		
		String getRelevantColumnToUpdate = MongoDBReportDAO.getLinkNames(updatableColumnName,contextType);
		String collectionName = "";
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject updateQuery = new BasicDBObject();
		DBCollection collection = null;
		if (collectionNames != null) {
		for (int i = 0; i < collectionNames.length; i++) {
			collectionName = collectionNames[i];
			
			String updatableColumnDataType = MongoDBReportDAO.getTagValues("report_param_collection",
					"values_and_conditions", "", customerDatabase, updatableColumnName, "");
			if (updatableColumnDataType != null) {
					if (updatableColumnDataType.equalsIgnoreCase("text")) { //For newly added text, we will not get from report_param_collection
				searchQuery
				.append(getRelevantColumnToUpdate,existingValue);
				updateQuery.append("$set", new BasicDBObject()
				.append(getRelevantColumnToUpdate,newValue));
			}
			else {
			if (updatableColumnDataType.equalsIgnoreCase("number")) {
				searchQuery
				.append(getRelevantColumnToUpdate,Integer.parseInt(existingValue));
				updateQuery.append("$set", new BasicDBObject()
				.append(getRelevantColumnToUpdate,Integer.parseInt(newValue)));
			}
			}
			}
			else {  //This part is applicable only to newly created columns or tags
				try {
					int existingValueNumeric = Integer.parseInt(existingValue);
					searchQuery
					.append(updatableColumnName,existingValueNumeric);
				} catch(NumberFormatException e) {
					searchQuery
					.append(updatableColumnName,existingValue);
				}
				try {
					int newValueNumeric = Integer.parseInt(newValue);
					updateQuery.append("$set", new BasicDBObject()
					.append(updatableColumnName,newValueNumeric));
				} catch(NumberFormatException e) {
					updateQuery.append("$set", new BasicDBObject()
					.append(updatableColumnName,newValue));
				}
						
					
					
					
			}
			collection = customerDatabase.getCollection(collectionName);
			DBCursor cursor = collection.find(searchQuery);
			beforeUpdateCount = cursor.count();
			System.out.println("Before update.." + beforeUpdateCount);
			while (cursor.hasNext()) {
				DBObject o = cursor.next();
				System.out.println("before update.." + o.get(getRelevantColumnToUpdate)
						+ "..." + o.get("Evidence_Identifier"));
			}
			if (beforeUpdateCount > 0) {
			collection.updateMulti(searchQuery, updateQuery);
			if (updatableColumnDataType != null) {
			if (updatableColumnDataType.equalsIgnoreCase("number")) {
				searchQuery
				.append(getRelevantColumnToUpdate,Integer.parseInt(newValue));
			}
			else {
			searchQuery
			.append(getRelevantColumnToUpdate,newValue);
			}
			}
			else {
				try {
					int newValueNumeric = Integer.parseInt(newValue);
					searchQuery
					.append(updatableColumnName,newValueNumeric);
				} catch(NumberFormatException e) {
					searchQuery
					.append(updatableColumnName,newValue);
				}
				
			}
			
			cursor = collection.find(searchQuery);
			afterUpdateCount = cursor.count();
			System.out.println("After update.." + afterUpdateCount);
			while (cursor.hasNext()) {
				DBObject o = cursor.next();
				System.out.println("after update.." + o.get(getRelevantColumnToUpdate)
						+ "..." + o.get("Evidence_Identifier"));
			}
			
			if (beforeUpdateCount == afterUpdateCount) {
				request.setAttribute("success" + i, "Sucess in updating " + updatableColumnName + " column value from " + existingValue + " to " + newValue + " for " + afterUpdateCount + " occurences in " + collectionName);
				//Here we need to again get unique values for updated column alone
				MongoDBReportDAO.setUniqueValues(contextType,session,customerDatabase,updatableColumnName,collectionName);
			}
			else {
				int failedCount = beforeUpdateCount - afterUpdateCount;
				request.setAttribute("success" + i, afterUpdateCount + " Occurences are updated for " + updatableColumnName + " column value from " + existingValue + " to " + newValue + " .Remaining " + failedCount + " are not updated.");
			}
			}
			else { //Specific text is not found
				request.setAttribute("success" + i, updatableColumnName + " column value with " + existingValue + " is not available in " + collectionName);
			}
		}
		request.setAttribute("collectionCount",String.valueOf(collectionNames.length));
		String redirectingJsp = "/resultpage.jsp";
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);
		}
		


	}
	private String getActualDate(String todateParamValue,SimpleDateFormat dateFormat, int noOfDays) {
		Calendar cal = Calendar.getInstance();    
		try {
			cal.setTime( dateFormat.parse(todateParamValue));
			cal.add( Calendar.DATE, noOfDays );    
			convertedToDate=dateFormat.format(cal.getTime());    
			System.out.println("Actualdate.."+convertedToDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return convertedToDate;
	}
	
}



