package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.mongodb.MongoClient;

@WebServlet("/updateOptionsMenu")
public class updateOptionsMenuServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	private String itemCollectionName = "", evidenceCollectionName = "";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reportName = request.getParameter("report");
		String redirectingJsp = "";
		System.out.println("reportName.." + reportName);
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		HttpSession session = request.getSession();
		//Here we will be getting customer databasename and we should always refer for that customer id
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String contextType = (String)session.getAttribute("contextType");
		List collectionNamesList 	= new ArrayList();
		
		collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
		
		if (collectionNamesList != null && collectionNamesList.size() > 0) {
		
		//if (contextType.equalsIgnoreCase("Rotating_assets")) {
		if (reportName.equalsIgnoreCase("Update New Fields")) {
			redirectingJsp = "/update_newfields.jsp";
		}
		if ( reportName.equalsIgnoreCase("Update New Tags") )			 {					
			redirectingJsp = "/update_newtags_using_ajax.jsp";
		}
		if ( reportName.equalsIgnoreCase("Update Existing Fields") )			 {					
			//redirectingJsp = "/update_existing_fields.jsp";
			redirectingJsp = "/update_existing_fields_using_ajax.jsp";
		}
		}
		else {
			redirectingJsp = "/nodata.jsp";
		}
		
		/*if (contextType.equalsIgnoreCase("machine_uptime")) {
		if (reportName.equalsIgnoreCase("Update New Fields")) {
			redirectingJsp = "/update_newfields.jsp";
		}
		if ( reportName.equalsIgnoreCase("Update New Tags") )			 {					
			redirectingJsp = "/update_newtags_using_ajax.jsp";
		}
		if ( reportName.equalsIgnoreCase("Update Existing Fields") )			 {					
			//redirectingJsp = "/update_existing_fields.jsp";
			redirectingJsp = "/update_machineuptime_existing_fields_using_ajax.jsp";
		}
		}*/
		session.setAttribute("resultpagevalue", reportName);
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
