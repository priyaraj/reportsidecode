package com.ponnivi.mongodb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;
import com.ponnivi.mongodb.helper.Utilities;
import com.ponnivi.mongodb.report.RotatingAssetsStandardReportCode;

@WebServlet("/prepareStandardReport")
public class PrepareStandardReportServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	
	

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		String databaseName = (String)request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		/*		String metaJsonFileName = (String)request.getContextPath()+"/metajson_rotatingassets.json";
		metaJsonFileName = "http://" + request.getRemoteAddr() + ":8080" + metaJsonFileName;
		
		System.out.println("request.getRequestURI().." + request.getRequestURI());
		System.out.println("request.getRequestURL().." + request.getRequestURL());
		System.out.println(request.getLocalAddr() + request.getLocalPort());
		//metaJsonFileName = (String)request.getRealPath("/") + ;
		System.out.println("metaJsonFileName.." + metaJsonFileName);*/
		
		String metaJsonFileName = Utilities.getJsonFileNames("meta");

		//Parse meta json and inserted into evidence_description_collection
		MongoDBReportDAO metaJsonDAO = new MongoDBReportDAO(mongo, databaseName,"evidence_description_collection",metaJsonFileName);
		
		String dataJsonFileName = Utilities.getJsonFileNames("data");
		MongoDBReportDAO dataJsonDAO = new MongoDBReportDAO(mongo, databaseName,"evidences_collection",dataJsonFileName);
		
		RotatingAssetsStandardReportCode reportCode = new RotatingAssetsStandardReportCode(mongo,databaseName);
		
		
	}
}
