package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/deleteRows")
public class deleteRowsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	String convertedFromDate = "",convertedToDate="";
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String contextType = (String)session.getAttribute("contextType");
		
		int beforeUpdateCount = 0, afterUpdateCount = 0;

		String[] collectionNames = (String[])request.getParameterValues("itemcollectionnames");
		String updatableColumns = (String)request.getParameter("updatablecolumns");
		String condition = (String)request.getParameter("conditions");
		String existingValue = (String)request.getParameter("existingvalue");
		
		
		String collectionName = "",getColumnToCheck = "";
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject updateQuery = new BasicDBObject();
		DBCollection collection = null;
		String redirectingJsp = "";
		if (collectionNames != null) {
		for (int i = 0; i < collectionNames.length; i++) {
			collectionName = collectionNames[i];
			//for (int identifierIndex = 0; identifierIndex < evidenceIdentifier.length; identifierIndex++) {
				//evidence_Identifier = evidenceIdentifier[identifierIndex];
				List<BasicDBObject>  searchObject = new ArrayList<BasicDBObject>();
				getColumnToCheck = MongoDBReportDAO.getLinkNames(updatableColumns,contextType);
				if (getColumnToCheck.equalsIgnoreCase("")) {  //For newly added columns, we will not have the link names
					getColumnToCheck = updatableColumns;
				}
				//searchObject.put(getColumnToCheck, getColumnToCheck);
				
				String dataType = MongoDBReportDAO.getTagValues("report_param_collection",
						"values_and_conditions", "", customerDatabase, updatableColumns, "");
				searchObject = MongoDBReportDAO.setCondition(condition,
						searchObject, updatableColumns, existingValue,
						dataType,collectionName,contextType);
				//.append(getRelevantColumnToUpdate,existingValue);
				//Basically we can give numeric value as initial startup and when it is executed for second time, since field is already available, it is incremented and not created newly
				collection = customerDatabase.getCollection(collectionName);
				DBCursor cursor = collection.find(searchObject.get(0));  //As we are not adding additional conditions, always it will have one argument
				if (cursor.count() > 0) {
					if (contextType.equalsIgnoreCase("Rotating_assets")) {
						if (updatableColumns.equalsIgnoreCase("price")) {													
							collection.remove(searchObject.get(0));
						}
						else {
							collection.remove(new BasicDBObject().append(getColumnToCheck, existingValue));
						}
					}
					else {
						try {
							int existingValueNumeric = Integer.parseInt(existingValue);
							collection.remove(searchObject.get(0));
						} catch(NumberFormatException ex) {
							collection.remove(new BasicDBObject().append(getColumnToCheck, existingValue));	
						}
						
					}
					
					DBCursor cursor1 = collection.find(searchObject.get(0));
				//cursor = Collection.find(searchObject);
				while (cursor1.hasNext()) {
					DBObject o = cursor1.next();
//					System.out.println("updated field of " + fieldThatIsGettingAdded + ".." + o.get(fieldThatIsGettingAdded) + "..." + o.get("Evidence_Identifier"));
				}
				if (cursor1.count() > 0) {
					request.setAttribute("success" + i, "'" + updatableColumns + " " + condition + " " + existingValue + " ' is not removed from '" + collectionName + "'");
				}
				else {
					request.setAttribute("success" + i, "'" + updatableColumns + " " + condition + " " + existingValue + " ' is removed from '" + collectionName + "'");
					MongoDBReportDAO.setUniqueValues(contextType,session,customerDatabase);
				}
			}
				else {
					request.setAttribute("success" + i, "'" + updatableColumns + "' with value '" + existingValue + " ' is not removed from '" + collectionName + "'");
				}
			
			
			//}

			
			
		}
		request.setAttribute("collectionCount",String.valueOf(collectionNames.length));		
		redirectingJsp = "/resultpage.jsp";
		}
		else {
			redirectingJsp = "/nodata.jsp";
		}
		
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);
		}
		


	}

	




