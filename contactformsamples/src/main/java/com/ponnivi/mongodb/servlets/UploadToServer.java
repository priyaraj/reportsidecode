package com.ponnivi.mongodb.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.MongoClient;
import com.ponnivi.mongodb.helper.Utilities;

@WebServlet("/UploadToServer")
public class UploadToServer extends HttpServlet {

	private static final long serialVersionUID = -7060758261496829905L;
	static String dataJsonFileName="", osName="",dataFileName="";
	OutputStream out;
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
			System.out.println(".... in doPost...");
			MongoClient mongo = (MongoClient) request.getServletContext()
					.getAttribute("MONGO_CLIENT");
			String databaseName = (String)request.getServletContext()
					.getAttribute("PONNIVI_DATABASE");
			
	        InputStream in = request.getInputStream();
	        osName = Utilities.getUserOperatingSystem();   
	        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
			Calendar calobj = Calendar.getInstance();
			int  month = new Date().getMonth();
			month += 1;
			String fileNameAppender = new Date().getDate() + "_" + month + "_" + calobj.get(Calendar.YEAR) + "_" + new Date().getHours() + "_" + new Date().getMinutes() + "_" + new Date().getSeconds();

	        dataFileName = "datafile_" + fileNameAppender + ".json";
	        
	        if (osName.startsWith("Windows")) {
	        	//dataJsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\" + dataFileName;
	        	dataJsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\" + dataFileName;
	        	dataFileName = dataJsonFileName;
	        }
	        else {
	        	dataJsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/" + dataFileName;
	        	dataFileName = dataJsonFileName;
	        }
	        File dataFile = new File(dataJsonFileName);
	        if (!dataFile.exists()) {
	        	dataFile.createNewFile();
	        }
	        
	       //	out = new FileOutputStream(dataJsonFileName);
	    	out = new FileOutputStream(dataFile);
	       
	        Utilities.copy(in, out); //The function is below
	        out.flush();
	        out.close();
	       
	    
	        //Remove unwanted string and create datajson
	        dataJsonFileName = Utilities.removeAndCreateAptData(dataJsonFileName,"json");

	}
	


	
	}


