package com.ponnivi.mongodb.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/adminMenu")
public class AdminMenuServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	private String itemCollectionName = "", evidenceCollectionName = "";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reportName = request.getParameter("report");
		String redirectingJsp = "";
		System.out.println("reportName.." + reportName);
		/*MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		HttpSession session = request.getSession();
		//Here we will be getting customer databasename and we should always refer for that customer id
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		
		String contextType = (String)session.getAttribute("contextType");
		
		//Depends upon contextType we need to take uptablecolumn and pass in update jsp
		String updatable_fields_json_filename = "Updatable_Fields_" + contextType + ".json";
		
		updatable_fields_json_filename = MongoDBReportDAO.getJsonFileNames(updatable_fields_json_filename);
		
		List updatable_columns = MongoDBReportDAO.getUpdatableColumns(updatable_fields_json_filename);
		
		session.setAttribute("updatableColumns", updatable_columns);*/
		
		

		if (reportName.equalsIgnoreCase("Update")) {
						
			//RotatingAssetsStandardReportCode reportCode = new RotatingAssetsStandardReportCode(mongo,databaseName);
			redirectingJsp = "/instaunite_update_options.jsp";
		}
		if (reportName.equalsIgnoreCase("Delete")) {			
			//RotatingAssetsStandardReportCode reportCode = new RotatingAssetsStandardReportCode(mongo,databaseName);
			redirectingJsp = "/instaunite_delete_options.jsp";
		}
		/*if ( reportName.equalsIgnoreCase("Delete") )
				 {
					if ( reportName.equalsIgnoreCase("Custom Report Toolboxes")) {
						itemCollectionName = "items_collection";
					}
					else {
						itemCollectionName = "items_equipments_collection";
					}
					
			
			//'Get unique incharge values for "Rotating Assets"
					//Changes done on July 2
			List uniqueInchargeList = MongoDBReportDAO.getDistinct(customerDatabase,itemCollectionName,"D");
			List uniqueEvidenceIdentifierList = MongoDBReportDAO.getDistinct(customerDatabase,itemCollectionName,"Evidence_Identifier");
			request.setAttribute("evidenceIdentifierList", uniqueEvidenceIdentifierList);
			session.setAttribute("evidenceIdentifierListItems", uniqueEvidenceIdentifierList);
			request.setAttribute("inchargelist", uniqueInchargeList);	
			List<ReportParameterNames> reportParameterList = MongoDBReportDAO.getReportParameters("evidence_description_collection", "Evidence_Record", "Items", customerDatabase);
			System.out.println("reportParameterList size" + reportParameterList.size());
			 request.setAttribute("reportparameterlist", reportParameterList);		 
			 
			 

			session.setAttribute("itemCollectionName", itemCollectionName); 
			redirectingJsp = "/customreports.jsp";
		}*/
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
