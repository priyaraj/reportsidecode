package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/customreport")
public class CustomReportsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	
	String redirectingJsp = "";
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String convertedFromDate = "",convertedToDate="";
		boolean fromDropDown = false;
		String param1Name = (String)request.getParameter("firstparameter");
		String param1Condition = (String)request.getParameter("param1Condition");
		String param1Value = (String)request.getParameter("param1Value");
		
		String param2Name = (String)request.getParameter("secondparameter");
		String param2Condition = (String)request.getParameter("param2Condition");
		String param2Value = (String)request.getParameter("param2Value");
		
		String param3Name = (String)request.getParameter("thirdparameter");
		String param3Condition = (String)request.getParameter("param3Condition");
		String param3Value = (String)request.getParameter("param3Value");
		
		String evidenceIdentifierValue = (String)request.getParameter("evidenceIdentifier");
		
		String fromdateParamValue = (String)request.getParameter("fromdateParam");
		String todateParamValue = (String)request.getParameter("todateParam");
		
		
		
		String reportTitle = (String)request.getParameter("reporttitle");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd-MM-yy" );
		if (fromdateParamValue.length() > 0 && todateParamValue.length() > 0) {
		   convertedToDate = getActualDate(todateParamValue, dateFormat, 1);
		   convertedFromDate = fromdateParamValue;
		/*Calendar cal = Calendar.getInstance();    
		try {
			cal.setTime( dateFormat.parse(todateParamValue));
			cal.add( Calendar.DATE, 1 );    
			convertedDate=dateFormat.format(cal.getTime());    
			System.out.println("Date increase by one.."+convertedDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/    
		}
		else { //User has chosen dropdown
			fromdateParamValue = (String)request.getParameter("datepattern");
			fromDropDown = true;
			if (!fromdateParamValue.equalsIgnoreCase("NULL")) {
			System.out.println("fromdateParamValue.." + fromdateParamValue);
			todateParamValue = dateFormat.format(new Date());   
			if (fromdateParamValue.equalsIgnoreCase("Less than a week")) {
				convertedFromDate = getActualDate(todateParamValue, dateFormat, -7);				
			}
			if (fromdateParamValue.equalsIgnoreCase("Less than a month")) {
				convertedFromDate = getActualDate(todateParamValue, dateFormat, -30);		
			}
			if (fromdateParamValue.equalsIgnoreCase("Less than 6 months")) {
				convertedFromDate = getActualDate(todateParamValue, dateFormat, -180);	
			}
			if (fromdateParamValue.equalsIgnoreCase("Less than a year")) {
				convertedFromDate = getActualDate(todateParamValue, dateFormat, -365);	
			}
	
			convertedToDate = getActualDate(todateParamValue, dateFormat, 1);
			}
		}
		System.out.println("convertedFromDate.." + convertedFromDate);
		System.out.println("convertedToDate.." + convertedToDate);
		
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		String databaseName = (String)request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		HttpSession session = request.getSession(false);
		List evidenceIdentifierList = null;
		if (session != null) {
			evidenceIdentifierList = (ArrayList)session.getAttribute("evidenceIdentifierListItems");
		}
		String collectionName = (String)session.getAttribute("itemCollectionName");
		//Query based upon menu
		
		//String customerDatabaseName = (String)session.getAttribute("customerDbName");
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		
		String contextType = (String)session.getAttribute("contextType");
		
		List<DBObject> report1Details = null;
		
		String identificationElement = null;
		String logoutTime = "LogoutTime";
		
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
			identificationElement = "Evidence_Identifier";			
		}
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			identificationElement = "Asset_Zzz";			
		}
		
		report1Details = MongoDBReportDAO.retrieveReportWithAndOr(collectionName, "Evidence_Record", "Items", customerDatabase, param1Name,
				param1Condition,param1Value,param2Name,param2Condition,param2Value,param3Name,param3Condition,param3Value,identificationElement,
				evidenceIdentifierValue,evidenceIdentifierList,logoutTime,convertedFromDate,convertedToDate, contextType);
	
/*		List<DBObject> report1Details = MongoDBReportDAO.retrieveReportWithAndOrAnotherPattern("items_collection", "Evidence_Record", "Items", mongo, databaseName,
		param1Name,param1Condition,param1Value,param2Name,param2Condition,param2Value,param3Name,param3Condition,param3Value,
		"Evidence_Identifier",evidenceIdentifierValue,"LogoutTime",dateParamValue);*/
		
		request.setAttribute("report1Details", report1Details);	
		 if (reportTitle == null) {
			 reportTitle = "";
		 }
		 request.setAttribute("reportTitle", reportTitle);
		 String reportCondtions = " When " + param1Name + " " + param1Condition + " " + param1Value;
		 if (!param2Name.equalsIgnoreCase("NULL")) {
			 reportCondtions += " and " + param2Name + " " + param2Condition + " " + param2Value;
		 }
		 if (!param3Name.equalsIgnoreCase("NULL")) {
			 reportCondtions += " or " + param3Name + " " + param3Condition + " " + param3Value;
		 }
		 if (collectionName.equalsIgnoreCase("items_collection")) {
			 reportCondtions += " for toolbox = " + "'" + evidenceIdentifierValue + "'";
		 }
		 if (collectionName.equalsIgnoreCase("items_equipments_collection")){
			 reportCondtions += " for equipmentbox = " + "'" + evidenceIdentifierValue + "'";
		 }
		 else{
		 	reportCondtions += " for RemoteUnit = " + "'" + evidenceIdentifierValue + "'";
		 }
		 if (fromDropDown) {
			 reportCondtions += " and in the dateranges of " + "'" + fromdateParamValue + "'";
		 }
		 else {
		 reportCondtions += " and in the dateranges between " + "'" + fromdateParamValue + " and " + todateParamValue + "'";
		 }
		 request.setAttribute("reportCondtions", reportCondtions);
		
		 if (contextType.equalsIgnoreCase("Rotating_assets")) {
			 redirectingJsp = "/rotatingassetsreportdetails.jsp";
		 }
		 if (contextType.equalsIgnoreCase("machine_uptime")) {
			 redirectingJsp = "/machineuptimereportdetails.jsp";
		 }
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);


	}
	private String getActualDate(String todateParamValue,SimpleDateFormat dateFormat, int noOfDays) {
		String toDate = "";
		Calendar cal = Calendar.getInstance();    
		try {
			cal.setTime( dateFormat.parse(todateParamValue));
			cal.add( Calendar.DATE, noOfDays );    
			toDate=dateFormat.format(cal.getTime());    
			System.out.println("Actualdate.."+toDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toDate;
	}
	
}



