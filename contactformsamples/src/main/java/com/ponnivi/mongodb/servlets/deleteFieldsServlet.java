package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/deleteFields")
public class deleteFieldsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	String updatableColumnName = "",existingValue="",collectionName = "";
	DB customerDatabase = null;
	BasicDBObject searchQuery = new BasicDBObject();
	List updatableColumnList = null;
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		customerDatabase = (DB)session.getAttribute("customerDb");
		String contextType = (String)session.getAttribute("contextType");
		updatableColumnList = (ArrayList)session.getAttribute("updatableColumns");
		String[] collectionNames = (String[])request.getParameterValues("itemcollectionnames");
		updatableColumnName = (String)request.getParameter("updatablecolumns");
		existingValue = (String)request.getParameter("existingvalue");
		String redirectingJsp = "";
		
		
		List collectionNameList = new ArrayList();
		if (collectionNames != null) {
			for (int i = 0; i < collectionNames.length; i++) {
				collectionNameList.add(collectionNames[i]);
			}
			queryCollectionAndDeleteColumnsInCollectionOrJson(collectionNameList,updatableColumnName,existingValue,contextType,"deletefromcollection",request,session);

			//Now need to remove the column from json.first remove from the list which is in session
			//Here we need to check whether the same column appears in other collection
			collectionNameList = (ArrayList)session.getAttribute("collectionNames");
			queryCollectionAndDeleteColumnsInCollectionOrJson(collectionNameList,updatableColumnName,existingValue,contextType,"deletefromjson",request,session);
			request.setAttribute("collectionCount",String.valueOf(collectionNames.length));	
			redirectingJsp = "/resultpage.jsp";
		}
		
		else {
			redirectingJsp = "/nodata.jsp";
		}
		
		
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);
		}
		
	public void queryCollectionAndDeleteColumnsInCollectionOrJson(List collectionNameList,String updatableColumnName,
			String existingValue,String contextType,String type,HttpServletRequest request,
			HttpSession session) {
		boolean deletedColumnStillFound = false;
		if (collectionNameList != null) {
			for (int i = 0; i < collectionNameList.size(); i++) {
				collectionName = collectionNameList.get(i).toString();
				DBCollection collection = null;
				String getRelevantColumnToDelete = MongoDBReportDAO.getLinkNames(updatableColumnName,contextType);
				collection = customerDatabase.getCollection(collectionName);
				searchQuery = new BasicDBObject();
				String updatableColumnDataType = MongoDBReportDAO.getTagValues("report_param_collection",
						"values_and_conditions", "", customerDatabase, updatableColumnName, "");
				if (updatableColumnDataType != null) {
					if (updatableColumnDataType.equalsIgnoreCase("text")) { //For newly added text, we will not get from report_param_collection
						searchQuery.append(getRelevantColumnToDelete,existingValue);
					}
					else {
						if (updatableColumnDataType.equalsIgnoreCase("number")) {
							searchQuery
							.append(getRelevantColumnToDelete,Integer.parseInt(existingValue));
						}
					}
				} else {  //This part is applicable only to newly created columns or tags
					try {
						int existingValueNumeric = Integer.parseInt(existingValue);
						searchQuery
						.append(updatableColumnName,existingValueNumeric);
					} catch(NumberFormatException e) {
						searchQuery
						.append(updatableColumnName,existingValue);
					}	
				}

				DBCursor cursor = collection.find(searchQuery);
				if (type.equalsIgnoreCase("deletefromcollection")) {
				if (cursor.count() > 0) {
					
					DBObject deletedObject =new BasicDBObject();
					deletedObject.put("$unset", new BasicDBObject().append(updatableColumnName, ""));
					while (cursor.hasNext()) {
						DBObject o = cursor.next();
					collection.update(searchQuery, deletedObject);
					}

					request.setAttribute("success" + i, "'" + updatableColumnName + " ' is removed from '" + collectionName + "'");
				}
				else {
					request.setAttribute("success" + i, "'" + updatableColumnName + " ' is not removed from '" + collectionName + "'");
				}
			}
			if (type.equalsIgnoreCase("deletefromjson")) {
					if (cursor.count() > 0) {
						deletedColumnStillFound = true;
						break;
					}
			}				}
		}
		if (type.equalsIgnoreCase("deletefromjson")) {
		if (!deletedColumnStillFound) {  //if that column is not present in any of the collections, we should remove from json
			updatableColumnList.remove(updatableColumnName);
			MongoDBReportDAO.refreshUpdatableColumns(contextType,updatableColumnList);
			session.setAttribute("updatableColumns", updatableColumnList);
		}
		}


	}

}




