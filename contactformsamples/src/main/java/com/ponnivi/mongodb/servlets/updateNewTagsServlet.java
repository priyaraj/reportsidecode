package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/updateNewTags")
public class updateNewTagsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	String convertedFromDate = "", convertedToDate = "";

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		DB customerDatabase = (DB) session.getAttribute("customerDb");
		String contextType = (String) session.getAttribute("contextType");
		List updatableColumnList = (ArrayList) session
				.getAttribute("updatableColumns");
		int beforeUpdateCount = 0, afterUpdateCount = 0;
		String fieldThatIsGettingAdded = (String) request
				.getParameter("newfield");
		String tagNameOfTheField = (String) request.getParameter("newtag");
		String[] collectionNames = (String[]) request
				.getParameterValues("itemcollectionnames");
		String updatableColumns = (String) request
				.getParameter("updatablecolumns");
		String condition = (String) request.getParameter("conditions");
		String existingValue = (String) request.getParameter("existingvalue");

		String collectionName = "", getColumnToCheck = "";
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject updateQuery = new BasicDBObject();
		DBCollection collection = null;
		if (collectionNames != null) {
			for (int i = 0; i < collectionNames.length; i++) {
				collectionName = collectionNames[i];
				// for (int identifierIndex = 0; identifierIndex <
				// evidenceIdentifier.length; identifierIndex++) {
				// evidence_Identifier = evidenceIdentifier[identifierIndex];
				List<BasicDBObject> searchObject = new ArrayList<BasicDBObject>();
				getColumnToCheck = MongoDBReportDAO
						.getLinkNames(updatableColumns,contextType);
				if (getColumnToCheck.equalsIgnoreCase("")) {
					getColumnToCheck = updatableColumns;
				}
				// searchObject.put(getColumnToCheck, getColumnToCheck);

				String dataType = MongoDBReportDAO.getTagValues(
						"report_param_collection", "values_and_conditions", "",
						customerDatabase, updatableColumns, "");

				searchObject = MongoDBReportDAO.setCondition(condition,
						searchObject, updatableColumns, existingValue,
						dataType, collectionName,contextType);

				// .append(getRelevantColumnToUpdate,existingValue);
				// Basically we can give numeric value as initial startup and
				// when it is executed for second time, since field is already
				// available, it is incremented and not created newly
				collection = customerDatabase.getCollection(collectionName);
				DBCursor cursor = collection.find(searchObject.get(0)); // As we
																		// are
																		// not
																		// adding
																		// additional
																		// conditions,
																		// always
																		// it
																		// will
																		// have
																		// one
																		// argument
				if (cursor.count() > 0) {
					DBObject modifiedObject = new BasicDBObject();
					modifiedObject.put("$set", new BasicDBObject().append(
							fieldThatIsGettingAdded, 1));
					List checkDistinctElements = MongoDBReportDAO.getDistinct(
							customerDatabase, collectionName,
							fieldThatIsGettingAdded, getColumnToCheck,
							existingValue);
					if (checkDistinctElements.size() == 0) { // field is not
																// there, so
						// we can add
						collection.update(searchObject.get(0), modifiedObject,
								true, true);
						// Now update the column with the new tag value
						updateQuery = new BasicDBObject();
						BasicDBObject searchQueryAfterUpdate = new BasicDBObject();

						try {
							int tagNameOfTheFieldNumeric = Integer
									.parseInt(tagNameOfTheField);
							updateQuery.append("$set", new BasicDBObject()
									.append(fieldThatIsGettingAdded,
											tagNameOfTheFieldNumeric));
							searchQueryAfterUpdate.put(fieldThatIsGettingAdded,
									tagNameOfTheFieldNumeric);
						} catch (NumberFormatException ex) {
							updateQuery.append("$set", new BasicDBObject()
									.append(fieldThatIsGettingAdded,
											tagNameOfTheField));
							searchQueryAfterUpdate.put(fieldThatIsGettingAdded,
									tagNameOfTheField);
						}

						collection
								.updateMulti(searchObject.get(0), updateQuery);
						// DBCursor cursor1 =
						// collection.find(searchObject.get(0));
						DBCursor cursor1 = collection
								.find(searchQueryAfterUpdate);
						// cursor = Collection.find(searchObject);
						while (cursor1.hasNext()) {
							DBObject o = cursor1.next();
							System.out.println("updated field of "
									+ fieldThatIsGettingAdded + ".."
									+ o.get(fieldThatIsGettingAdded) + "..."
									+ o.get("Evidence_Identifier"));
						}
						if (cursor1.count() > 0) {
							request.setAttribute(
									"success" + i,
									"'"
											+ fieldThatIsGettingAdded
											+ "' is added/updated with the value of '"
											+ tagNameOfTheField + " ' in '"
											+ collectionName + "'" + " for '"
											+ cursor1.count() + "' occurences");
							// Appending the newly added field in json
							MongoDBReportDAO
									.updateJsonAndUpdatableColumnValues(
											contextType,
											fieldThatIsGettingAdded, session);
						} else {
							request.setAttribute(
									"success" + i,
									"'"
											+ fieldThatIsGettingAdded
											+ "' is not added/updated with the value of '"
											+ tagNameOfTheField + " ' in '"
											+ collectionName + "'");
						}
					} else {
						request.setAttribute(
								"success" + i,
								"'"
										+ fieldThatIsGettingAdded
										+ "' is not added/updated with the value of '"
										+ tagNameOfTheField + " ' in '"
										+ collectionName + "'");
					}
				} else {
					request.setAttribute("success" + i, "'"
							+ fieldThatIsGettingAdded
							+ "' is not added/updated with the value of '"
							+ tagNameOfTheField + " ' in '" + collectionName
							+ "'");
				}

				// }

			}
			request.setAttribute("collectionCount",
					String.valueOf(collectionNames.length));
		}

		String redirectingJsp = "/resultpage.jsp";

		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);
	}

}
