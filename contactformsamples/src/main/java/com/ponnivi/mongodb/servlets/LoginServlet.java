package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBReportDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String customerId = request.getParameter("customerId");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String redirectingJsp = "";
		boolean isValid = validateCredentials(customerId,userName, password, request);
		if (isValid) {
			MongoClient mongo = (MongoClient) request.getServletContext()
					.getAttribute("MONGO_CLIENT");
			String databaseName = (String) request.getServletContext()
					.getAttribute("PONNIVI_DATABASE");
			DB ponniviDb = mongo.getDB(databaseName);
			isValid = false; //Check whether customerId exists in customer_definition_collection
			List validCustomerIdList = MongoDBReportDAO.getDistinct(ponniviDb, "customer_definition_collection", "CustomerId");
			for (int i = 0; i < validCustomerIdList.size(); i++) {
				if (validCustomerIdList.get(i).toString().equalsIgnoreCase(customerId)) {
					isValid = true;
					break;
				}
			}
			if (isValid) {
				String customerDbName = MongoDBReportDAO.getCollectionSpecificDetails(ponniviDb,"customer_definition_collection","CustomerId",customerId);
				//request.setAttribute("customerDbName", customerDbName);
				HttpSession session = request.getSession(true);
				ponniviDb = mongo.getDB(customerDbName);
				session.setAttribute("customerDb", ponniviDb);
				//Check for the contextType : Eg : Rotating_Assets or Machine_uptime
				String contextType = MongoDBReportDAO.getConnectingLinkNames(
						"evidence_description_collection", "Evidence_Record",
						"Context_Reference_Example", "", ponniviDb);
				session.setAttribute("contextType", contextType);				
				session.setAttribute("customerId", customerId);
				//create report_param_collection
				long collectionCount = MongoDBReportDAO.getCollectionCount("report_param_collection", ponniviDb);
				if (collectionCount == 0) {
				String optionsArrayJsonFileName = MongoDBReportDAO.getJsonFileNames("options_array.json","");
				MongoDBReportDAO optionsArrayDAO = new MongoDBReportDAO(mongo,customerDbName,"report_param_collection",optionsArrayJsonFileName);
				}
				MongoDBReportDAO.setUniqueValues(contextType,session,ponniviDb);
				List collectionNames = (ArrayList)session.getAttribute("collectionNames");
				if (collectionNames != null && collectionNames.size() > 0) {				
					redirectingJsp = "/menu.jsp";
				}
				else {
					redirectingJsp = "/nodata.jsp";
				}
			} else { 
				request.setAttribute("error", "CustomerId does not exist");
				redirectingJsp = "/login.jsp";
			}

		} else {
			redirectingJsp = "/login.jsp"; 
			 
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				  redirectingJsp); 
				  rd.forward(request, response);


	}

	private boolean validateCredentials(String customerId,String userName, String password,
			HttpServletRequest request) {
		if ((customerId == null || customerId.equals(""))) {
			request.setAttribute("error", "CustomerId is Missing");
			return false;
		}
		/*if ((userName == null || userName.equals(""))) {
			request.setAttribute("error", "User Name is Missing");
			return false;
		} else if ((password == null || password.equals(""))) {
			request.setAttribute("error", "Password is Missing");
			return false;
		}*/
		return true;
	}
}
