package com.ponnivi.mongodb.dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.ponnivi.mongodb.helper.Utilities;
import com.ponnivi.mongodb.model.ReportOptionsDetails;
import com.ponnivi.mongodb.model.ReportParameterNames;

//DAO class for different MongoDB CRUD operations
//take special note of "id" String to ObjectId conversion and vice versa
//also take note of "_id" key for primary key
public class MongoDBReportDAO {

	private DBCollection col;
	static DB ponniviDb = null;
	static String contextReferenceExample;
	static String evidence_2;
	static String evidence_3;
	static String evidence_4;
	static String reportId = "1";
	static Map<String, Object> map = new HashMap<String, Object>();

	// Here we have inserted meta and data jsons
	public MongoDBReportDAO(MongoClient mongo, String dbName,
			String collectionName, String filePath) {
		// System.out.println("filePath.." + filePath);
		ponniviDb = mongo.getDB(dbName);

		DBCollection Collection = ponniviDb.getCollection(collectionName);

		// Get collectionName details and put

		// convert JSON to DBObject directly

		DBObject jsonDbObject = (DBObject) JSON.parse(Utilities
				.getJsonDetails(filePath));

		Collection.insert(jsonDbObject);

	}
	
	//This method will have all the changes related to contextType and set the values in session
	//Eg : contextType : Rotating_assets
	public static void setUniqueValues(String contextType,HttpSession session,DB customerDatabase) {
		//Available collections to query
		List contextTypeCollectionNames = new ArrayList();	
		List availableCollectionsForAdminFunctions = new ArrayList();
		List uniqueItemDList = null;
		List uniqueItemCList = null;
		List uniqueItemAList = null;
		List uniqueItemBList = null;
		List uniqueEvidenceIdentifierList = null;
		List uniqueEvidence3List = null;
		List uniqueEvidence2List = null;
		List uniqueMissingItemSeverityList = new ArrayList();
		List uniqueServiceItemSeverityList = new ArrayList();
		List uniqueEvidence5List = null;
		List uniqueEvidence6List = null;
		List uniqueEvidence4List = null;
		List uniqueAssetxxList = null;
		List uniqueAssetyyyList = null;
		List uniqueAssetzzzList = null;
		String collectionName = "";
		String qualifier8 = ""; //To get the category kind of items from rotatingassets etc
		//ReportOptionsDetails uniqueQualifier8List = null;
		List uniqueQualifier8List = new ArrayList();
		DBCollection collection = null;
		//Depends upon contextType we need to take uptablecolumn and pass in update jsp
				String updatable_fields_json_filename = "Updatable_Fields_" + contextType + ".json";
				
				updatable_fields_json_filename = MongoDBReportDAO.getJsonFileNames(updatable_fields_json_filename, contextType);
				
				List updatable_columns = MongoDBReportDAO.getUpdatableColumns(updatable_fields_json_filename, session);
				
				session.setAttribute("updatableColumns", updatable_columns);

		try {
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
			contextTypeCollectionNames.add("items_collection");
			contextTypeCollectionNames.add("items_equipments_collection");
		}
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			contextTypeCollectionNames.add("items_collection");
		}
		//session.setAttribute("collectionNames", contextTypeCollectionNames); Moving this only when collections are available
		//Get unique details list
		
		//set uniquevalues of updatable items for all contexts
		//As incharge,price,articleno,productname column is unique for rotating_assets alone we are checking like this
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
			for (int i = 0; i < contextTypeCollectionNames.size(); i++) {
				collectionName = contextTypeCollectionNames.get(i).toString();
				//check whether that collection is not empty. if not empty it is progressed further
				collection = customerDatabase.getCollection(collectionName);
				DBCursor cursor = collection.find();
				if (cursor.count() > 0) {
					availableCollectionsForAdminFunctions.add(collectionName);
				uniqueEvidence5List = new ArrayList();
				uniqueEvidence6List = new ArrayList();
				uniqueEvidence4List = new ArrayList();
				uniqueItemAList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"A");
				uniqueItemBList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"B");
				uniqueItemCList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"C");
				uniqueItemDList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"D");
				 if (collectionName.equalsIgnoreCase("items_collection")) { 
						qualifier8 = "Category";
				} else {
					 	qualifier8 = "Category_equipment";
					 	 }  
				//uniqueQualifier8List = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",qualifier8,customerDatabase);
				 uniqueQualifier8List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Qualifier_8");
				
				uniqueEvidence4List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_4");
				
				uniqueEvidence5List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_5");
				
				uniqueEvidence6List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_6");
				
				uniqueMissingItemSeverityList = new ArrayList();
				uniqueServiceItemSeverityList = new ArrayList();
				//uniqueEvidenceIdentifierList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_Identifier");
				uniqueEvidence3List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_3");
				uniqueEvidence2List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_2");
				
				
				uniqueMissingItemSeverityList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Qualifier_2");
				uniqueServiceItemSeverityList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Qualifier_1");
				
				if (collectionName.equalsIgnoreCase("items_collection")) {
					session.setAttribute("items_collection_a_value", uniqueItemAList);
					session.setAttribute("items_collection_b_value", uniqueItemBList);
					session.setAttribute("items_collection_c_value", uniqueItemCList);
					session.setAttribute("items_collection_d_value", uniqueItemDList);
					session.setAttribute("items_collection_qualifier8_value", uniqueQualifier8List);
					
					session.setAttribute("items_collection_added_stauts", uniqueEvidence5List);
					session.setAttribute("items_collection_deleted_status", uniqueEvidence6List);
					session.setAttribute("items_collection_scanned_status", uniqueEvidence4List);
					
					//session.setAttribute("items_collection_evidence_identifier", uniqueEvidenceIdentifierList);
					session.setAttribute("items_collection_service_text", uniqueEvidence2List);
					session.setAttribute("items_collection_missing_text", uniqueEvidence3List);
					
					session.setAttribute("items_collection_service_color", uniqueServiceItemSeverityList);
					session.setAttribute("items_collection_missing_color", uniqueMissingItemSeverityList);
					//If required, we need to find out newly added column, currently not progressing -- Sep 2
					/*List newlyAddedColumns = (ArrayList)session.getAttribute("newlyaddedfields");
					for (int i = 0; i < newlyAddedColumns.size();i++) {
						
					}*/
				}
				if (collectionName.equalsIgnoreCase("items_equipments_collection")) {
					session.setAttribute("items_equipments_collection_a_value", uniqueItemAList);
					session.setAttribute("items_equipments_collection_b_value", uniqueItemBList);
					session.setAttribute("items_equipments_collection_c_value", uniqueItemCList);
					session.setAttribute("items_equipments_collection_d_value", uniqueItemDList);
					session.setAttribute("items_equipments_collection_qualifier8_value", uniqueQualifier8List);
					
					session.setAttribute("items_equipments_collection_added_stauts", uniqueEvidence5List);
					session.setAttribute("items_equipments_collection_deleted_status", uniqueEvidence6List);
					session.setAttribute("items_equipments_collection_scanned_status", uniqueEvidence4List);
					
					//session.setAttribute("items_equipments_collection_evidence_identifier", uniqueEvidenceIdentifierList);
					session.setAttribute("items_equipments_collection_service_text", uniqueEvidence2List);
					session.setAttribute("items_equipments_collection_missing_text", uniqueEvidence3List);
					
					session.setAttribute("items_equipments_collection_service_color", uniqueServiceItemSeverityList);
					session.setAttribute("items_equipments_collection_missing_color", uniqueMissingItemSeverityList);

				}
			}
			}
			
		}  //for rotating_assets
		
		//for machine_uptime , code change on sep 3
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			collectionName = "items_collection"; //As there is only one collection, we are giving here as items_collection itself
			collection = customerDatabase.getCollection(collectionName);
			DBCursor cursor = collection.find();
			if (cursor.count() > 0) {
				
				availableCollectionsForAdminFunctions.add(collectionName);
			uniqueEvidence2List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_2");
			uniqueEvidence3List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_3");
			uniqueEvidence4List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_4");
			uniqueAssetxxList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Asset_Xxx");
			uniqueAssetyyyList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Asset_Yyy");
			uniqueAssetzzzList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Asset_Zzz");
			session.setAttribute("items_collection_reasons_value", uniqueEvidence2List);
			session.setAttribute("items_collection_machine_downtime_value", uniqueEvidence3List);
			session.setAttribute("items_collection_action_value", uniqueEvidence4List);
			
			session.setAttribute("items_collection_Machine_Name_value", uniqueAssetxxList);
			session.setAttribute("items_collection_Supervisor_Name_value", uniqueAssetyyyList);
			session.setAttribute("items_collection_remoteunit_value", uniqueAssetzzzList);
			}
		}
		//Evidence_Identifier is common across all apptypes
		for (int i = 0; i < availableCollectionsForAdminFunctions.size(); i++) {
			collectionName = availableCollectionsForAdminFunctions.get(i).toString();
		uniqueEvidenceIdentifierList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_Identifier");
		if (collectionName.equalsIgnoreCase("items_collection")) {
		session.setAttribute("items_collection_evidence_identifier", uniqueEvidenceIdentifierList);
		}
		if (collectionName.equalsIgnoreCase("items_equipments_collection")) {
		session.setAttribute("items_equipments_collection_evidence_identifier", uniqueEvidenceIdentifierList);
		}
		}
		if (availableCollectionsForAdminFunctions != null) {
		session.setAttribute("collectionNames", availableCollectionsForAdminFunctions);
		}
		

	}catch(Exception ex) {
		
	}
	}

	public static void setUniqueValues(String contextType,HttpSession session,DB customerDatabase,String updatedColumn,String collectionName) {
		//Available collections to query
		List contextTypeCollectionNames = new ArrayList();	
		List uniqueItemDList = null;
		List uniqueItemCList = null;
		List uniqueItemAList = null;
		List uniqueItemBList = null;
		List uniqueEvidenceIdentifierList = null;
		List uniqueEvidence3List = null;
		List uniqueEvidence2List = null;
		List uniqueMissingItemSeverityList = null;
		List uniqueServiceItemSeverityList = null;
		List uniqueEvidence5List = null;
		List uniqueEvidence6List = null;
		List uniqueEvidence4List = null;
		String qualifier8 = ""; //To get the category kind of items from rotatingassets etc
		List uniqueQualifier8List = null;
		List newlyAddedFieldList = null;
		
		List uniqueAssetxxList = null;
		List uniqueAssetyyyList = null;
		List uniqueAssetzzzList = null;

		try {

		//Get unique details list
		
		//set uniquevalues of updatable items for all contexts
		//As incharge,price,articleno,productname column is unique for rotating_assets alone we are checking like this
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
			//for (int i = 0; i < contextTypeCollectionNames.size(); i++) {
				//collectionName = contextTypeCollectionNames.get(i).toString();
				if (updatedColumn.equalsIgnoreCase("ArticleNumber")) {
					uniqueItemAList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"A");
				}
				else if (updatedColumn.equalsIgnoreCase("ProductName")) {
				uniqueItemBList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"B");
				}
				else if (updatedColumn.equalsIgnoreCase("price")) {
				uniqueItemCList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"C");
				}
				else if (updatedColumn.equalsIgnoreCase("Incharge")) {
				uniqueItemDList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"D");
				}
				if (updatedColumn.equalsIgnoreCase("Category")) {
				 if (collectionName.equalsIgnoreCase("items_collection")) { 
						qualifier8 = "Category";
				} else {
					 	qualifier8 = "Category_equipment";
					 	 }
				uniqueQualifier8List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Qualifier_8");
				}
				
				else if (updatedColumn.equalsIgnoreCase("Scanned_Item")) {
					uniqueEvidence4List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_4");
				}
				
				else if (updatedColumn.equalsIgnoreCase("Added_Item")) {
					uniqueEvidence5List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_5");
				}
				else if (updatedColumn.equalsIgnoreCase("Deleted_Item")) {
					uniqueEvidence6List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_6");
				}
				else if (updatedColumn.equalsIgnoreCase("Missing_Item_Text")) {
					uniqueEvidence3List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_3");
				}
				else if (updatedColumn.equalsIgnoreCase("To_Service_Item_Text")) {
				uniqueEvidence2List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_2");
				}
				else if (updatedColumn.equalsIgnoreCase("Missing_Item_Color")) {
					uniqueMissingItemSeverityList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Qualifier_2");
					}
				else if (updatedColumn.equalsIgnoreCase("To_Service_Item_Color")) {
					uniqueServiceItemSeverityList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Qualifier_1");
					}
				else {
					newlyAddedFieldList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,updatedColumn);
				}
				
				if (collectionName.equalsIgnoreCase("items_collection")) {
					if (uniqueItemAList != null) {
						session.setAttribute("items_collection_a_value", uniqueItemAList);
					}
					if (uniqueItemBList != null) {
					session.setAttribute("items_collection_b_value", uniqueItemBList);
					}
					if (uniqueItemCList != null) {
					session.setAttribute("items_collection_c_value", uniqueItemCList);
					}
					if (uniqueItemDList != null) {
					session.setAttribute("items_collection_d_value", uniqueItemDList);
					}
					
					if (uniqueEvidence4List != null) {
						session.setAttribute("items_collection_scanned_status",  uniqueEvidence4List);
					}
					if (uniqueEvidence5List != null) {
					session.setAttribute("items_collection_added_stauts", uniqueEvidence5List);
					}
					if (uniqueEvidence6List != null) {
					session.setAttribute("items_collection_deleted_status", uniqueEvidence6List);
					}
					if (uniqueQualifier8List != null) {
						session.setAttribute("items_collection_qualifier8_value", uniqueQualifier8List);
					}
					if (newlyAddedFieldList != null) {
						session.setAttribute(collectionName + "_" + updatedColumn, newlyAddedFieldList);
					}
					if (uniqueEvidence2List != null) {
						session.setAttribute("items_collection_service_text", uniqueEvidence2List);
					}
					if (uniqueEvidence3List != null) {
					session.setAttribute("items_collection_missing_text", uniqueEvidence3List);
					}
					if (uniqueServiceItemSeverityList != null) {
					session.setAttribute("items_collection_service_color", uniqueServiceItemSeverityList);
					}
					if (uniqueMissingItemSeverityList != null) {
					session.setAttribute("items_collection_missing_color", uniqueMissingItemSeverityList);
					}
				}
				if (collectionName.equalsIgnoreCase("items_equipments_collection")) {
					if (uniqueItemAList != null) {
					session.setAttribute("items_equipments_collection_a_value", uniqueItemAList);
					}
					if (uniqueItemBList != null) {
					session.setAttribute("items_equipments_collection_b_value", uniqueItemBList);
					}
					if (uniqueItemCList != null) {
					session.setAttribute("items_equipments_collection_c_value", uniqueItemCList);
					}
					if (uniqueItemDList != null) {
					session.setAttribute("items_equipments_collection_d_value", uniqueItemDList);
					}
					
					if (uniqueEvidence4List != null) {
						session.setAttribute("items_equipments_collection_scanned_status",  uniqueEvidence4List);
					}
					if (uniqueEvidence5List != null) {
					session.setAttribute("items_equipments_collection_added_stauts", uniqueEvidence5List);
					}
					if (uniqueEvidence6List != null) {
					session.setAttribute("items_equipments_collection_deleted_status", uniqueEvidence6List);
					}
					if (uniqueQualifier8List != null) {
						session.setAttribute("items_equipments_collection_qualifier8_value", uniqueQualifier8List);
					}
					if (newlyAddedFieldList != null) {
						session.setAttribute(collectionName + "_" + updatedColumn, newlyAddedFieldList);
					}
					if (uniqueEvidence2List != null) {
						session.setAttribute("items_equipments_collection_service_text", uniqueEvidence2List);
						}
						if (uniqueEvidence3List != null) {
						session.setAttribute("items_equipments_collection_missing_text", uniqueEvidence3List);
						}
						if (uniqueServiceItemSeverityList != null) {
						session.setAttribute("items_equipments_collection_service_color", uniqueServiceItemSeverityList);
						}
						if (uniqueMissingItemSeverityList != null) {
						session.setAttribute("items_equipments_collection_missing_color", uniqueMissingItemSeverityList);
						}

				}
				

		}  //for rotating_assets
		
		//for machine_uptime , code change on sep 3
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			collectionName = "items_collection"; //As there is only one collection, we are giving here as items_collection itself
			if (updatedColumn.equalsIgnoreCase("Reasons")) {
			uniqueEvidence2List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_2");
			}
			else if (updatedColumn.equalsIgnoreCase("Machine DownTime")) {
			uniqueEvidence3List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_3");
			}
			else if (updatedColumn.equalsIgnoreCase("Action")) {
			uniqueEvidence4List = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Evidence_4");
			}
			else if (updatedColumn.equalsIgnoreCase("Machine Name")) {
			uniqueAssetxxList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Asset_Xxx");
			}
			else if (updatedColumn.equalsIgnoreCase("Supervisor Name")) {
			uniqueAssetyyyList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Asset_Yyy");
			}
			else if (updatedColumn.equalsIgnoreCase("remoteunit")) {
			uniqueAssetzzzList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,"Asset_Zzz");
			}
			if (uniqueEvidence2List != null) {
				session.setAttribute("items_collection_reasons_value", uniqueEvidence2List);
			}
			if (uniqueEvidence3List != null) {
			session.setAttribute("items_collection_machine_downtime_value", uniqueEvidence3List);
			}
			if (uniqueEvidence4List != null) {
			session.setAttribute("items_collection_action_value", uniqueEvidence4List);
			}
			if (uniqueAssetxxList != null) {
			session.setAttribute("items_collection_Machine_Name_value", uniqueAssetxxList);
			}
			if (uniqueAssetyyyList != null) {
			session.setAttribute("items_collection_Supervisor_Name_value", uniqueAssetyyyList);
			}
			if (uniqueAssetzzzList != null) {
			session.setAttribute("items_collection_remoteunit_value", uniqueAssetzzzList);
			}
		}
		


	}catch(Exception ex) {
		
	}
	}
	public static boolean checkIfExisting(String fieldThatIsGettingAdded,List updatableColumns) {
		boolean addingColumnAlreadyPresent = false;
		for (int idx = 0; idx < updatableColumns.size();idx++) {
			if (updatableColumns.get(idx).toString().equalsIgnoreCase(fieldThatIsGettingAdded)) {
				addingColumnAlreadyPresent = true;
				break;
			}
		}
		return addingColumnAlreadyPresent;
	}
	//check whether the newly added column already present or not and if not update json and session
	public static void updateJsonAndUpdatableColumnValues(String contextType,String fieldThatIsGettingAdded,
			HttpSession session) {
		boolean addingColumnAlreadyPresent = false; //In this case, only incrementation of value is seen in the column, i.e. update alone occurs
	String updatable_fields_json_filename = "Updatable_Fields_" + contextType + ".json";
	List updatableColumns = (ArrayList)session.getAttribute("updatableColumns");
	addingColumnAlreadyPresent = checkIfExisting(fieldThatIsGettingAdded,updatableColumns);
	/*for (int idx = 0; idx < updatableColumns.size();idx++) {
		if (updatableColumns.get(idx).toString().equalsIgnoreCase(fieldThatIsGettingAdded)) {
			addingColumnAlreadyPresent = true;
			break;
		}
	}*/
	if (!addingColumnAlreadyPresent) {
	updatable_fields_json_filename = MongoDBReportDAO.getJsonFileNames(updatable_fields_json_filename, contextType);
	
	List updatable_columns = MongoDBReportDAO.appendNewlyAddedUpdatableColumns(updatable_fields_json_filename,fieldThatIsGettingAdded);
	session.setAttribute("updatableColumns", updatable_columns);
	}
	}
	//Method to remove column in json when a column is removed
	public static void refreshUpdatableColumns(String contextType,List updatableColumns
			) {
		String updatable_fields_json_filename = "Updatable_Fields_" + contextType + ".json";
		updatable_fields_json_filename = MongoDBReportDAO.getJsonFileNames(updatable_fields_json_filename, contextType);
		org.json.simple.JSONArray list = new org.json.simple.JSONArray();
		try {
		for (int i = 0; i < updatableColumns.size();i++) {
			JSONObject obj = new JSONObject();			
			obj.put("Option",updatableColumns.get(i).toString());			
			list.add(obj);
		}
		org.json.simple.JSONObject modifiedObject = new org.json.simple.JSONObject();
		modifiedObject.put("updatable_fields", list);
		

			FileWriter file = new FileWriter(updatable_fields_json_filename);
			file.write(modifiedObject.toJSONString());
			file.flush();
			file.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//Method to append newly added columns
	public static List appendNewlyAddedUpdatableColumns(String updatableJson,String fieldThatIsGettingAdded) {
		JSONParser parser = new JSONParser();
		 org.json.simple.JSONArray updatableColumnJsonArray = new  org.json.simple.JSONArray();
		List updatableColumnList = new ArrayList();
		try {
			 
	            Object obj = parser.parse(new FileReader(updatableJson));
	                    
	            
	            org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
	 		
		
			updatableColumnJsonArray = (org.json.simple.JSONArray) jsonObject.get("updatable_fields");
			
			JSONObject newObject = new JSONObject();
			newObject.put("Option",fieldThatIsGettingAdded);
			updatableColumnJsonArray.add(newObject);
			org.json.simple.JSONObject modifiedObject = new org.json.simple.JSONObject();
			modifiedObject.put("updatable_fields", updatableColumnJsonArray);
			try {

				FileWriter file = new FileWriter(updatableJson);
				file.write(modifiedObject.toJSONString());
				file.flush();
				file.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			Iterator<String> iterator = updatableColumnJsonArray.iterator();
           while (iterator.hasNext()) {
               updatableColumnList.add(iterator.next());
           }
           JSONArray optionsJson = new JSONArray(updatableColumnList);
   		updatableColumnList = new ArrayList();
   		
   		
   		for (int i = 0; i < optionsJson.length(); i++) {
   			JSONObject json = new JSONObject(optionsJson.getString(i));
   			updatableColumnList.add(json.getString("Option"));
   		}
           
		} catch(Exception ex) {
			System.out.println("exception.." + ex.getMessage());
		}
           return updatableColumnList;           
	}
	public static List getUpdatableColumns(String updatableJson, HttpSession session) {
		JSONParser parser = new JSONParser();
		 org.json.simple.JSONArray updatableColumnJsonArray = new  org.json.simple.JSONArray();
		List updatableColumnList = new ArrayList();
		List newlyAddedFieldsList = new ArrayList();
		try {
			 
	            Object obj = parser.parse(new FileReader(updatableJson));
	                    
	            
	            org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
	            updatableColumnJsonArray = (org.json.simple.JSONArray) jsonObject.get("updatable_fields");
			Iterator<String> iterator = updatableColumnJsonArray.iterator();
            while (iterator.hasNext()) {
                updatableColumnList.add(iterator.next());
            }
		
		JSONArray optionsJson = new JSONArray(updatableColumnList);
		updatableColumnList = new ArrayList();
		
		
		for (int i = 0; i < optionsJson.length(); i++) {
			JSONObject json = new JSONObject(optionsJson.getString(i));
			updatableColumnList.add(json.getString("Option"));
			//Need to check type here. Will get implemented if it is concluded
			/*if (json.getString("type").equalsIgnoreCase("newfield")) {
				newlyAddedFieldsList.add(json.getString("Option"));
			}*/
		}
		//session.setAttribute("newlyaddedfields", newlyAddedFieldsList); //if above commented flow is there, this will be created
	            
		} catch(Exception ex) {
			System.out.println("exception.." + ex.getMessage());
		}
		return updatableColumnList;
	}
	
	// Metajson Context_Reference_Example, Evidence_2, Evidence_3 etc., string
		// displayed as output
		public static String getConnectingLinkNames(String collectionName,
				String evidenceRecordSplitter, String linkName, String linkName1,
				DB ponniviDb) {
			String connectorLink = "";
			
			if (ponniviDb != null) {
				DBCollection metaJsonCollection = ponniviDb
						.getCollection(collectionName);

				DBCursor metaJsonCursor = metaJsonCollection.find();

				BasicDBObject metaJsonDbObject = (BasicDBObject) metaJsonCursor
						.next();
				try {
					while (metaJsonDbObject != null) {

						BasicDBObject evidenceRecord = (BasicDBObject) metaJsonDbObject
								.get(evidenceRecordSplitter);
						if (linkName.equalsIgnoreCase("Context_Reference_Example")) {
							connectorLink = evidenceRecord.getString(linkName);
						} else {
							com.mongodb.BasicDBList metaJsonDbListItems = (com.mongodb.BasicDBList) evidenceRecord
									.get(linkName);
							Iterator iter = metaJsonDbListItems.iterator();
							while (iter.hasNext()) {
								BasicDBObject obj = (BasicDBObject) iter.next();
								connectorLink = obj.getString(linkName1);
								if (connectorLink != null) {
									// System.out.println("connectorLink.."
									// + connectorLink);
									break;
								}
							}
						}

						metaJsonDbObject = (BasicDBObject) (metaJsonCursor
								.hasNext() ? metaJsonCursor.next() : null);

					}

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					metaJsonCursor.close();
				}
			}
			return connectorLink;
		}
	
	public static String getJsonFileNames(String type, String contextType) {
		String jsonFileName = "";
		String userOperatingSystem = System.getProperty("os.name");		
		//if (type.equalsIgnoreCase("Updatable_Fields_Rotating_assets.json")) {
		//Need to continue here
		if (contextType.equalsIgnoreCase("Rotating_assets") &&
				type.equalsIgnoreCase("Updatable_Fields_Rotating_assets.json")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\options_array.json";				
				jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\updatable_fields_rotating_assets.json";
				//jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\generictype\\instaunitereports\\src\\main\\webapp\\updatable_fields_rotating_assets.json";
			}
			else {
				jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/updatable_fields_rotating_assets.json";
			}
		}
		if (contextType.equalsIgnoreCase("machine_uptime") &&
				type.equalsIgnoreCase("Updatable_Fields_machine_uptime.json")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\options_array.json";				
				jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\updatable_fields_machine_uptime.json";
				//jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\generictype\\instaunitereports\\src\\main\\webapp\\updatable_fields_rotating_assets.json";
			}
			else {
				jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/updatable_fields_machine_uptime.json";
			}
		}
		if (type.equalsIgnoreCase("options_array.json")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\options_array.json";				
				jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\options_array.json";
			}
			else {
				jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/options_array.json";
			}
		}
		return jsonFileName;		
	}

	
	// Get distinct fields
		public static List getDistinct(DB ponniviDb, String collectionName,
				String distinctFieldName)
				throws UnknownHostException {

			//ponniviDb = mongo.getDB(dbName);
			System.out.println("collectionName.."+collectionName);
			DBCollection Collection = ponniviDb.getCollection(collectionName);

			// call distinct method and store the result in list l1
			List distinctList = Collection.distinct(distinctFieldName);

			// iterate through the list and print the elements
			for (int i = 0; i < distinctList.size(); i++) {
				System.out.println(distinctList.get(i));
			}

			return distinctList;

		}
		
		//Get distinct list based upon evidence identifier
		public static List getDistinct(DB ponniviDb, String collectionName,
				String distinctFieldName,String checkFieldName, String checkingValue)
				throws UnknownHostException {

			//ponniviDb = mongo.getDB(dbName);
			System.out.println("collectionName.."+collectionName);
			DBCollection Collection = ponniviDb.getCollection(collectionName);

			// call distinct method and store the result in list l1
			BasicDBObject searchObject = new BasicDBObject();
			searchObject
					.put(checkFieldName, checkingValue);
			List distinctList = Collection.distinct(distinctFieldName,searchObject);

			// iterate through the list and print the elements
			for (int i = 0; i < distinctList.size(); i++) {
				System.out.println(distinctList.get(i));
			}

			return distinctList;

		}
		
		//Get distinct fields based upon evidence_identifier and article no  -- This is an example
		/*public static List getDistinctFieldNames(DB ponniviDb, String collectionName,
				String param1Name, String param1Value,String param2Name, String param2Value)
				throws UnknownHostException {

			//ponniviDb = mongo.getDB(dbName);

			DBCollection collection = ponniviDb.getCollection(collectionName);
			
			DBObject evidenceIdentifierClause = addEvidenceIdentifier(
					param1Name, param1Value,
					null);
			BasicDBObject searchObject = new BasicDBObject();
			searchObject.put(param2Name, param2Value);

			BasicDBList resultList = new BasicDBList();
			resultList.add(searchObject);
			resultList.add(evidenceIdentifierClause);			
			DBObject query = new BasicDBObject("$and", resultList);
			DBCursor cursor = collection.find(query);
			while (cursor.hasNext()) {
				// System.out.println(cursor.next());
				DBObject o = cursor.next();
				resultSet.add(o);
			}



			return distinctList;

		}*/
		
		
		//Aggregate functions
		public static AggregationOutput getAggregateFunctionsForMachineUptime(DB ponniviDb, String collectionName, String evidence) {
			AggregationOutput output = null;
			if (ponniviDb != null) {
				DBCollection Collection = ponniviDb.getCollection(collectionName);			 
				 /*DBObject mostSightedReason = new BasicDBObject("$group", new BasicDBObject("_id", "$" + evidence)
				 .append("maximum", new BasicDBObject("$sum", 1)));*/
				 
				 DBObject groupFields = new BasicDBObject("_id","$" + evidence);
				    groupFields.put("times", new BasicDBObject("$sum",1));
				    DBObject group = new BasicDBObject("$group", groupFields);
				    DBObject sort = new BasicDBObject("$sort", new BasicDBObject("times",-1) );
				    DBObject limit = new BasicDBObject("$limit", 1 );
				    
				    output = Collection.aggregate(group, sort);

				    for (DBObject result : output.results()) {
						  System.out.println(result.get("_id") + ".." + result.get("times"));
						  }
				  
				 		  
			}
			return output;
		}
		
		//Compute total downtime value and average downtime values
		public static void computeTotalDownTimeAndAverageDowntime(AggregationOutput output,HttpSession session) {
			int totalDownTime = 0, timeFrame = 0, occurences = 0, totalOccurences = 0; 
			 for (DBObject result : output.results()) {
				  System.out.println(result.get("_id") + ".." + result.get("times"));
				  occurences = Integer.parseInt(result.get("times").toString());
				  if (result.get("_id").toString().equalsIgnoreCase("Less than a minute")) {
					  timeFrame = 1;				  
				  }
				  if (result.get("_id").toString().equalsIgnoreCase("Less than 5 mins")) {
					  timeFrame = 5;
				  }
				  if (result.get("_id").toString().equalsIgnoreCase("Less than 15 mins")) {
					  timeFrame = 15;
				  }
				  if (result.get("_id").toString().equalsIgnoreCase("Less than 30 mins")) {
					  timeFrame = 30;
				  }
				  if (result.get("_id").toString().equalsIgnoreCase("Less than 1 hr")) {
					  timeFrame = 59;
				  }
				  if (result.get("_id").toString().equalsIgnoreCase("More than 1 hr")) {
					  timeFrame = 60;
				  }
				  if (result.get("_id").toString().equalsIgnoreCase("More than a shift")) {
					  timeFrame = 480;  //i.e. 8 hours
				  }
				  totalOccurences += occurences;
				  totalDownTime += (timeFrame * occurences);
				  }
			 session.setAttribute("totalDownTime", totalDownTime);
			 int averageDownTime = totalDownTime / totalOccurences;
			 session.setAttribute("averageDownTime", averageDownTime);
			
		}





	

	public static ReportOptionsDetails getParameterValuesOrConditions(
			String collectionName, String spliiter, String paramName,
			DB ponniviDb) {
		String connectorLink = "";
		//ponniviDb = mongo.getDB(ponniviDb);
		List<String> optionsList = new ArrayList<String>();
		ReportOptionsDetails reportOptionDetails = new ReportOptionsDetails();
		if (ponniviDb != null) {
			DBCollection optionsArrayCollection = ponniviDb
					.getCollection(collectionName);

			DBCursor optionsArrayCursor = optionsArrayCollection.find();

			BasicDBObject optionsArrayDbObject = (BasicDBObject) optionsArrayCursor
					.next();

			try {
				while (optionsArrayDbObject != null) {

					BasicDBObject valuesOrConditionsRecord = (BasicDBObject) optionsArrayDbObject
							.get(spliiter);
					com.mongodb.BasicDBList valuesDbListItems = null;
					com.mongodb.BasicDBList conditionsDbListItems = null;
					String paramNameValues = paramName + "_values";
					String paramNameConditions = paramName + "_conditions";
					valuesDbListItems = (com.mongodb.BasicDBList) valuesOrConditionsRecord
							.get(paramNameValues);

					conditionsDbListItems = (com.mongodb.BasicDBList) valuesOrConditionsRecord
							.get(paramNameConditions);
					Iterator iter = null;
					if (valuesDbListItems != null) {
					 iter = valuesDbListItems.iterator();
					while (iter.hasNext()) {
						BasicDBObject obj = (BasicDBObject) iter.next();
						// System.out
						// .println("string.." + obj.getString("Option"));
						optionsList.add(obj.getString("Option"));
					}
					reportOptionDetails.setValuesList(optionsList);
					}
					if (conditionsDbListItems != null) {
					iter = conditionsDbListItems.iterator();
					optionsList = new ArrayList();
					while (iter.hasNext()) {
						BasicDBObject obj = (BasicDBObject) iter.next();
						// System.out
						// .println("string.." + obj.getString("Option"));
						optionsList.add(obj.getString("Option"));
					}
					reportOptionDetails.setConditionsList(optionsList);
					}
					break;
				}
				optionsArrayDbObject = (BasicDBObject) (optionsArrayCursor
						.hasNext() ? optionsArrayCursor.next() : null);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				optionsArrayCursor.close();
			}
		}
		return reportOptionDetails;
	}
	
	public static String getCollectionSpecificDetails(DB ponniviDb, String collectionName,String fieldName,String fieldValue) {
		String result = "";
	//	ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);
			BasicDBObject searchObject = new BasicDBObject();
			searchObject.put(fieldName,
					fieldValue);
			DBCursor cursor = collection.find(searchObject);
			while (cursor.hasNext()) {
				// System.out.println(cursor.next());
				DBObject dbObject = cursor.next();
				if (collectionName.equalsIgnoreCase("customer_definition_collection")) {
					result = (String)dbObject.get("CustomerDbName");
				}
			}			
		}
		return result;
	}
	
	public static long getCollectionCount(String collectionName,DB ponniviDb) {
		//ponniviDb = mongo.getDB(dbName);
		long count = 0;
		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);

			count = collection.getCount();

		}
		return count;

	}

	public static String getTagValues(String collectionName, String spliiter,
			String contextReferenceExample, DB ponniviDb, String paramName,
			String reportName) {
		//ponniviDb = mongo.getDB(dbName);
		String tagValue = "";
		String contextReference = "";
		if (collectionName.equalsIgnoreCase("report_param_collection")) {
			if (paramName.equalsIgnoreCase("Missing_Item_Text") ||
					paramName.equalsIgnoreCase("Missing_Item_Color")) {
				paramName = "Missing_Item";
			}
			if (paramName.equalsIgnoreCase("To_Service_Item_Text") ||
					paramName.equalsIgnoreCase("To_Service_Item_Color")) {
				paramName = "To_Service_Item";
			}
			paramName = paramName + "_type";
		}

		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);

			DBCursor cursor = collection.find();

			BasicDBObject dbObject = (BasicDBObject) cursor.next();

			try {
				while (dbObject != null) {

					BasicDBObject record = (BasicDBObject) dbObject
							.get(spliiter);
					if (collectionName
							.equalsIgnoreCase("evidence_description_collection")) {
						tagValue = record.getString(contextReferenceExample);
						if (tagValue.equalsIgnoreCase(reportName)) {
							tagValue = record.getString(paramName);
						}
					} else if (collectionName
							.equalsIgnoreCase("evidences_collection")) {
						if (paramName.equalsIgnoreCase("Evidence_Identifier")) {
							com.mongodb.BasicDBObject dbTimeObject = (com.mongodb.BasicDBObject) record
									.get("Time_Location_Details");
							tagValue = dbTimeObject
									.getString("Evidence_Identifier");

						} else { // Need to get other items like above if
									// required -- June 2
							tagValue = record.getString(paramName);
						}
					}
					else if (collectionName
							.equalsIgnoreCase("report_param_collection")) {  //Added on Aug 19 to find the datatype of price_type and others as it is not working properly
						
						tagValue = record.getString(paramName);
					}

					dbObject = (BasicDBObject) (cursor.hasNext() ? cursor
							.next() : null);
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				cursor.close();
			}

		}
		return tagValue;
	}



	public static List<DBObject> retrieveReportWithAndOr(String collectionName,
			String evidenceRecordSplitter, String linkName, DB ponniviDb,
			String param1Name, String param1Condition, String param1Value,
			String param2Name, String param2Condition, String param2Value,
			String param3Name, String param3Condition, String param3Value,
			String evidenceIdentifier, String evidenceIdentifierValue,
			List evidenceIdentifierListItems, String logoutTime,
			String fromDateValue, String toDateValue, String contextType) {
		
			

		List<DBObject> resultSet = new ArrayList<DBObject>();
		//ponniviDb = mongo.getDB(dbName);

		String param1DataType = getTagValues("report_param_collection",
				"values_and_conditions", "", ponniviDb, param1Name, "");
		String param2DataType = "", param3DataType = "";
		if (!param2Name.equalsIgnoreCase("NULL")) {
			param2DataType = getTagValues("report_param_collection",
					"values_and_conditions", "", ponniviDb, param2Name, "");
		}
		if (!param3Name.equalsIgnoreCase("NULL")) {
			param3DataType = getTagValues("report_param_collection",
					"values_and_conditions", "", ponniviDb, param3Name, "");
		}

		// System.out.println("param1DataType.."
		// + param1DataType + "param2DataType" + param2DataType +
		// "param3DataType" + param3DataType);

		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);
			String linkNameToMatch = "";
			linkNameToMatch = getLinkNames(param1Name, contextType);


			BasicDBList resultList = new BasicDBList(); // final resultList

			// From and to date parameters add applicable for all type of search
			// conditions -- June 10
			BasicDBObject dateParameterClause = addDateParameters(
					fromDateValue, toDateValue, logoutTime);
			if (dateParameterClause == null) {
				resultSet = null;
			} else { // If date parameter in dd-MM-yy is correct, proceed for
						// the following

				// Add evidenceIdentifier applicable for all type of search
				// conditions
				DBObject evidenceIdentifierClause = addEvidenceIdentifier(
						evidenceIdentifier, evidenceIdentifierValue,
						evidenceIdentifierListItems);

				BasicDBObject searchObject = new BasicDBObject();

				// filter according to param1Name alone
				if (!param1Name.equalsIgnoreCase("")
						&& param2Name.equalsIgnoreCase("NULL")
						&& param3Name.equalsIgnoreCase("NULL")) {

					if (param1Condition.equalsIgnoreCase("<")) {
						searchObject.put(linkNameToMatch, new BasicDBObject(
								"$lt", Integer.parseInt(param1Value)));
					}
					if (param1Condition.equalsIgnoreCase(">=")) {
						searchObject.put(linkNameToMatch, new BasicDBObject(
								"$gte", Integer.parseInt(param1Value)));
					}
					if (param1Condition.equalsIgnoreCase("=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchObject.put(linkNameToMatch,
									Integer.parseInt(param1Value));
						} else {
							if (param1Value.equalsIgnoreCase("ALL")) {
								List<String> list = new ArrayList<String>();
								if (linkNameToMatch
										.equalsIgnoreCase("Qualifier_8")) {
								list = getCategoryList(collectionName);

								}
								if (linkNameToMatch
										.equalsIgnoreCase("Qualifier_2")
										|| linkNameToMatch
												.equalsIgnoreCase("Qualifier_1")) {
									list.add("High_Severity");
									list.add("Low_Severity");
									// searchObject.put(linkNameToMatch,new
									// BasicDBObject("$in",list));
								}
								searchObject.put(linkNameToMatch,
										new BasicDBObject("$in", list));
							} else {
								searchObject
										.put(linkNameToMatch,
												new BasicDBObject("$regex",
														param1Value).append(
														"$options", "i"));
							}
						}
					}
					if (param1Condition.equalsIgnoreCase("!=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchObject.put(
									linkNameToMatch,
									new BasicDBObject("$ne", Integer
											.parseInt(param1Value)));
						} else {
							searchObject.put(linkNameToMatch,
									new BasicDBObject("$ne", param1Value));
						}
					}
					if (param1Condition.equalsIgnoreCase("LIKE")) {
						searchObject.put(linkNameToMatch, new BasicDBObject(
								"$regex", param1Value).append("$options", "i"));
					}
					if (param1Condition.equalsIgnoreCase("NOT LIKE")) { // Here
																		// we
																		// should
																		// change
																		// condition
																		// --
																		// May
																		// 20
						searchObject.put(linkNameToMatch, new BasicDBObject(
								"$regex", param1Value).append("$options", "i"));
					}

					resultList = new BasicDBList();
					resultList.add(searchObject);
					resultList.add(evidenceIdentifierClause);
					resultList.add(dateParameterClause);

					DBObject query = new BasicDBObject("$and", resultList);
					DBCursor cursor = collection.find(query);
					while (cursor.hasNext()) {
						// System.out.println(cursor.next());
						DBObject o = cursor.next();
						resultSet.add(o);
					}
				}

				// when param1 and param2 are present, they are with and
				// condition always as per the requirement
				List<BasicDBObject> searchArguments = new ArrayList<BasicDBObject>();

				if (!param1Name.equalsIgnoreCase("")
						&& !param2Name.equalsIgnoreCase("NULL")
						&& param3Name.equalsIgnoreCase("NULL")) {
					if (param1Condition.equalsIgnoreCase("<")) {
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$lt", Integer
										.parseInt(param1Value))));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase(">=")) {
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$gte", Integer
										.parseInt(param1Value))));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase("=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, Integer
											.parseInt(param1Value)));
						}
						/*
						 * else { searchArguments.add(new
						 * BasicDBObject(linkNameToMatch,param1Value)); }
						 */
						else {
							if (param1Value.equalsIgnoreCase("ALL")) {
								List<String> list = new ArrayList<String>();
								if (linkNameToMatch
										.equalsIgnoreCase("Qualifier_8")) {
									list = getCategoryList(collectionName);
								}
								if (linkNameToMatch
										.equalsIgnoreCase("Qualifier_2")
										|| linkNameToMatch
												.equalsIgnoreCase("Qualifier_1")) {
									list.add("High_Severity");
									list.add("Low_Severity");

								}
								searchArguments.add(new BasicDBObject(
										linkNameToMatch, new BasicDBObject(
												"$in", list)));
							} else {
								searchArguments.add(new BasicDBObject(
										linkNameToMatch, new BasicDBObject(
												"$regex", param1Value).append(
												"$options", "i")));
							}
						}
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase("!=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, new BasicDBObject("$ne",
											Integer.parseInt(param1Value))));
						} else {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, new BasicDBObject("$ne",
											param1Value)));
						}
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					// need to change
					if (param1Condition.equalsIgnoreCase("LIKE")) {
						/*
						 * searchObject.put(linkNameToMatch, new
						 * BasicDBObject("$regex", param1Value)
						 * .append("$options", "i"));
						 */
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$regex", param1Value)
										.append("$options", "i")));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase("NOT LIKE")) { // Here
																		// we
																		// should
																		// change
																		// condition
																		// --
																		// May
																		// 20
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$regex", param1Value)
										.append("$options", "i")));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}

					searchObject.put("$and", searchArguments);

					/*
					 * DBObject evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier,
					 * evidenceIdentifierValue); if
					 * (evidenceIdentifierValue.equalsIgnoreCase("ALL")) {
					 * evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier, new
					 * BasicDBObject("$in",evidenceIdentifierListItems)); } else
					 * { evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier,
					 * evidenceIdentifierValue); }
					 */
					resultList = new BasicDBList();
					resultList.add(searchObject);
					resultList.add(evidenceIdentifierClause);
					resultList.add(dateParameterClause);
					DBObject query = new BasicDBObject("$and", resultList);
					DBCursor logicalQueryResults = collection.find(query);

					resultSet = new ArrayList<DBObject>();
					while (logicalQueryResults.hasNext()) {
						// System.out.println("OR operator Result --->"+logicalQueryResults.next());
						DBObject o = logicalQueryResults.next();
						resultSet.add(o);
					}
				}

				// We may need to generalize this code -- may 20

				if (!param1Name.equalsIgnoreCase("")
						&& param2Name.equalsIgnoreCase("NULL")
						&& !param3Name.equalsIgnoreCase("NULL")) {
					if (param1Condition.equalsIgnoreCase("<")) {
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$lt", Integer
										.parseInt(param1Value))));
						searchArguments = setCondition(param3Condition,
								searchArguments, param3Name, param3Value,
								param3DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase(">=")) {
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$gte", Integer
										.parseInt(param1Value))));
						searchArguments = setCondition(param3Condition,
								searchArguments, param3Name, param3Value,
								param3DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase("=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, Integer
											.parseInt(param1Value)));
						} else {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, param1Value));
						}
						searchArguments = setCondition(param3Condition,
								searchArguments, param3Name, param3Value,
								param3DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase("!=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, new BasicDBObject("$ne",
											Integer.parseInt(param1Value))));
						} else {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, new BasicDBObject("$ne",
											param1Value)));
						}
						searchArguments = setCondition(param3Condition,
								searchArguments, param3Name, param3Value,
								param3DataType,collectionName,contextType);
					}
					// need to change
					if (param1Condition.equalsIgnoreCase("LIKE")) {
						/*
						 * searchObject.put(linkNameToMatch, new
						 * BasicDBObject("$regex", param1Value)
						 * .append("$options", "i"));
						 */searchArguments.add(new BasicDBObject(
								linkNameToMatch, new BasicDBObject("$regex",
										param1Value).append("$options", "i")));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);

					}
					if (param1Condition.equalsIgnoreCase("NOT LIKE")) { // Here
																		// we
																		// should
																		// change
																		// condition
																		// --
																		// May
																		// 20
					/*
					 * searchObject.put(linkNameToMatch, new
					 * BasicDBObject("$regex", param1Value) .append("$options",
					 * "i"));
					 */
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$regex", param1Value)
										.append("$options", "i")));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);

					}
					searchObject.put("$or", searchArguments);
					/*
					 * DBObject evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier,
					 * evidenceIdentifierValue); if
					 * (evidenceIdentifierValue.equalsIgnoreCase("ALL")) {
					 * evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier, new
					 * BasicDBObject("$in",evidenceIdentifierListItems)); } else
					 * { evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier,
					 * evidenceIdentifierValue); }
					 */
					resultList = new BasicDBList();
					resultList.add(searchObject);
					resultList.add(evidenceIdentifierClause);
					resultList.add(dateParameterClause);
					DBObject query = new BasicDBObject("$and", resultList);
					DBCursor logicalQueryResults = collection.find(query);

					resultSet = new ArrayList<DBObject>();
					while (logicalQueryResults.hasNext()) {
						// System.out.println("OR operator Result --->"+logicalQueryResults.next());
						DBObject o = logicalQueryResults.next();
						resultSet.add(o);
					}
				}

				// for all 3 params and or
				if (!param1Name.equalsIgnoreCase("")
						&& !param2Name.equalsIgnoreCase("NULL")
						&& !param3Name.equalsIgnoreCase("NULL")) {
					if (param1Condition.equalsIgnoreCase("<")) {
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$lt", Integer
										.parseInt(param1Value))));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
						// searchArguments = setCondition(param3Condition,
						// searchArguments, param3Name, param3Value,
						// param3DataType);

					}
					if (param1Condition.equalsIgnoreCase(">=")) {
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$gte", Integer
										.parseInt(param1Value))));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
						// searchArguments = setCondition(param3Condition,
						// searchArguments, param3Name, param3Value,
						// param3DataType);
					}
					if (param1Condition.equalsIgnoreCase("=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, Integer
											.parseInt(param1Value)));
						} else {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, param1Value));
						}
						// searchArguments = setCondition(param3Condition,
						// searchArguments, param3Name, param3Value,
						// param3DataType);
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					if (param1Condition.equalsIgnoreCase("!=")) {
						if (param1DataType.equalsIgnoreCase("number")) {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, new BasicDBObject("$ne",
											Integer.parseInt(param1Value))));
						} else {
							searchArguments.add(new BasicDBObject(
									linkNameToMatch, new BasicDBObject("$ne",
											param1Value)));
						}
						// searchArguments = setCondition(param3Condition,
						// searchArguments, param3Name, param3Value,
						// param3DataType);
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);
					}
					// need to change
					if (param1Condition.equalsIgnoreCase("LIKE")) {
						/*
						 * searchObject.put(linkNameToMatch, new
						 * BasicDBObject("$regex", param1Value)
						 * .append("$options", "i"));
						 */searchArguments.add(new BasicDBObject(
								linkNameToMatch, new BasicDBObject("$regex",
										param1Value).append("$options", "i")));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);

					}
					if (param1Condition.equalsIgnoreCase("NOT LIKE")) { // Here
																		// we
																		// should
																		// change
																		// condition
																		// --
																		// May
																		// 20
					/*
					 * searchObject.put(linkNameToMatch, new
					 * BasicDBObject("$regex", param1Value) .append("$options",
					 * "i"));
					 */
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$regex", param1Value)
										.append("$options", "i")));
						searchArguments = setCondition(param2Condition,
								searchArguments, param2Name, param2Value,
								param2DataType,collectionName,contextType);

					}
					searchObject.put("$and", searchArguments);

					// or part
					DBObject orClausePart = setCondition(param3Condition,
							param3Name, param3Value, param3DataType,collectionName,contextType);
					// searchArguments = setCondition(param3Condition,
					// searchArguments, param3Name, param3Value,
					// param3DataType);

					// DBObject clause13 = new BasicDBObject(searchArguments);
					BasicDBList or1 = new BasicDBList();
					or1.add(searchObject);

					or1.add(orClausePart);
					// DBObject query = new BasicDBObject("$or", or);
					DBObject query1 = new BasicDBObject("$or", or1);

					/*
					 * DBObject evidenceIdentifierClause = new
					 * BasicDBObject(evidenceIdentifier,
					 * evidenceIdentifierValue);
					 */
					resultList = new BasicDBList();
					resultList.add(query1);
					resultList.add(evidenceIdentifierClause);
					resultList.add(dateParameterClause);
					DBObject query = new BasicDBObject("$and", resultList);
					DBCursor logicalQueryResults = collection.find(query);

					resultSet = new ArrayList<DBObject>();
					while (logicalQueryResults.hasNext()) {
						// System.out.println("OR operator Result --->"+logicalQueryResults.next());
						DBObject o = logicalQueryResults.next();
						resultSet.add(o);
					}
				}
			}
		}
		return resultSet;
	}
	
	public static void setReportParam(String id,String connectorLink,ReportParameterNames reportParameterNames,List<ReportParameterNames> reportParameterNamesList) {
		reportParameterNames = new ReportParameterNames();
		reportParameterNames.setId(id);
		reportParameterNames
				.setParameterName(connectorLink);
		reportParameterNamesList
				.add(reportParameterNames);
	}
	
	public static List<ReportParameterNames> getReportParameters(
			String collectionName, String evidenceRecordSplitter,
			String linkName, DB ponniviDb) {

		
		String connectorLink = "";
		String contextReferenceExample = "";
		 ReportParameterNames reportParameterNames = new ReportParameterNames();
		 List<ReportParameterNames> reportParameterNamesList = new ArrayList<ReportParameterNames>();
	//	ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			DBCollection metaJsonCollection = ponniviDb
					.getCollection(collectionName);

			DBCursor metaJsonCursor = metaJsonCollection.find();

			BasicDBObject metaJsonDbObject = (BasicDBObject) metaJsonCursor
					.next();
			try {
				while (metaJsonDbObject != null) {

					BasicDBObject evidenceRecord = (BasicDBObject) metaJsonDbObject
							.get(evidenceRecordSplitter);
					//Get exact context reference example
					contextReferenceExample = evidenceRecord.getString("Context_Reference_Example");
					if (linkName.equalsIgnoreCase("Context_Reference_Example") ||
							(linkName.equalsIgnoreCase("Asset_Xxx")) ||
							(linkName.equalsIgnoreCase("Asset_Yyy")) ) {
						connectorLink = evidenceRecord.getString(linkName);
						if (contextReferenceExample.equalsIgnoreCase("machine_uptime")) {
							if ( linkName.equalsIgnoreCase("Asset_Xxx") ||
							(linkName.equalsIgnoreCase("Asset_Yyy")) ) {
						reportId = String.valueOf(Integer.parseInt(reportId) + 1);
						setReportParam(reportId,connectorLink,reportParameterNames,reportParameterNamesList);
							}
						}
					}					
					else {
						
						
						com.mongodb.BasicDBList metaJsonDbListItems = (com.mongodb.BasicDBList) evidenceRecord
								.get(linkName);
						Iterator iter = metaJsonDbListItems.iterator();
						while (iter.hasNext()) {
							BasicDBObject obj = (BasicDBObject) iter.next();
							if (contextReferenceExample.equalsIgnoreCase("machine_uptime")) {
								connectorLink = obj.getString("Evidence_2");
								if (connectorLink != null) {
									setReportParam(reportId,connectorLink,reportParameterNames,reportParameterNamesList);
								}
								connectorLink = obj.getString("Evidence_3");
								if (connectorLink != null) {
									reportId = String.valueOf(Integer.parseInt(reportId) + 1);
									setReportParam(reportId,connectorLink,reportParameterNames,reportParameterNamesList);
								}
								connectorLink = obj.getString("Evidence_4");
								if (connectorLink != null) {
									reportId = String.valueOf(Integer.parseInt(reportId) + 1);
									setReportParam(reportId,connectorLink,reportParameterNames,reportParameterNamesList);
								}
							}
							else {
							connectorLink = obj.getString("C");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("1");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("D");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("2");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Qualifier_1");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("3");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Qualifier_2");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("4");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Qualifier_8");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("5");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}

							connectorLink = obj.getString("Evidence_5");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("6");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Evidence_6");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("7");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}

						}
						}
					}

					metaJsonDbObject = (BasicDBObject) (metaJsonCursor
							.hasNext() ? metaJsonCursor.next() : null);

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				metaJsonCursor.close();
			}
		}
		return reportParameterNamesList;
	}

	public static List<ReportParameterNames> getReportParameters1(
			String collectionName, String evidenceRecordSplitter,
			String linkName, DB ponniviDb) {
		List<ReportParameterNames> reportParameterNamesList = new ArrayList<ReportParameterNames>();
		ReportParameterNames reportParameterNames = new ReportParameterNames();

		String connectorLink = "";
	//	ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			DBCollection metaJsonCollection = ponniviDb
					.getCollection(collectionName);

			DBCursor metaJsonCursor = metaJsonCollection.find();

			BasicDBObject metaJsonDbObject = (BasicDBObject) metaJsonCursor
					.next();
			try {
				while (metaJsonDbObject != null) {

					BasicDBObject evidenceRecord = (BasicDBObject) metaJsonDbObject
							.get(evidenceRecordSplitter);
					if (linkName.equalsIgnoreCase("Context_Reference_Example")) {
						connectorLink = evidenceRecord.getString(linkName);
					} else {
						com.mongodb.BasicDBList metaJsonDbListItems = (com.mongodb.BasicDBList) evidenceRecord
								.get(linkName);
						Iterator iter = metaJsonDbListItems.iterator();
						while (iter.hasNext()) {
							BasicDBObject obj = (BasicDBObject) iter.next();

							connectorLink = obj.getString("C");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("1");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("D");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("2");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Qualifier_1");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("3");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Qualifier_2");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("4");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Qualifier_8");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("5");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}

							connectorLink = obj.getString("Evidence_5");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("6");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}
							connectorLink = obj.getString("Evidence_6");
							if (connectorLink != null) {
								reportParameterNames = new ReportParameterNames();
								reportParameterNames.setId("7");
								reportParameterNames
										.setParameterName(connectorLink);
								reportParameterNamesList
										.add(reportParameterNames);

							}

						}
					}

					metaJsonDbObject = (BasicDBObject) (metaJsonCursor
							.hasNext() ? metaJsonCursor.next() : null);

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				metaJsonCursor.close();
			}
		}
		return reportParameterNamesList;
	}

	// Metajson Context_Reference_Example, Evidence_2, Evidence_3 etc., string
	// displayed as output
	public static String getConnectingLinkNames(String collectionName,
			String evidenceRecordSplitter, String linkName, String linkName1,
			MongoClient mongo, String dbName) {
		String connectorLink = "";
		ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			DBCollection metaJsonCollection = ponniviDb
					.getCollection(collectionName);

			DBCursor metaJsonCursor = metaJsonCollection.find();

			BasicDBObject metaJsonDbObject = (BasicDBObject) metaJsonCursor
					.next();
			try {
				while (metaJsonDbObject != null) {

					BasicDBObject evidenceRecord = (BasicDBObject) metaJsonDbObject
							.get(evidenceRecordSplitter);
					if (linkName.equalsIgnoreCase("Context_Reference_Example")) {
						connectorLink = evidenceRecord.getString(linkName);
					} else {
						com.mongodb.BasicDBList metaJsonDbListItems = (com.mongodb.BasicDBList) evidenceRecord
								.get(linkName);
						Iterator iter = metaJsonDbListItems.iterator();
						while (iter.hasNext()) {
							BasicDBObject obj = (BasicDBObject) iter.next();
							connectorLink = obj.getString(linkName1);
							if (connectorLink != null) {
								// System.out.println("connectorLink.."
								// + connectorLink);
								break;
							}
						}
					}

					metaJsonDbObject = (BasicDBObject) (metaJsonCursor
							.hasNext() ? metaJsonCursor.next() : null);

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				metaJsonCursor.close();
			}
		}
		return connectorLink;
	}

	/*
	 * dataJsonCollectionName - Input for dataJsonCollectionName eg :
	 * DataJsonRotatingAssetsDetails parameter1 - eg : defective/missing etc..
	 * appName - eg : Rotating_assets etc., By passing the input parameters,
	 * this method iterates the data json and give the arraylist of jsonobjects
	 */

	public static ArrayList<org.json.JSONObject> getDataJsonDetails(
			String dataJsonCollectionName, String parameter1, String appName,
			MongoClient mongo, String dbName) {
		ponniviDb = mongo.getDB(dbName);
		ArrayList<org.json.JSONObject> detailsList = new ArrayList<org.json.JSONObject>();
		DBCursor dataJsonCursor = null;
		try {
			if (ponniviDb != null) {

				DBCollection dataJsonCollection = ponniviDb
						.getCollection(dataJsonCollectionName);

				dataJsonCursor = dataJsonCollection.find();

				BasicDBObject dataJsonDbObject = (BasicDBObject) dataJsonCursor
						.next();

				while (dataJsonDbObject != null) {

					BasicDBObject evidenceRecord = (BasicDBObject) dataJsonDbObject
							.get("Evidence_Record");
					String contextType = evidenceRecord
							.getString("Context_Type");
					String metaJsoncollectionName = "";
					if (appName.equals("Rotating_assets")) { // For
																// Rotating_assets,
																// we need to
																// pass
																// MetaJsonRotatingAssetsDetails.
						metaJsoncollectionName = "MetaJsonRotatingAssetsDetails";
					}
					if (appName.equals("machine_uptime")) { // For
															// machine_uptime,
															// we need to pass
															// MetaJsonMachineUptimeDetails.
						metaJsoncollectionName = "MetaJsonMachineUptimeDetails";
					}
					contextReferenceExample = getConnectingLinkNames(
							metaJsoncollectionName, "Evidence_Record",
							"Context_Reference_Example", "", mongo, dbName);

					evidence_2 = getConnectingLinkNames(metaJsoncollectionName,
							"Evidence_Record", "Items", "Evidence_2", mongo,
							dbName);
					evidence_3 = getConnectingLinkNames(metaJsoncollectionName,
							"Evidence_Record", "Items", "Evidence_3", mongo,
							dbName);
					evidence_4 = getConnectingLinkNames(metaJsoncollectionName,
							"Evidence_Record", "Items", "Evidence_4", mongo,
							dbName);

					if (contextType.equalsIgnoreCase(contextReferenceExample)) {
						// System.out.println("Meta and Data json match..");
						String items = evidenceRecord.getString("Items");
						// System.out.println(items);
						JSONArray itemsArray = new JSONArray(items);
						int count1 = 0, count2 = 0, count3 = 0;
						BigDecimal cost1 = new BigDecimal(0), cost2 = new BigDecimal(
								0), cost3 = new BigDecimal(0);
						for (int i = 0; i < itemsArray.length(); i++) {

							org.json.JSONObject subItemDetails = (org.json.JSONObject) itemsArray
									.get(i);
							// System.out.println(subItemDetails.getJSONObject("Specific_Details"));
							String requiredCode = "";
							org.json.JSONObject specificDetails = subItemDetails
									.getJSONObject("Specific_Details");

							if ((evidence_2.equalsIgnoreCase("Service_Note"))
									|| (evidence_3
											.equalsIgnoreCase("Missing_Note"))) {
								org.json.JSONObject evidenceQualifiers = subItemDetails
										.getJSONObject("Evidence_Qualifiers");
								if (parameter1.equalsIgnoreCase("defective")) { // Rotating_assets
									requiredCode = evidenceQualifiers
											.getString("Qualifier_1");
								}
								if (parameter1.equalsIgnoreCase("missing")) { // Rotating_assets
									requiredCode = evidenceQualifiers
											.getString("Qualifier_2");
								}
							}
							if ((evidence_2.equalsIgnoreCase("Reasons"))
									|| (evidence_3
											.equalsIgnoreCase("Machine_DownTime"))
									|| (evidence_4.equalsIgnoreCase("Action"))) {
								org.json.JSONObject evidences = subItemDetails
										.getJSONObject("Evidences");
								if (parameter1.equalsIgnoreCase("Reasons")) { // machine_uptime
									requiredCode = evidences
											.getString("Evidence_2");
								}
								if (parameter1
										.equalsIgnoreCase("Machine_DownTime")) { // machine_uptime
									requiredCode = evidences
											.getString("Evidence_3");
								}
								if (parameter1.equalsIgnoreCase("Action")) { // machine_uptime
									requiredCode = evidences
											.getString("Evidence_4");
								}
							}

							if (appName.equals("Rotating_assets")) {
								if (requiredCode.equalsIgnoreCase("green")
										|| requiredCode.equalsIgnoreCase("red")) {
									detailsList.add(subItemDetails);
								}
							} else if (appName.equals("machine_uptime")) {
								if (requiredCode != null
										&& !requiredCode.equalsIgnoreCase("")) {
									detailsList.add(subItemDetails);
								}
							}

						}

					}

					dataJsonDbObject = (BasicDBObject) (dataJsonCursor
							.hasNext() ? dataJsonCursor.next() : null);
					if (dataJsonCursor.hasNext())
						dataJsonDbObject = (BasicDBObject) dataJsonCursor
								.next();
					else {
						dataJsonCursor.close();
						dataJsonDbObject = null;
					}
				}

			} // != null
		} catch (Exception e) {
			System.out.println("Mongodb exception.." + e.getMessage());
			// e.printStackTrace();
		} finally {
			dataJsonCursor.close();
		}

		return detailsList;
	}

	public static DBObject setCondition(String paramCondition,
			String paramName, String paramValue, String paramDataType,String collectionName, String contextType) {
		String linkNameToMatch = getLinkNames(paramName, contextType);
		DBObject orClausePart = null;

		if (paramCondition.equalsIgnoreCase("<")) {
			orClausePart = new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$lt", Integer.parseInt(paramValue)));
		}
		if (paramCondition.equalsIgnoreCase(">=")) {
			// searchArguments.add(new BasicDBObject(linkNameToMatch,new
			// BasicDBObject("$gte",Integer.parseInt(paramValue))));
			orClausePart = new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$gte", Integer.parseInt(paramValue)));
		}
		if (paramCondition.equalsIgnoreCase("=")) {
			if (paramDataType.equalsIgnoreCase("number")) {
				// searchArguments.add(new
				// BasicDBObject(linkNameToMatch,Integer.parseInt(paramValue)));
				orClausePart = new BasicDBObject(linkNameToMatch,
						Integer.parseInt(paramValue));
			} else {
				if (paramValue.equalsIgnoreCase("ALL")) {
					List<String> list = new ArrayList<String>();
					if (linkNameToMatch.equalsIgnoreCase("Qualifier_8")) {
						list = getCategoryList(collectionName);
					}
					if (linkNameToMatch.equalsIgnoreCase("Qualifier_2")
							|| linkNameToMatch.equalsIgnoreCase("Qualifier_1")) {
						list.add("High_Severity");
						list.add("Low_Severity");
					}
					orClausePart = new BasicDBObject(linkNameToMatch,
							new BasicDBObject("$in", list));
				} else {
					orClausePart = new BasicDBObject(linkNameToMatch,
							new BasicDBObject("$regex", paramValue).append(
									"$options", "i"));
				}
			}
		}
		if (paramCondition.equalsIgnoreCase("!=")) {
			if (paramDataType.equalsIgnoreCase("number")) {
				orClausePart = new BasicDBObject(linkNameToMatch,
						new BasicDBObject("$ne", Integer.parseInt(paramValue)));
			} else {
				orClausePart = new BasicDBObject(linkNameToMatch,
						new BasicDBObject("$ne", paramValue));
			}

		}
		if (paramCondition.equalsIgnoreCase("LIKE")) {
			orClausePart = new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$regex", paramValue + "*").append(
							"$options", "i"));
		}
		if (paramCondition.equalsIgnoreCase("NOT LIKE")) { // Here we should
															// change condition
															// -- May 20
			orClausePart = new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$regex", paramValue).append("$options",
							"i"));
		}
		// return searchArguments;
		return orClausePart;
	}

	public static List<BasicDBObject> setCondition(String paramCondition,
			List<BasicDBObject> searchArguments, String paramName,
			String paramValue, String paramDataType,String collectionName, String contextType) {
		String linkNameToMatch = getLinkNames(paramName, contextType);
		if (linkNameToMatch.equalsIgnoreCase("")) { 
			//For newly added columns, there will not be matching, hence adding the paramName itself as matching name - Sep 1
			try {
				int paramValueNumeric = Integer.parseInt(paramValue);
				paramCondition = "=";
				paramDataType = "number";
			}catch(NumberFormatException ex) {
				paramCondition = "LIKE";					
			}			
			linkNameToMatch = paramName; 
			
		}

		if (paramCondition.equalsIgnoreCase("<")) {
			searchArguments.add(new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$lt", Integer.parseInt(paramValue))));
		}
		if (paramCondition.equalsIgnoreCase(">=")) {
			searchArguments.add(new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$gte", Integer.parseInt(paramValue))));
			// searchArguments.add(new
			// BasicDBObject("$gte",Integer.parseInt(paramValue)));
		}
		if (paramCondition.equalsIgnoreCase("=")) {
			if (paramDataType.equalsIgnoreCase("number")) {
				searchArguments.add(new BasicDBObject(linkNameToMatch, Integer
						.parseInt(paramValue)));
			} else {
				if (paramValue.equalsIgnoreCase("ALL")) {
					List<String> list = new ArrayList<String>();
					if (linkNameToMatch.equalsIgnoreCase("Qualifier_8")) {
						list = getCategoryList(collectionName);
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$in", list)));
					}
					if (linkNameToMatch.equalsIgnoreCase("Qualifier_2")
							|| linkNameToMatch.equalsIgnoreCase("Qualifier_1")) {
						list.add("High_Severity");
						list.add("Low_Severity");
						searchArguments.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$in", list)));
					}
				} else {
					searchArguments.add(new BasicDBObject(linkNameToMatch,
							new BasicDBObject("$regex", paramValue).append(
									"$options", "i")));
				}
			}
		}
		if (paramCondition.equalsIgnoreCase("!=")) {
			if (paramDataType.equalsIgnoreCase("number")) {
				searchArguments
						.add(new BasicDBObject(linkNameToMatch,
								new BasicDBObject("$ne", Integer
										.parseInt(paramValue))));
			} else {
				searchArguments.add(new BasicDBObject(linkNameToMatch,
						new BasicDBObject("$ne", paramValue)));
			}

		}
		if (paramCondition.equalsIgnoreCase("LIKE")) {
			searchArguments.add(new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$regex", paramValue + "*").append(
							"$options", "i")));
		}
		if (paramCondition.equalsIgnoreCase("NOT LIKE")) { // Here we should
															// change condition
															// -- May 20
			searchArguments.add(new BasicDBObject(linkNameToMatch,
					new BasicDBObject("$regex", paramValue).append("$options",
							"i")));
		}
		return searchArguments;
	}

	

	public static BasicDBObject addDateParameters(String fromDate,
			String toDate, String logoutTime) {
		BasicDBObject dateQuery = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy");
		try {
			Date startDate = simpleDateFormat.parse(fromDate);
			Date endDate = simpleDateFormat.parse(toDate);
			dateQuery = new BasicDBObject(logoutTime, new BasicDBObject("$gte",
					startDate).append("$lte", endDate));

		} catch (java.text.ParseException e) {
			//e.printStackTrace();
			return null;
		}
		return dateQuery;
	}

	public static DBObject addEvidenceIdentifier(String evidenceIdentifier,
			String evidenceIdentifierValue, List evidenceIdentifierListItems) {
		DBObject evidenceIdentifierClause = null;
		if (evidenceIdentifierValue.equalsIgnoreCase("ALL")) {
			evidenceIdentifierClause = new BasicDBObject(evidenceIdentifier,
					new BasicDBObject("$in", evidenceIdentifierListItems));
		} else {
			evidenceIdentifierClause = new BasicDBObject(evidenceIdentifier,
					evidenceIdentifierValue);
		}
		return evidenceIdentifierClause;
	}

	public static String getLinkNames(String paramName, String contextType) {
		String linkNameToMatch = "";
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
		if (paramName.equalsIgnoreCase("price")) {
			linkNameToMatch = "C";
		}
		if (paramName.equalsIgnoreCase("Incharge")) {
			linkNameToMatch = "D";
		}
		if (paramName.equalsIgnoreCase("ArticleNumber")) {
			linkNameToMatch = "A";
		}
		if (paramName.equalsIgnoreCase("ProductName")) {
			linkNameToMatch = "B";
		}
		if (paramName.equalsIgnoreCase("Category")) {
			linkNameToMatch = "Qualifier_8";
		}
		if (paramName.equalsIgnoreCase("To_Service_Item") || paramName.equalsIgnoreCase("To_Service_Item_Color")) {
			linkNameToMatch = "Qualifier_1";
		}
		if (paramName.equalsIgnoreCase("To_Service_Item_Text")) {
			linkNameToMatch = "Evidence_2";
		}
		if (paramName.equalsIgnoreCase("Missing_Item") || paramName.equalsIgnoreCase("Missing_Item_Color")) {
			linkNameToMatch = "Qualifier_2";
		}
		if (paramName.equalsIgnoreCase("Missing_Item_Text")) {
			linkNameToMatch = "Evidence_3";
		}
		if (paramName.equalsIgnoreCase("Added_Item")) {
			linkNameToMatch = "Evidence_5";
		}
		if (paramName.equalsIgnoreCase("Deleted_Item")) {
			linkNameToMatch = "Evidence_6";
		}
		if (paramName.equalsIgnoreCase("Scanned_Item")) {
			linkNameToMatch = "Evidence_4";
		}
		}
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			if (paramName.equalsIgnoreCase("remoteunit")) {
				linkNameToMatch = "Asset_Zzz";
			}
			if (paramName.equalsIgnoreCase("Reasons")) {
				linkNameToMatch = "Evidence_2";
			}
			if (paramName.equalsIgnoreCase("Machine DownTime")) {
				linkNameToMatch = "Evidence_3";
			}
			if (paramName.equalsIgnoreCase("Action")) {				
				linkNameToMatch = "Evidence_4";
			}
			if (paramName.equalsIgnoreCase("Machine Name")) {
				linkNameToMatch = "Asset_Xxx";
			}
			if (paramName.equalsIgnoreCase("Supervisor Name")) {
				linkNameToMatch = "Asset_Yyy";
			}
			}
		// end for change
		return linkNameToMatch;

	}
	
	public static List 	getCategoryList(String collectionName) {
		List list = new ArrayList();
		if (collectionName.equalsIgnoreCase("items_collection")) {
		list.add("screwdriver");
		list.add("spanner");
		list.add("miscellaneous");
		list.add("powertool");
	}
	else {
		list.add("any measurement tools".toLowerCase());
		list.add("climbing gear".toLowerCase());
		list.add("Work clothes".toLowerCase());
		list.add("Car".toLowerCase());
		list.add("Keys".toLowerCase());
		list.add("Others".toLowerCase());
	}
		return list;
	}
	
	public static List<DBObject> getSelectedItemsForToolsOrEquipments(DB ponniviDb, String collectionName,
			String evidenceIdentifier,String evidenceIdentifierValue) {
		DBObject evidenceIdentifierClause = null;		
		List<DBObject> resultSet = new ArrayList<DBObject>();
			evidenceIdentifierClause = new BasicDBObject(evidenceIdentifier,
					evidenceIdentifierValue);
			DBCollection collection = ponniviDb.getCollection(collectionName);
			DBCursor cursor = collection.find(evidenceIdentifierClause);
			while (cursor.hasNext()) {
				// System.out.println(cursor.next());
				DBObject o = cursor.next();
				resultSet.add(o);
			}
			return resultSet;
		}
}
