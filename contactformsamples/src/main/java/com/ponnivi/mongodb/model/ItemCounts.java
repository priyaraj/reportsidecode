/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> //Bean class that is used to pass as datasource : eg to rotating assets report

 * */
package com.ponnivi.mongodb.model;

import java.math.BigDecimal;



public class ItemCounts {
	public ItemCounts(int count1, int count2, int count3, BigDecimal cost1,
			BigDecimal cost2, BigDecimal cost3, String count4, String cost4) {
		super();
		this.count1 = count1;
		this.count2 = count2;
		this.count3 = count3;
		this.cost1 = cost1;
		this.cost2 = cost2;
		this.cost3 = cost3;
		this.count4 = count4;
		this.cost4 = cost4;
	}
	public ItemCounts() {
		
	}

	public int getCount1() {
		return count1;
	}

	public void setCount1(int count1) {
		this.count1 = count1;
	}

	public int getCount2() {
		return count2;
	}

	public void setCount2(int count2) {
		this.count2 = count2;
	}

	public int getCount3() {
		return count3;
	}

	public void setCount3(int count3) {
		this.count3 = count3;
	}

	public BigDecimal getCost1() {
		return cost1;
	}

	public void setCost1(BigDecimal cost1) {
		this.cost1 = cost1;
	}

	public BigDecimal getCost2() {
		return cost2;
	}

	public void setCost2(BigDecimal cost2) {
		this.cost2 = cost2;
	}

	public BigDecimal getCost3() {
		return cost3;
	}

	public void setCost3(BigDecimal cost3) {
		this.cost3 = cost3;
	}

	private int count1;
	private int count2;
	private int count3;
	private String count4;
	private BigDecimal cost1;
	private BigDecimal cost2;
	private BigDecimal cost3;
	private String cost4;

	public String getCount4() {
		return count4;
	}

	public void setCount4(String count4) {
		this.count4 = count4;
	}

	public String getCost4() {
		return cost4;
	}

	public void setCost4(String cost4) {
		this.cost4 = cost4;
	}
}