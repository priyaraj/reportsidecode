package com.ponnivi.mongodb.model;

import java.util.List;

public class ReportOptionsDetails {

	private List<String> valuesList;

	private List<String> conditionsList;

	public List<String> getValuesList() {
		return valuesList;
	}

	public void setValuesList(List<String> valuesList) {
		this.valuesList = valuesList;
	}

	public List<String> getConditionsList() {
		return conditionsList;
	}

	public void setConditionsList(List<String> conditionsList) {
		this.conditionsList = conditionsList;
	}

	

}
