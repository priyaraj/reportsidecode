<%@page import="com.mongodb.AggregationOutput"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>AwesomeChartJS for CU_LGN</title>
        <style>
        
            body{
                background: #fff;
                color: #333;
            }
            
            a, a:visited, a:link, a:active{
                color: #333;
            }
            
            a:hover{
                color: #00f;
            }
        
            .charts_container{
                width: 900px;
                height: 420px;
                margin: 10px auto;
            }
            
            .chart_container_centered{
                text-align: center;
                width: 900px;
                height: 420px;
                margin: 10px auto;
            }
            
            .chart_container{
                width: 400px;
                height: 400px;
                margin: 0px 25px;
                float: left;
            }
            
            .footer{
                font-size: small;
                text-align: right;
            }
        </style>
        <script type="application/javascript" src="js/awesomechart.js"> </script>
    </head>
    <body>
    <%!
    public List computeChartValues(AggregationOutput aggregationOutput) {
	int idx = 0;
	String idStr = "",timesStr="";
	List chartList = null;
	 if(aggregationOutput != null) {
		 idx = 0;
		 chartList = new ArrayList();
	 for (DBObject result : aggregationOutput.results()) {      
	    	idx += 1;
		if(idx>5){
		break;}
		if (idx < 5) {
		idStr +=  "\'" + result.get("_id") + "\',";
		System.out.println("idstr=" + idStr);
		timesStr += result.get("times") +",";
		}
		else {
			idStr += "\'" + result.get("_id") + "\'";
			timesStr += result.get("times");
		}
		 
	 } 
	 chartList.add(idStr);
	 chartList.add(timesStr);
	} 
	 return chartList;
}
    %>    
        <div class="chart_container_centered">

            <canvas id="chartCanvas13" width="600" height="400">
                Your web-browser does not support the HTML 5 canvas element.
            </canvas>

        </div>
        
        <div class="charts_container">
            <div class="chart_container">
            
                <canvas id="chartCanvas1" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>
                
            </div>
            
            <div class="chart_container">
            
                <canvas id="chartCanvas2" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>
                
            </div>
        </div>

        <div class="charts_container">
            <div class="chart_container">

                <canvas id="chartCanvas11" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>

            <div class="chart_container">

                <canvas id="chartCanvas12" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
        </div>

        <div class="charts_container">
            <div class="chart_container">

                <canvas id="chartCanvas3" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>

            <div class="chart_container">

                <canvas id="chartCanvas4" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
        </div>
        
        <div class="charts_container">
            <div class="chart_container">

                <canvas id="chartCanvas5" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>

            <div class="chart_container">

                <canvas id="chartCanvas6" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
        </div>
        
        <div class="charts_container">
            <div class="chart_container">

                <canvas id="chartCanvas7" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
            
            <div class="chart_container">

                <canvas id="chartCanvas8" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
        </div>
        
        <div class="charts_container">
            <div class="chart_container">

                <canvas id="chartCanvas9" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
            
            <div class="chart_container">

                <canvas id="chartCanvas10" width="400" height="400">
                    Your web-browser does not support the HTML 5 canvas element.
                </canvas>

            </div>
        </div>
        
        <p class="footer">Data source: <a href="http://gs.statcounter.com/#browser-ww-monthly-200912-201012-bar" target="_blank">StatCounter</a></p>
        
        <script type="application/javascript">
        <%
        AggregationOutput aggregationOutput = null;
        aggregationOutput = (AggregationOutput)session.getAttribute("machinedowntimeOutput");
        List chartList = computeChartValues(aggregationOutput) ;
        if (chartList != null) { %>
        
            var chart1 = new AwesomeChart('chartCanvas1');
            chart1.title = "Top 5 Machine Downtime Data";
            chart1.data = [<%= chartList.get(1) %>];
            chart1.labels = [<%= chartList.get(0) %>];
            chart1.colors = ['#006CFF', '#FF6600', '#34A038', '#945D59', '#93BBF4', '#F493B8'];
            chart1.randomColors = true;
            chart1.animate = true;
            chart1.animationFrames = 30;
            chart1.draw();
            
            var chart2 = new AwesomeChart('chartCanvas2');
            chart2.title = "Top 5 Machine Downtime Data";
            chart2.data = [<%= chartList.get(1) %>];
            chart2.labels = [<%= chartList.get(0) %>];
            chart2.barFillStyle = chart1.colors[0];
            chart2.labelFillStyle = chart1.colors[0];
            chart2.animate = true;
            chart2.animationFrames = 30;
            chart2.draw();
            
            var chart3 = new AwesomeChart('chartCanvas3');
            chart3.chartType = "exploded pie";
            chart3.title = "Top 5 Machine Downtime Data";
            chart3.data = [<%= chartList.get(1) %>];
            chart3.labels = [<%= chartList.get(0) %>];
            chart3.colors = chart1.colors;
            chart3.animate = true;
            chart3.draw();

            var chart4 = new AwesomeChart('chartCanvas4');
            chart4.chartType = "exploded pie";
            chart4.pieTotal = 100;
            chart4.pieStart = 90;
            chart4.title = "Top 5 Machine Downtime Data";
            chart4.data = [<%= chartList.get(1) %>];
            chart4.labels = [<%= chartList.get(0) %>];
            chart4.pieFillStyle = chart1.colors[0];
            chart4.labelFillStyle = chart1.colors[0];
            chart4.animate = true;
            chart4.draw();

            var chart5 = new AwesomeChart('chartCanvas5');
            chart5.chartType = "pie";
            chart5.title = "Top 5 Machine Downtime Data";
            chart5.data = [<%= chartList.get(1) %>];
            chart5.labels = [<%= chartList.get(0) %>];
            chart5.colors = chart1.colors;
            chart5.animate = true;
            chart5.draw();

            var chart6 = new AwesomeChart('chartCanvas6');
            chart6.chartType = "pie";
            chart6.pieTotal = 100;
            chart6.title = "Top 5 Machine Downtime Data";
            chart6.data = [<%= chartList.get(1) %>];
            chart6.labels = [<%= chartList.get(0) %>];
            chart6.pieFillStyle = chart1.colors[0];
            chart6.labelFillStyle = chart1.colors[0];
            chart6.animate = true;
            chart6.draw();
            
            var chart7 = new AwesomeChart('chartCanvas7');
            chart7.chartType = "ring";
            chart7.title = "Top 5 Machine Downtime Data";
            chart7.data = [<%= chartList.get(1) %>];
            chart7.labels = [<%= chartList.get(0) %>];
            chart7.colors = chart1.colors;
            chart7.randomColors = true;
            chart7.animate = true;
            chart7.draw();

            var chart8 = new AwesomeChart('chartCanvas8');
            chart8.chartType = "ring";
            chart8.pieTotal = 100;
            chart8.title = "Top 5 Machine Downtime Data";
            chart8.data = [<%= chartList.get(1) %>];
            chart8.labels = [<%= chartList.get(0) %>];
            chart8.pieFillStyle = chart1.colors[0];
            chart8.labelFillStyle = chart1.colors[0];
            chart8.animate = true;
            chart8.draw();
            
            var chart9 = new AwesomeChart('chartCanvas9');
            chart9.chartType = "ring";
            chart9.pieTotal = 100;
            chart9.pieStart = 51.62;
            chart9.title = "Top 5 Machine Downtime Data";
            chart9.data = [<%= chartList.get(1) %>];
            chart9.labels = [<%= chartList.get(0) %>];
            chart9.pieFillStyle = chart1.colors[1];
            chart9.labelFillStyle = chart1.colors[1];
            chart9.animate = true;
            chart9.animationFrames = 40;
            chart9.draw();
            
            var chart10 = new AwesomeChart('chartCanvas10');
            chart10.chartType = "ring";
            chart10.pieTotal = 100;
            chart10.pieStart = 82.92;
            chart10.title = "Top 5 Machine Downtime Data";
            chart10.data = [<%= chartList.get(1) %>];
            chart10.labels = [<%= chartList.get(0) %>];
            chart10.pieFillStyle = chart1.colors[2];
            chart10.labelFillStyle = chart1.colors[2];
            chart10.animate = true;
            chart10.animationFrames = 20;
            chart10.draw();

            var chart11 = new AwesomeChart('chartCanvas11');
            chart11.chartType = "horizontal bars";
            chart11.title = "Top 5 Machine Downtime Data";
            chart11.data = [<%= chartList.get(1) %>];
            chart11.labels = [<%= chartList.get(0) %>];
            chart11.colors = chart1.colors;
            chart11.randomColors = true;
            chart11.animate = true;
            chart11.animationFrames = 30;
            chart11.draw();

            var chart12 = new AwesomeChart('chartCanvas12');
            chart12.chartType = "horizontal bars";
            chart12.title = "Top 5 Machine Downtime Data";
            chart12.data = [<%= chartList.get(1) %>];
            chart12.labels = [<%= chartList.get(0) %>];
            chart12.barFillStyle = chart1.colors[0];
            chart12.labelFillStyle = chart1.colors[0];
            chart12.animate = true;
            chart12.animationFrames = 30;
            chart12.draw();
            
            
            var chart13 = new AwesomeChart('chartCanvas13');
            chart13.chartType = "pareto";
            chart13.title = "Top 5 Machine Downtime Data";
            chart13.data = [<%= chartList.get(1) %>];
            chart13.labels = [<%= chartList.get(0) %>];
            chart13.colors = chart1.colors;
            chart13.chartLineStrokeStyle = 'rgba(0, 0, 200, 0.5)';
            chart13.chartPointFillStyle = 'rgb(0, 0, 200)';
            chart13.draw();
            <% } %>
        </script>
        
    </body>
</html>

