<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ponnivi.mongodb.model.ReportOptionsDetails"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | Update new tags</title>

<script type="text/javascript" src="js/adminfunctions.js"></script>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script>
function changevalues() {
	//alert("changevalues");
	var selectedUpdatableColumn = document.update_new_tags.updatablecolumns.value;
	var collections = document.update_new_tags.itemcollectionnames;
	var selectedCollection = [];
	for (var i = 0; i < collections.options.length; i++) {
	  if (collections.options[i].selected) {
		  selectedCollection.push(collections.options[i].value);
	  }
	}
	//var selectedCollection = document.update_new_tags.itemcollectionnames.value;
	//alert("in changevalues.." + selectedUpdatableColumn);
	
<%
ReportOptionsDetails details = new ReportOptionsDetails();
	List list 	= new ArrayList(); 
	List entireList = new ArrayList();
	String retrievalParamName = "";
	DB customerDatabase = (DB)session.getAttribute("customerDb");%>
	
if (selectedUpdatableColumn == "Incharge") {
<%
	retrievalParamName = "Incharge";
	 list 	= new ArrayList(); 
	entireList = new ArrayList();	
	%>
		for (var i = 0; i < selectedCollection.length;i++) {
		//	alert("option="+selectedCollection[i]);
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn in incharge ="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_d_value");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn in incharge ="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_d_value");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}	
	
	if (selectedUpdatableColumn == "price") {	
		<%		
		retrievalParamName = "price";
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {
		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_c_value");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_c_value");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}
	
	if (selectedUpdatableColumn == "ArticleNumber") {	
		<%		
		retrievalParamName = "Incharge";  //Since we need to get similar kind of dropdown for conditions, assigned to Incharge
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_a_value");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_a_value");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}
	
	if (selectedUpdatableColumn == "ProductName") {	
		<%		
		retrievalParamName = "Incharge";
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {
		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_b_value");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_b_value");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}
	if (selectedUpdatableColumn == "Missing_Item_Text") {	
		<%		
		retrievalParamName = "Missing_Item";
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {
		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_missing_text");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_missing_text");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}
	if (selectedUpdatableColumn == "To_Service_Item_Text") {	
		<%		
		retrievalParamName = "To_Service_Item";
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {
		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_service_text");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_service_text");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}	
	if (selectedUpdatableColumn == "Missing_Item_Color") {	
		<%		
		retrievalParamName = "Missing_Item";
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {
		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_missing_color");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_missing_color");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}
	if (selectedUpdatableColumn == "To_Service_Item_Color") {	
		<%		
		retrievalParamName = "To_Service_Item";
		 list 	= new ArrayList(); 
		entireList = new ArrayList(); %>
		for (var i = 0; i < selectedCollection.length;i++) {
		
			if (selectedCollection[i] == "items_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_service_color");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (selectedCollection[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_service_color");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	}
	//Similarly we need to do for rest of items.
	//Trying to customize and informed Bala to customize it if possible or else need to repeat the above process  -- Aug 21
	
	
		
}

function initialize() {
	<%
	
	List updatableFieldList 	= new ArrayList();
	
	
	updatableFieldList = (ArrayList)session.getAttribute("updatableColumns");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableFieldListOptions = '<select id="updatablecolumns" name="updatablecolumns" onchange="changevalues()" >';
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     updatableFieldListOptions += '<option><%= updatableFieldList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('updatablecolumns').outerHTML = updatableFieldListOptions;
	<%
	List collectionNamesList 	= new ArrayList();
	
	
	collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
	if (!collectionNamesList.isEmpty()) {
		%>
		var collectionNamesListOptions = '<select id="itemcollectionnames" name="itemcollectionnames" size="3" multiple="multiple">';
		<%		
	     for(int i = 0; i < collectionNamesList.size();i++)
	     { 
	    	 if (!collectionNamesList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     collectionNamesListOptions += '<option><%= collectionNamesList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	collectionNamesListOptions += '</select>';
	document.getElementById('itemcollectionnames').outerHTML = collectionNamesListOptions;
	

	<%
	
	 list 	= new ArrayList(); 
	entireList = new ArrayList(); %>
	var collections = document.update_new_tags.itemcollectionnames;
		for (var i = 0; i < collections.length;i++) {
		//	alert("option="+selectedCollection[i]);
			if (collections[i] == "items_collection") {
				//alert("selectedUpdatableColumn in incharge ="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_collection_d_value");
				entireList = addElementsToList(list, entireList);

			%>				
			}
			if (collections[i] == "items_equipments_collection") {
				//alert("selectedUpdatableColumn in incharge ="+selectedUpdatableColumn);
				<% list = (ArrayList)session.getAttribute("items_equipments_collection_d_value");
				entireList = addElementsToList(list, entireList);
				%>				
			}
		}
		<%
			entireList = removeDuplicates(entireList);
		%>
		var updatableFieldListOptions = '<select id="existingvalue" name="existingvalue">';
		<%
			if (entireList != null) {
			if (!entireList.isEmpty()) {
		
			%>
			
			<%		
		     for(int i = 0; i < entireList.size();i++)
		     { 
		    	 if (!entireList.get(i).toString().equalsIgnoreCase("")) {
		     %>
		     updatableFieldListOptions += '<option><%= entireList.get(i) %></option>';	
		     <% 
		    	 }
		     }		
		}
		}
		else  {
		%>
		updatableFieldListOptions += '<option>NULL</option>';
		<% } %>
		updatableFieldListOptions += '</select>';
		document.getElementById('existingvalue').outerHTML = updatableFieldListOptions;
		
		var conditions= "";
		conditions = '<select id="conditions" name="conditions" >';		
		<% retrievalParamName = "Incharge"; 
		list = getConditionsList(retrievalParamName, customerDatabase) ;
		for (int i = 0; i < list.size(); i++) {
			%>
			conditions += '<option><%= list.get(i) %></option>';
			<%
		}
		%>
		conditions += '</select>';
		document.getElementById('conditions').outerHTML = conditions;
	
	
}
</script>
<body onload="initialize()">
<%!

	public List getConditionsList(String retrievalParamName,DB customerDatabase) {
	List list = new ArrayList();
	ReportOptionsDetails details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);
	list = details.getConditionsList();
	return list;
	}
    
    public List removeDuplicates(List entireList){
	//For removing duplicate entries
	if (entireList != null) {
			int count = entireList.size();
			System.out.println("count.."+count);
		    for (int i = 0; i < count; i++) 
		    {
		        for (int j = i + 1; j < count; j++) 
		        {
		            if (entireList.get(i).equals(entireList.get(j)))
		            {
		            	entireList.remove(j--);
		                count--;
		            }
		        }
		    }
		    return entireList;
    }
	else
		return null;
}

	public List addElementsToList(List inputList,List outputList) {
		if (inputList != null) {
		for (int idx = 0; idx < inputList.size(); idx++) {
			outputList.add(inputList.get(idx).toString());
			}
		return outputList;
		}
		else
			return null;
	}
%>
<form name = "update_new_tags" action="updateNewTags" method="post" onsubmit="return(validateForm(this));">

	  <div id="logo">
      		<img src="images/instaunite_logo.png"/>
      </div>
	  <h1>Update Screen (new tags)</h1>	   	
       <hr>
       <table style="width:100%;padding-top: 30px;">
       <tr>
    <td ><h4> Add New field</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="newfield" id="newfield"/></td>
  </tr>
  <tr>
  <tr>
    <td ><h4> Add New tags</h4></td>
    <td><input type="text"   name="newtag" id="newtag" style="float: left;"/></td>
  </tr>
  <tr>
    <td ><h4> Choose collection</h4></td>
    <td><select id="itemcollectionnames" size="3" multiple="multiple" name="itemcollectionnames"> 
             		<!-- <option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option> -->
             </select></td>
  </tr>
  <tr>
    <td id="align"></td>
    <td style="float: left;margin-top: 25px;"> <select id="updatablecolumns" name = "updatableColumns"> 
             		<!-- <option> Incharge </option>
                    <option> Price </option>
                  	<option> Added </option> -->
             </select></td>
             
             <td style="padding-top: 25px;float: left;margin-left: 10px;"> <select id="conditions" name="conditions"> 
             		<option> = </option>
                    <!-- <option> Price </option>
                  	<option> Added </option> -->
             </select></td>
             <td style="padding-top: 25px;float: left;margin-left: 10px;"><select id="existingvalue" name="existingvalue"></select></td>
  </tr>
  
</table>
<div class="inpsbtn" align="center">
       		<input type="submit" class="updatebutton" value="Submit"/>
</div>     
</form> 
</body>
</html>
