<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.model.ReportParameterNames"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="com.ponnivi.mongodb.model.ReportOptionsDetails"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | custom report</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script>
function changedropdownvalues(reportparameternumber) {	
	//alert("reportparameternumber" + reportparameternumber);
	
	var firstParameterName;
	if (reportparameternumber == 1) {
		firstParameterName = document.getElementById('firstparameter').value;
	}
	if (reportparameternumber == 2) {
		firstParameterName = document.getElementById('secondparameter').value;
	}
	if (reportparameternumber == 3) {
		firstParameterName = document.getElementById('thirdparameter').value;
	}
	

	
			<%
		MongoClient mongo = (MongoClient) request.getServletContext()
		.getAttribute("MONGO_CLIENT");
		String databaseName = (String)request.getServletContext()
		.getAttribute("PONNIVI_DATABASE");
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		ReportOptionsDetails details = new ReportOptionsDetails();
		String retrievalParamName = "";
		String itemCollectionName = (String)session.getAttribute("itemCollectionName");
	
		
		%>
	if (firstParameterName == "NULL") {
		var fillvalues = "",fillconditions = "";
		if (reportparameternumber == 2) {
			fillvalues = '<select id="param2Value" name="param2Value" ><option>NULL</option></select>';
			fillconditions = '<select id="param2Condition" name="param2Condition" ><option>NULL</option></select>';
			document.getElementById('param2Value').outerHTML = fillvalues;
			document.getElementById('param2Condition').outerHTML = fillconditions;
		}
		if (reportparameternumber == 3) {
			fillvalues = '<select id="param3Value" name="param3Value" ><option>NULL</option></select>';
			fillconditions = '<select id="param3Condition" name="param3Condition" ><option>NULL</option></select>';
			document.getElementById('param3Value').outerHTML = fillvalues;
			document.getElementById('param3Condition').outerHTML = fillconditions;
		}
		
	}
	if (firstParameterName == "price") {
		<% retrievalParamName = "price"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
	//	alert("firstParameterName12212.." + firstParameterName);
	var pricevalues = "",priceconditions = "";
	if (reportparameternumber == 1) {
		pricevalues = '<select id="param1Value" name="param1Value" >';
		priceconditions = '<select id="param1Condition" name="param1Condition" >';
	}
	if (reportparameternumber == 2) {
		pricevalues = '<select id="param2Value" name="param2Value" >';
		priceconditions = '<select id="param2Condition" name="param2Condition" >';
	}
	if (reportparameternumber == 3) {
		pricevalues = '<select id="param3Value" name="param3Value" >';
		priceconditions = '<select id="param3Condition" name="param3Condition" >';
	}
	
		<%
			
			List list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				pricevalues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			pricevalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = pricevalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = pricevalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = pricevalues;
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				priceconditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			priceconditions += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Condition').outerHTML = priceconditions;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = priceconditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = priceconditions;
			}
	}
	if (firstParameterName == "Incharge") {
		<% retrievalParamName = "Incharge"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);
		List inchargeList = new ArrayList();
		if (itemCollectionName.equalsIgnoreCase("items_collection")) { 
				inchargeList = (ArrayList)session.getAttribute("items_collection_d_value");
		} else {
			 	inchargeList = (ArrayList)session.getAttribute("items_equipments_collection_d_value");
	 	 }
		
		
		%>
		var inchargevalues = "",inchargeconditions = "";
		if (reportparameternumber == 1) {
		 inchargevalues = '<select id="param1Value" name="param1Value" >';
		inchargeconditions = '<select id="param1Condition" name="param1Condition" >';
		}
		if (reportparameternumber == 2) {
			 inchargevalues = '<select id="param2Value" name="param2Value" >';
			inchargeconditions = '<select id="param2Condition" name="param2Condition" >';
		}
		if (reportparameternumber == 3) {
			 inchargevalues = '<select id="param3Value" name="param3Value" >';
			inchargeconditions = '<select id="param3Condition" name="param3Condition" >';
		}
		<%
			
			// list = details.getValuesList();
			for (int i = 0; i < inchargeList.size(); i++) {
				 if (!inchargeList.get(i).toString().equalsIgnoreCase("")) {
				%>
				inchargevalues += '<option><%= inchargeList.get(i) %></option>';
				<%
				 }
			}
			%>
			inchargevalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = inchargevalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = inchargevalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = inchargevalues;
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				inchargeconditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			inchargeconditions += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Condition').outerHTML = inchargeconditions;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = inchargeconditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = inchargeconditions;
			}
	}
	if (firstParameterName == "Added_Item") {
		<% retrievalParamName = "Added_Item"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
		var addedItemValues = "",addedItemConditions=""; 
		if (reportparameternumber == 1) {
		addedItemValues = '<select id="param1Value" name="param1Value" >';
		addedItemConditions = '<select id="param1Condition" name="param1Condition" >';
		}
		if (reportparameternumber == 2) {
			addedItemValues = '<select id="param2Value" name="param2Value" >';
			addedItemConditions = '<select id="param2Condition" name="param2Condition" >';
		}
		if (reportparameternumber == 3) {
			addedItemValues = '<select id="param3Value" name="param3Value" >';
			addedItemConditions = '<select id="param3Condition" name="param3Condition" >';
		}
		<%
			
			 list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				addedItemValues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			addedItemValues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = addedItemValues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = addedItemValues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = addedItemValues;
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				addedItemConditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			addedItemConditions += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Condition').outerHTML = addedItemConditions;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = addedItemConditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = addedItemConditions;
			}
			
	}
	if (firstParameterName == "Deleted_Item") {
		<% retrievalParamName = "Deleted_Item"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
		var deletedItemValues = "",deletedItemConditions="";
		if (reportparameternumber == 1) {
		deletedItemValues = '<select id="param1Value" name="param1Value" >';
		deletedItemConditions = '<select id="param1Condition" name="param1Condition" >';
		}
		if (reportparameternumber == 2) {
			deletedItemValues = '<select id="param2Value" name="param2Value" >';
			deletedItemConditions = '<select id="param2Condition" name="param2Condition" >';
		}
		if (reportparameternumber == 3) {
			deletedItemValues = '<select id="param3Value" name="param3Value" >';
			deletedItemConditions = '<select id="param3Condition" name="param3Condition" >';
		}
		<%
			
			 list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				deletedItemValues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			deletedItemValues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = deletedItemValues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = deletedItemValues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = deletedItemValues;
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				deletedItemConditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			deletedItemConditions += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Condition').outerHTML = deletedItemConditions;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = deletedItemConditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = deletedItemConditions;
			}
	}
	if (firstParameterName == "To_Service_Item") {
		<% retrievalParamName = "To_Service_Item"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
		var toServiceItemValues="",toServiceItemConditions="";
		if (reportparameternumber == 1) {
		toServiceItemValues = '<select id="param1Value" name="param1Value" >';
		toServiceItemConditions = '<select id="param1Condition" name="param1Condition" >';
		}
		if (reportparameternumber == 2) {
			toServiceItemValues = '<select id="param2Value" name="param2Value" >';
			toServiceItemConditions = '<select id="param2Condition" name="param2Condition" >';
		}
		if (reportparameternumber == 3) {
			toServiceItemValues = '<select id="param3Value" name="param3Value" >';
			toServiceItemConditions = '<select id="param3Condition" name="param3Condition" >';
		}
		<%
			
			 list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				toServiceItemValues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			toServiceItemValues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = toServiceItemValues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = toServiceItemValues;			
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = toServiceItemValues;			
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				toServiceItemConditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			toServiceItemConditions += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Condition').outerHTML = toServiceItemConditions;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = toServiceItemConditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = toServiceItemConditions;			
			}
	}
	if (firstParameterName == "Category") {
	<% if (itemCollectionName.equalsIgnoreCase("items_collection")) { 
				retrievalParamName = "Category";
		} else {
			 	retrievalParamName = "Category_equipment";
			 	 }  
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
		var categoryValues = "",categoryConditions="";
		if (reportparameternumber == 1) { 
		categoryValues = '<select id="param1Value" name="param1Value" >';
		categoryConditions = '<select id="param1Condition" name="param1Condition" >';
		}
		if (reportparameternumber == 2) {
			categoryValues = '<select id="param2Value" name="param2Value" >';
			categoryConditions = '<select id="param2Condition" name="param2Condition" >';
		}
		if (reportparameternumber == 3) {
			categoryValues = '<select id="param3Value" name="param3Value" >';
			categoryConditions = '<select id="param3Condition" name="param3Condition" >';
		}
		<%
			
			 list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				categoryValues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			categoryValues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = categoryValues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = categoryValues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = categoryValues;
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				categoryConditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			categoryConditions += '</select>';
			if (reportparameternumber == 1) {
				document.getElementById('param1Condition').outerHTML = categoryConditions;
			}				
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = categoryConditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = categoryConditions;
			}
	}
	if (firstParameterName == "Missing_Item") {
		<% retrievalParamName = "Missing_Item"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
		var missingItemValues = "",missingItemConditions="";
		if (reportparameternumber == 1) {
		missingItemValues = '<select id="param1Value" name="param1Value" >';
		missingItemConditions = '<select id="param1Condition" name="param1Condition" >';
		}
		if (reportparameternumber == 2) {
			missingItemValues = '<select id="param2Value" name="param2Value" >';
			missingItemConditions = '<select id="param2Condition" name="param2Condition" >';
		}
		if (reportparameternumber == 3) {
			missingItemValues = '<select id="param3Value" name="param3Value" >';
			missingItemConditions = '<select id="param3Condition" name="param3Condition" >';
		}
		<%
			
			 list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				missingItemValues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			missingItemValues += '</select>';
			if (reportparameternumber == 1) {
				document.getElementById('param1Value').outerHTML = missingItemValues;
			}
			
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = missingItemValues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = missingItemValues;
			}
			<%
			list = details.getConditionsList();
			for (int i = 0; i < list.size(); i++) {
				%>
				missingItemConditions += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			missingItemConditions += '</select>';
			
			if (reportparameternumber == 1) {
				document.getElementById('param1Condition').outerHTML = missingItemConditions;				
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Condition').outerHTML = missingItemConditions;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Condition').outerHTML = missingItemConditions;
			}
	}

	
}
function loadFirstParamValues() {
	<%
	
	List evidenceIdentifierList = new ArrayList();
	
	
	evidenceIdentifierList = (ArrayList)request.getAttribute("evidenceIdentifierList");
	if (!evidenceIdentifierList.isEmpty()) {
		%>
		var evidenceIdentifieroptions = '<select id="evidenceIdentifier" name="evidenceIdentifier" ><option>ALL</option>';
		<%		
	     for(int i = 0; i < evidenceIdentifierList.size();i++)
	     { 
	    	 if (!evidenceIdentifierList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     evidenceIdentifieroptions += '<option><%= evidenceIdentifierList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	evidenceIdentifieroptions += '</select>';
	<%
	List<ReportParameterNames> reportparameterlist=new ArrayList<ReportParameterNames>();

	 reportparameterlist = (ArrayList)request.getAttribute("reportparameterlist");	 

	 if(!reportparameterlist.isEmpty())
	 { %>
	 var options = '<select id="firstparameter" name="firstparameter" onchange="changedropdownvalues(1)">';
	 var options1 = '<select id="secondparameter" name="secondparameter" onchange="changedropdownvalues(2)"><option>NULL</option>';
	 var options2 = '<select id="thirdparameter" name="thirdparameter" onchange="changedropdownvalues(3)"><option>NULL</option>';
	 <%
	     for(ReportParameterNames info: reportparameterlist)
	     { 
	     %>
	     options += '<option><%= info.getParameterName() %></option>';	
	     options1 += '<option><%= info.getParameterName() %></option>';
	     options2 += '<option><%= info.getParameterName() %></option>';
	        <% 
	     }

	 }
	%>
	options += '</select>';
	options1 += '</select>';
	options2 += '</select>';
	document.getElementById('firstparameter').outerHTML = options;
	document.getElementById('secondparameter').outerHTML = options1;
	document.getElementById('thirdparameter').outerHTML = options2;
	
	<%
	MongoClient mongo1 = (MongoClient) request.getServletContext()
	.getAttribute("MONGO_CLIENT");
	String databaseName1 = (String)request.getServletContext()
	.getAttribute("PONNIVI_DATABASE");
	ReportOptionsDetails details1 = new ReportOptionsDetails();

	
	details1 = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions","price",customerDatabase);%>
//	alert("firstParameterName12212.." + firstParameterName);
	var pricevalues = '<select id="param1Value" name="param1Value" >';
	var priceconditions = '<select id="param1Condition" name="param1Condition" >';
	<%
		
		List list1 = details1.getValuesList();
		for (int i = 0; i < list1.size(); i++) {
			%>
			pricevalues += '<option><%= list1.get(i) %></option>';
			<%
		}
		%>
		pricevalues += '</select>';
		document.getElementById('param1Value').outerHTML = pricevalues;
		<%
		list1 = details1.getConditionsList();
		for (int i = 0; i < list1.size(); i++) {
			%>
			priceconditions += '<option><%= list1.get(i) %></option>';
			<%
		}
		%>
		priceconditions += '</select>';
		document.getElementById('param1Condition').outerHTML = priceconditions;	
		
		document.getElementById('evidenceIdentifier').outerHTML = evidenceIdentifieroptions;	

}

</script>

<body onload="loadFirstParamValues()">
	<%-- ServiceHistory Add/Edit logic --%>
	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <%
      	if (itemCollectionName.equalsIgnoreCase("items_collection")) { %>
      	<h1>Custom Report Toolboxes</h1>	   	
		<%}
		else {%>
		<h1>Custom Report Equipments</h1>
		<%}%>

         <hr>
<form name="customizedreport" action="customreport" method="post">              
   <div class="report" align="center">
   			<h4> Report Title</h4>
   			    	<input type="text" class="inptxt" name="reporttitle"/>
     </div>  
     
     <div class="toolbox" >
     <% if (itemCollectionName.equalsIgnoreCase("items_collection")) { %>
     <h8>Choose a toolbox</h8>
     <%}
		else {%>
		<h8>Choose a equipmentbox</h8>
		<%}%>
   			 <select name="evidenceIdentifier" id = "evidenceIdentifier">
                	<option> Choose a value</option>
                    </select>
       </div> 
        <div class="date">
        <h8>Enter From date <br>dd-mm-yy format</h8>
<input type="text"  name="fromdateParam" id="fromdateParam"/>        
		
		 
        <h8>Enter To date <br>dd-mm-yy format</h8>
<input type="text"  name="todateParam" id="todateParam"/>        

  <h9>(or)</h9>
<h8>Select date Patterns</h8>
<select  id="datepattern" name="datepattern" >
	<option>NULL</option>
    <option>Less than a week</option>
    <option>Less than a month</option>
    <option>Less than 6 months</option>
    <option>Less than a year</option>
     </select>  
     
		
       </div> 

   
 <div align="center">
   <table class="tblcls" width="200" >
  <tr>
    <td></td>
    <td>Allow Queries</td>
    <td>
     </td>
    <td></td>
  </tr>
  <tr>
    <td></td>

  
    <td><select  id="firstparameter" name="firstparameter" onChange="changedropdownvalues()">
    <option>Choose a parameter</option>
     </select>
</td>

    <td><select name="param1Condition" id = "param1Condition">
                	<option> Choose a value</option>
                    </select></td>
    
     <td><select  id="param1Value" name="param1Value">
    <option>Choose a value</option>
     </select>
</td>
  </tr>
  <tr>
    <td>and</td>
    <td><select id="secondparameter">
                	<option>Null</option>
                    </select></td>
        <td><select name="param2Condition" id = "param2Condition">
                	<option> NULL</option>
                    </select></td>
    
     <td><select  id="param2Value" name="param2Value">
    <option>NULL</option>
     </select>
</td>
  </tr>
  <tr>
    <td>or</td>
    <td><select id="thirdparameter">
                	<option>Null</option>
                    </select></td>
                    
        <td><select name="param3Condition" id = "param3Condition">
                	<option> NULL</option>
                    </select></td>
    
     <td><select  id="param3Value" name="param3Value">
    <option>NULL</option>
     </select>
</td>
  </tr>
  <tr><td></td><td><input type="submit" class="button" value="Submit"/>
  </td>
 
 
  
</table>
 
  </div>

</form>
</body>
</html>
