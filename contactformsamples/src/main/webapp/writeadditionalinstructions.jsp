<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Service History Page</title>
<style>
table,th,td {
	border: 0px solid blue;
}
</style>
</head>
<body>
	<%-- ServiceHistory Add/Edit logic --%>
	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>
	<c:url value="/addServiceHistory" var="addURL"></c:url>
	

	

	<%-- Add Request --%>
<%-- 	<c:if test="${requestScope.ServiceHistory eq null}">  --%>
		<form action="addServiceHistory" method="post">
			<table border="3">
			 <tr>
				<td>Instructions</td>
			 	<td><textarea rows="10" cols="50" name="history"></textarea></td>
			 	
			 </tr>
			 <tr>
			 	<td> Additional instructions: </td>
			 	<td><textarea rows="10" cols="50" name="additionalinfo"></textarea></td>
			 </tr> 
			 <tr> 
			 <td></td>
			 <td><input type="submit"
				value="Add Additional Instructions"></td>
			</tr>
				</table>
		</form>
<%-- 	</c:if> --%>

	
	
	
</body>
</html>