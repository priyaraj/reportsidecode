<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.model.ReportParameterNames"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="com.ponnivi.mongodb.model.ReportOptionsDetails"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | custom report</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script>
function changedropdownvalues(reportparameternumber) {	
	//alert("reportparameternumber" + reportparameternumber);
	
	var firstParameterName;
	if (reportparameternumber == 1) {
		firstParameterName = document.getElementById('firstparameter').value;
	}
	if (reportparameternumber == 2) {
		firstParameterName = document.getElementById('secondparameter').value;
	}
	if (reportparameternumber == 3) {
		firstParameterName = document.getElementById('thirdparameter').value;
	}
	

	
			<%
		MongoClient mongo = (MongoClient) request.getServletContext()
		.getAttribute("MONGO_CLIENT");
		String databaseName = (String)request.getServletContext()
		.getAttribute("PONNIVI_DATABASE");
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		ReportOptionsDetails details = new ReportOptionsDetails();
		String retrievalParamName = "";
		//String itemCollectionName = (String)session.getAttribute("itemCollectionName");
	
		
		%>
	if (firstParameterName == "NULL") {
		var fillvalues = "",fillconditions = "";
		if (reportparameternumber == 2) {
			fillvalues = '<select id="param2Value" name="param2Value" ><option>=</option></select>';
			fillconditions = '<select id="param2Condition" name="param2Condition" ><option>=</option></select>';
			document.getElementById('param2Value').outerHTML = fillvalues;
			document.getElementById('param2Condition').outerHTML = fillconditions;
		}
		if (reportparameternumber == 3) {
			fillvalues = '<select id="param3Value" name="param3Value" ><option>=</option></select>';
			fillconditions = '<select id="param3Condition" name="param3Condition" ><option>=</option></select>';
			document.getElementById('param3Value').outerHTML = fillvalues;
			document.getElementById('param3Condition').outerHTML = fillconditions;
		}
		
	}
	if (firstParameterName == "Reasons") {
		<% retrievalParamName = "Reasons"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
	//	alert("firstParameterName12212.." + firstParameterName);
	var reasonsvalues = "",reasonsconditions = "";
	if (reportparameternumber == 1) {
		reasonsvalues = '<select id="param1Value" name="param1Value" >';
		reasonsconditions = '<select id="param1Condition" name="param1Condition" >';
	}
	if (reportparameternumber == 2) {
		reasonsvalues = '<select id="param2Value" name="param2Value" >';
		reasonsconditions = '<select id="param2Condition" name="param2Condition" >';
	}
	if (reportparameternumber == 3) {
		reasonsvalues = '<select id="param3Value" name="param3Value" >';
		reasonsconditions = '<select id="param3Condition" name="param3Condition" >';
	}
	
		<%
			
			List list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				reasonsvalues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			reasonsvalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = reasonsvalues;
			}

	}
	if (firstParameterName == "Machine DownTime") {
		<% retrievalParamName = "Machine DownTime"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
	//	alert("firstParameterName12212.." + firstParameterName);
	var reasonsvalues = "",reasonsconditions = "";
	if (reportparameternumber == 1) {
		reasonsvalues = '<select id="param1Value" name="param1Value" >';
		reasonsconditions = '<select id="param1Condition" name="param1Condition" >';
	}
	if (reportparameternumber == 2) {
		reasonsvalues = '<select id="param2Value" name="param2Value" >';
		reasonsconditions = '<select id="param2Condition" name="param2Condition" >';
	}
	if (reportparameternumber == 3) {
		reasonsvalues = '<select id="param3Value" name="param3Value" >';
		reasonsconditions = '<select id="param3Condition" name="param3Condition" >';
	}
	
		<%
			
			list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				reasonsvalues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			reasonsvalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = reasonsvalues;
			}

	}
	if (firstParameterName == "Action") {
		<% retrievalParamName = "Action"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
	//	alert("firstParameterName12212.." + firstParameterName);
	var reasonsvalues = "",reasonsconditions = "";
	if (reportparameternumber == 1) {
		reasonsvalues = '<select id="param1Value" name="param1Value" >';
		reasonsconditions = '<select id="param1Condition" name="param1Condition" >';
	}
	if (reportparameternumber == 2) {
		reasonsvalues = '<select id="param2Value" name="param2Value" >';
		reasonsconditions = '<select id="param2Condition" name="param2Condition" >';
	}
	if (reportparameternumber == 3) {
		reasonsvalues = '<select id="param3Value" name="param3Value" >';
		reasonsconditions = '<select id="param3Condition" name="param3Condition" >';
	}
	
		<%
			
			list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				reasonsvalues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			reasonsvalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = reasonsvalues;
			}

	}
	
	if (firstParameterName == "Machine Name") {
		<% retrievalParamName = "Machine Name"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
	//	alert("firstParameterName12212.." + firstParameterName);
	var reasonsvalues = "",reasonsconditions = "";
	if (reportparameternumber == 1) {
		reasonsvalues = '<select id="param1Value" name="param1Value" >';
		reasonsconditions = '<select id="param1Condition" name="param1Condition" >';
	}
	if (reportparameternumber == 2) {
		reasonsvalues = '<select id="param2Value" name="param2Value" >';
		reasonsconditions = '<select id="param2Condition" name="param2Condition" >';
	}
	if (reportparameternumber == 3) {
		reasonsvalues = '<select id="param3Value" name="param3Value" >';
		reasonsconditions = '<select id="param3Condition" name="param3Condition" >';
	}
	
		<%
			
			list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				reasonsvalues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			reasonsvalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = reasonsvalues;
			}

	}
	
	if (firstParameterName == "Supervisor Name") {
		<% retrievalParamName = "Supervisor Name"; 
		details = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions",retrievalParamName,customerDatabase);%>
	//	alert("firstParameterName12212.." + firstParameterName);
	var reasonsvalues = "",reasonsconditions = "";
	if (reportparameternumber == 1) {
		reasonsvalues = '<select id="param1Value" name="param1Value" >';
		reasonsconditions = '<select id="param1Condition" name="param1Condition" >';
	}
	if (reportparameternumber == 2) {
		reasonsvalues = '<select id="param2Value" name="param2Value" >';
		reasonsconditions = '<select id="param2Condition" name="param2Condition" >';
	}
	if (reportparameternumber == 3) {
		reasonsvalues = '<select id="param3Value" name="param3Value" >';
		reasonsconditions = '<select id="param3Condition" name="param3Condition" >';
	}
	
		<%
			
			list = details.getValuesList();
			for (int i = 0; i < list.size(); i++) {
				%>
				reasonsvalues += '<option><%= list.get(i) %></option>';
				<%
			}
			%>
			reasonsvalues += '</select>';
			if (reportparameternumber == 1) {
			document.getElementById('param1Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 2) {
				document.getElementById('param2Value').outerHTML = reasonsvalues;
			}
			if (reportparameternumber == 3) {
				document.getElementById('param3Value').outerHTML = reasonsvalues;
			}

	}


	
}
function loadFirstParamValues() {
	<%
	
	List evidenceIdentifierList = new ArrayList();
	
	
	evidenceIdentifierList = (List)request.getAttribute("evidenceIdentifierList");
	if (evidenceIdentifierList.size() > 0) {
		%>
		var evidenceIdentifieroptions = '<select id="evidenceIdentifier" name="evidenceIdentifier" ><option>ALL</option>';
		<%		
	     for(int i = 0; i < evidenceIdentifierList.size();i++)
	     { 
	    	 if (!evidenceIdentifierList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     evidenceIdentifieroptions += '<option><%= evidenceIdentifierList.get(i).toString() %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	evidenceIdentifieroptions += '</select>';
	<%
	List<ReportParameterNames> reportparameterlist=new ArrayList<ReportParameterNames>();

	 reportparameterlist = (ArrayList)request.getAttribute("reportparameterlist");	 

	 if(!reportparameterlist.isEmpty())
	 { %>
	 var options = '<select id="firstparameter" name="firstparameter" onchange="changedropdownvalues(1)">';
	 var options1 = '<select id="secondparameter" name="secondparameter" onchange="changedropdownvalues(2)"><option>NULL</option>';
	 var options2 = '<select id="thirdparameter" name="thirdparameter" onchange="changedropdownvalues(3)"><option>NULL</option>';
	 <%
	     for(ReportParameterNames info: reportparameterlist)
	     { 
	     %>
	     options += '<option><%= info.getParameterName() %></option>';	
	     options1 += '<option><%= info.getParameterName() %></option>';
	     options2 += '<option><%= info.getParameterName() %></option>';
	        <% 
	     }

	 }
	%>
	options += '</select>';
	options1 += '</select>';
	options2 += '</select>';
	document.getElementById('firstparameter').outerHTML = options;
	document.getElementById('secondparameter').outerHTML = options1;
	document.getElementById('thirdparameter').outerHTML = options2;
	
	<%
	MongoClient mongo1 = (MongoClient) request.getServletContext()
	.getAttribute("MONGO_CLIENT");
	String databaseName1 = (String)request.getServletContext()
	.getAttribute("PONNIVI_DATABASE");
	ReportOptionsDetails details1 = new ReportOptionsDetails();

	
	details1 = MongoDBReportDAO.getParameterValuesOrConditions("report_param_collection","values_and_conditions","Reasons",customerDatabase);%>
//	alert("firstParameterName12212.." + firstParameterName);
	var reasonsvalues = '<select id="param1Value" name="param1Value" >';
	var reasonsconditions = '<select id="param1Condition" name="param1Condition" >';
	<%
		
		List list1 = details1.getValuesList();
		for (int i = 0; i < list1.size(); i++) {
			%>
			reasonsvalues += '<option><%= list1.get(i) %></option>';
			<%
		}
		%>
		reasonsvalues += '</select>';
		document.getElementById('param1Value').outerHTML = reasonsvalues;
		
		
		document.getElementById('evidenceIdentifier').outerHTML = evidenceIdentifieroptions;	

}

</script>

<body onload="loadFirstParamValues()">
	<%-- ServiceHistory Add/Edit logic --%>
	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      
      	<h1>Custom Report Remote Units</h1>	   	
		

         <hr>
<form name="customizedreport" action="customreport" method="post">              
   <div class="report" align="center">
   			<h4> Report Title</h4>
   			    	<input type="text" class="inptxt" name="reporttitle"/>
     </div>  
     
     <div class="toolbox" >
     
     <h8>Choose a remote location</h8>
     
   			 <select name="evidenceIdentifier" id = "evidenceIdentifier">
                	<option>=</option>
                    </select>
       </div> 
        <div class="date">
        <h8>Enter From date <br>dd-mm-yy format</h8>
<input type="text"  name="fromdateParam" id="fromdateParam"/>        
		
		 
        <h8>Enter To date <br>dd-mm-yy format</h8>
<input type="text"  name="todateParam" id="todateParam"/>        

 <h9>(or)</h9>
<h8>Select date Patterns</h8>
<select  id="datepattern" name="datepattern" >
	<option>NULL</option>
    <option>Less than a week</option>
    <option>Less than a month</option>
    <option>Less than 6 months</option>
    <option>Less than a year</option>
     </select>  
		
       </div> 

   
 <div align="center">
   <table class="tblcls" width="200" >
  <tr>
    <td></td>
    <td></td>
    <td>
     </td>
    <td></td>
  </tr>
  <tr>
    <td></td>

  
    <td><select  id="firstparameter" name="firstparameter" onChange="changedropdownvalues()">
    <option>Choose a parameter</option>
     </select>
</td>

    <td><select name="param1Condition" id = "param1Condition">
                	<option>=</option>
                    </select></td>
    
     <td><select  id="param1Value" name="param1Value">
    <option>Choose a value</option>
     </select>
</td>
  </tr>
  <tr>
    <td>and</td>
    <td><select id="secondparameter">
                	<option>=</option>
                    </select></td>
        <td><select name="param2Condition" id = "param2Condition">
                	<option> =</option>
                    </select></td>
    
     <td><select  id="param2Value" name="param2Value">
    <option>NULL</option>
     </select>
</td>
  </tr>
  <tr>
    <td>or</td>
    <td><select id="thirdparameter">
                	<option>Null</option>
                    </select></td>
                    
        <td><select name="param3Condition" id = "param3Condition">
                	<option> =</option>
                    </select></td>
    
     <td><select  id="param3Value" name="param3Value">
    <option>NULL</option>
     </select>
</td>
  </tr>
  <tr><td></td><td><input type="submit" class="button" value="Submit"/>
  </td>
 
 
  
</table>
 
  </div>

</form>
</body>
</html>
