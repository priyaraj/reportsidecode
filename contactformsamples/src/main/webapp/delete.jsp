<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | Delete Screen</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>

<body>
	  <div id="logo">
      		<img src="images/instaunite_logo.png"/>
      </div>
	  <h1>Delete screen</h1>	   	
       <hr>
       <table>
  <tr>
    <td id="align"><h4> Delete toolbox </h4></td>
    <td> <select id="tool1"> 
             		<option value=" Toolbox 1">Toolbox 1</option>
                    <option value=" Toolbox 2">Toolbox 2</option>
                  	<option value=" Toolbox 3">Toolbox 3</option>
             </select></td>
  </tr>
</table>
     <div class="inpsbtn" align="center">
       		<input type="submit"  class="updatebutton" value="Submit"/>
	   </div>
       <table id="deleteitem">
  <tr>
    <td id="align"><h4> Delete item </h4> </td>
    <td> <select id="tool3"> 
             		<option value=" Toolbox 1">Item 1</option>
                    <option value=" Toolbox 2">Item 2</option>
                  	<option value=" Toolbox 3">Item 3</option>
             </select></td>
  </tr>
  <tr>
    <td id="align">&nbsp;</td>
    <td> <select id="tool2" multiple="multiple" size="3"> 
             		<option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option>
             </select></td>
  </tr>
</table>
 <div class="inpsbtn" align="center">
       		<input type="submit" class="updatebutton" value="Submit"/>
 </div>
</body>
</html>
