<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.model.ReportParameterNames"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="com.ponnivi.mongodb.model.ReportOptionsDetails"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Instaunite | Reports </title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>

<body>
<script>
function redirect(){
var actionURL ="MENTION URL YOU WANT TO REDIRECT";
// perform your operations
<% String contextType = (String)session.getAttribute("contextType"); %>
myForm.submit(actionURL); OR
myForm.submit();
}


</script>


<form action="reportsMenu" method="get">
	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
	   	<h1>Report Menu</h1>
         <hr>
           <div id="option" align="center">
            <table width="200" align="center" >
    <%   if (contextType.equalsIgnoreCase("Rotating_assets")) {  %>
  <tr>
      <td ><input type="submit" value="Your predefined Reports" class="menu" name="report"  /> </td>
  </tr>
  <tr>
      <td ><input type="submit" value="Custom Report Toolboxes" class="menu" name="report" /> </td>
  </tr>
  <tr>
    <td><input type="submit" value="Custom Report Equipments" class="menu" name="report" /></td>
  </tr>
  <tr>
    <td><input type="submit" value="Custom Reports - Graph Format" class="menu" name="report" /></td>
  </tr>
  <tr>
    <td > <input type="submit" value="Recommendation Reports" class="menu" name="report"/></td>
  </tr>
   <%}
    else if (contextType.equalsIgnoreCase("machine_uptime")){%>
   <tr>
      <td ><input type="submit" value="Your predefined Reports" class="menu" name="report"  /> </td>
  </tr>
  <tr>
      <td ><input type="submit" value="Custom Report Machine" class="menu" name="report" /> </td>
  </tr>
  <tr>
    <td><input type="submit" value="Custom Report - Others" class="menu" name="report" /></td>
  </tr>
  <tr>
    <td><input type="submit" value="Custom Reports - Graph Format" class="menu" name="report" /></td>
  </tr>
  <tr>
    <td > <input type="submit" value="Recommendation Reports" class="menu" name="report"/></td>
  </tr>
  <tr>
    <td > <input type="submit" value="Different Reports - AwesomeChartJs" class="menu" name="report"/></td>
  </tr>
   <%}%>
    
</table>
</div>
</form>
       </body>
       
</html>
