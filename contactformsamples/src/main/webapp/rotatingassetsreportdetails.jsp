<%@page import="com.mongodb.DBObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | custom report</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>


<body>

	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
<h1>Custom Report</h1>
<h2><%= (String)request.getAttribute("reportTitle") %></h2>



 
    
<form>   
 <div align="center">
          <hr>
         <br>
<h3><%= (String)request.getAttribute("reportCondtions") %></h3>	
  <% 
 List<DBObject> report1DetailsList=new ArrayList<DBObject>();

 report1DetailsList = (ArrayList)request.getAttribute("report1Details");	 
Double totalPrice = 0.0;
if (report1DetailsList == null ) {
	%>
	<table class="nomatch">
 <tr>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Invalid date format entered</FONT></td>
 </tr>
 </table>
 <%
}
else if(report1DetailsList != null && !report1DetailsList.isEmpty()) {
	%>
    	   	

 <table class="description" >
 <tr >
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No.</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">ToolBox</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Article Number</FONT></td> 
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Product Name</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Category</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Owner</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Price in Euro</FONT></td>
 </tr>

 <%
 	int srNo = 0;
     for(DBObject info: report1DetailsList)
     { 
    	 srNo = srNo + 1;
    	 totalPrice += (Integer)info.get("C");
     %>
     <tr BGCOLOR="F1F1F1">
     <td ><FONT COLOR="#333333">
     <%= srNo  %>
     </FONT></td>
      <td ><FONT COLOR="#333333">
     <%= info.get("Evidence_Identifier") %>
     </FONT></td>
     <td><FONT COLOR="#333333">
     <%= info.get("A") %>
     </FONT></td>
     <td><FONT COLOR="#333333">
     <%= info.get("B") %>
     </FONT></td>
      <td><FONT COLOR="#333333">
     <%= info.get("Qualifier_8") %>
     </FONT></td>
     <td><FONT COLOR="#333333">
     <%= info.get("D") %>
     </FONT></td>                    
     <td align="right"><FONT COLOR="#333333">
     <%= Double.parseDouble(info.get("C").toString()) %>
     </FONT></td>
     </tr>
        <% 
     }
     

 
 if (totalPrice > 0) {
%>
 <tr  BGCOLOR="F1F1F1">
  <td></td><td></td><td><FONT COLOR="#333333">Total</FONT></td><td></td><td></td><td></td>
  <td align="right"><FONT COLOR="#333333">
 <%= totalPrice %>
 </FONT></td>
 </tr>
 <% }
 }
else { %>
	
	<table class="nomatch">
 <tr>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No matching records found</FONT></td>
 </tr>
 </table>
<% }
 %>
 </table>
 
  </div>

</form>
</body>
</html>
