<%@page import="com.mongodb.AggregationOutput"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | predefined report</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
<script src='js/Chart.min.js'></script>
</head>


<body>
<%!
    
    public String getTopMost(AggregationOutput aggregationOutput){
		int idx = 0;
		String topRecord = "";
		 for (DBObject result : aggregationOutput.results()) { 
			 idx += 1;
			 if  (idx == 1) {
				 topRecord = (String)result.get("_id");
				 break;
		 	}
		 }
        return topRecord;
    }

public List computeChartValues(AggregationOutput aggregationOutput) {
	int idx = 0;
	String idStr = "",timesStr="";
	List chartList = null;
	 if(aggregationOutput != null) {
		 idx = 0;
		 chartList = new ArrayList();
	 for (DBObject result : aggregationOutput.results()) {      
	    	idx += 1;
		if(idx>5){
		break;}
		if (idx < 5) {
		idStr +=  "\"" + result.get("_id") + "\",";
		System.out.println("idstr=" + idStr);
		timesStr += result.get("times") +",";
		}
		else {
			idStr += "\"" + result.get("_id") + "\"";
			timesStr += result.get("times");
		}
		 
	 } 
	 chartList.add(idStr);
	 chartList.add(timesStr);
	} 
	 return chartList;
}

public List getSummationValues(AggregationOutput aggregationOutput) {
	int idx = 0;
	List summationList = null;
	 if(aggregationOutput != null) {
		 idx = 0;
		 summationList = new ArrayList();
	 for (DBObject result : aggregationOutput.results()) {      
	    	idx += 1;
		if(idx>5){
		break;}
		summationList.add(result.get("times"));
	 }	  
		}
	 return summationList;	
}
%>

	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>

<%-- <h2><%= (String)request.getAttribute("reportTitle") %></h2> --%>
    
<form>   
<h1>Weekly Machine Report</h1>
 <div align="center">
          <hr>
         <br>
<%-- <h6>>&nbsp;&nbsp;<%= (String)request.getAttribute("reportCondtions") %></h6>	 --%>
<table align="center" class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333">Total DownTime:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= session.getAttribute("totalDownTime")  %> minutes</h6>
<h6><FONT COLOR="#333333">Average DownTime:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= session.getAttribute("averageDownTime")  %> minutes</h6>
  <% 
  
  AggregationOutput aggregationOutput = null;
  String fieldName = "";
  String heading = "";
  String caption = "";
  int idx = 0;
  String idStr = "",timesStr = "";
  List chartList = null;
  caption = "Most Sighted Reason";
  aggregationOutput = (AggregationOutput)session.getAttribute("reasonOutput");
  String topMost = "";
  if (aggregationOutput == null ) {
		%>
		<table class="nomatch">
	 <tr>
	 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No data is observed</FONT></td>
	 </tr>
	 </table>
	 <%
	}
	else {
		topMost = getTopMost(aggregationOutput);
		%>    	   	

	 <table align="center" class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333"><%= caption %>:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= topMost  %></h6>
	 <%	 
	 
	 }

  aggregationOutput = (AggregationOutput)session.getAttribute("machinedowntimeOutput");
  caption = "Most Occured Downtime";
  if (aggregationOutput == null ) {
		%>
		<table class="nomatch">
	 <tr>
	 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No data is observed</FONT></td>
	 </tr>
	 </table>
	 <%
	}
	else  {	
		topMost = getTopMost(aggregationOutput);
	%>    	   
	 <table class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333"> <%= caption %>:</FONT>&nbsp;&nbsp; <%= topMost  %></h6>
	 <%	 	 
	 }
  
  aggregationOutput = (AggregationOutput)session.getAttribute("actionOutput");
  caption = "Most Taken Action";
  if (aggregationOutput == null ) {
		%>
		<table class="nomatch">
	 <tr>
	 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No data is observed</FONT></td>
	 </tr>
	 </table>
	 <%
	}
	else  {	
		topMost = getTopMost(aggregationOutput);
	%>    	   	

	 <table class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333"><%= caption %>:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= topMost  %></h6>
	 <%	 
	 
	 }
	
 
 %>
 <h3>Reasons chart</h3>
 
 </table>
 <!-- line chart canvas element -->
        <canvas id="Reasons" width="600" height="400"></canvas>
        <!-- bar chart canvas element -->
        <canvas id="reasonsInBarChart" width="600" height="400"></canvas>
        <!-- pie chart canvas element -->
        <canvas id="reasonsInPieChart" width="600" height="400"></canvas>
        <!--  doughNutChartLoc element -->
        <canvas id="reasonsInDoughnut" height="300" width="300"></canvas>
 <script>
            // line chart data
            <%
aggregationOutput = (AggregationOutput)session.getAttribute("reasonOutput");
  chartList = computeChartValues(aggregationOutput) ;
	if (chartList != null) {	
     %>
     
    	   	
 	
 	var ReasonsData;         
  
     ReasonsData = {
    		 labels : [<%= chartList.get(0) %>],
     datasets : [
     {
         fillColor : "#2392ff",
         strokeColor : "#ff9326",
         pointColor : "#fff",
         pointStrokeColor : "#9DB86D",
         data : [<%= chartList.get(1)  %>]
     }
 ]
 }
     
        
            
            // get line chart canvas
            var Reasons = document.getElementById('Reasons').getContext('2d');
            // draw line chart
            new Chart(Reasons).Line(ReasonsData);
            
             // bar chart data
            var barData = {
                labels : [<%= chartList.get(0) %>],
                datasets : [
                    {
                        fillColor : "#ff9326",
                        strokeColor : "#2392ff",
                        data : [<%= chartList.get(1)  %>]
                    }
                ]
            }
             
             
             
         // get bar chart canvas
            var reasonsInBarChart = document.getElementById("reasonsInBarChart").getContext("2d");
            // draw bar chart
            new Chart(reasonsInBarChart).Bar(barData);
            
            <% } %>
            
            
         // pie chart data
         <%    chartList = getSummationValues(aggregationOutput); %>
            var pieData = [
                {
                    value: <%= chartList.get(0) %>,
                    color:"#878BB6"
                },
                {
                    value : <%= chartList.get(1) %>,
                    color : "#4ACAB4"
                },
                {
                    value : <%= chartList.get(2) %>,
                    color : "#FF8153"
                },
                {
                    value : <%= chartList.get(3) %>,
                    color : "#2392ff"
                },
                {
                    value : <%= chartList.get(4) %>,
                    color : "#ff9326"
                },
            ];
            // pie chart options
            var pieOptions = {
                 segmentShowStroke : false,
                 animateScale : true
            }
            // get pie chart canvas
            var reasonsInPieChart= document.getElementById("reasonsInPieChart").getContext("2d");
            // draw pie chart
            new Chart(reasonsInPieChart).Pie(pieData, pieOptions);
            
            //dounought chart
            var reasonsInDoughNutChart = [
                                     {
                                         value: <%= chartList.get(0) %>,
                                         color:"#878BB6"
                                     
                                     },
                                     {
                                         value: <%= chartList.get(1) %>,
                                         color : "#4ACAB4"
                                     },
                                     {
                                         value: <%= chartList.get(2) %>,
                                         color : "#FF8153"
                                     },
                                     {
                                         value: <%= chartList.get(3) %>,
                                         color:"#2392ff"
                                     
                                     },
                                     {
                                         value: <%= chartList.get(4) %>,
                                         color : "#ff9326"
                                     },
                                 ]
                                 new Chart(document.getElementById
                                 ("reasonsInDoughnut").getContext("2d")).Doughnut
                     (reasonsInDoughNutChart);
 </script>
  

</form>
</body>
</html>
