<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.model.ReportParameterNames"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="com.ponnivi.mongodb.model.ReportOptionsDetails"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | Update Existing fields</title>



<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script type="text/javascript">
function changeexistingvalues(updatablecolumns)
{	
var xmlhttp;
//var updatablecolumns=document.updateexisting.updatablecolumns.value
//alert("changeexistingvalues" + updatablecolumns);
//var urls="http://localhost:8080/ajaxproject/fetch.jsp?ok="+keys
var urls="getdata.jsp?updatablecolumns="+updatablecolumns		
		
if (window.XMLHttpRequest)
  {
  xmlhttp=new XMLHttpRequest();
  //alert("urls in if.."+urls);
  }
else
  {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  //alert("urls in else.."+urls);
  }
 
xmlhttp.onreadystatechange=function()
  {
	
removeall();

  if (xmlhttp.readyState==4)
    {
	 // alert("inside onreadystatechange" + xmlhttp.readyState);
            z=0;
         //   var roott=xmlhttp.responseText;
         var roott=xmlhttp.responseXML.documentElement;
        //    alert("inside onreadystatechange" + roott);
             var ress=roott.getElementsByTagName("existingvalues")[z].childNodes[0].nodeValue;
 
            while(ress!=null)
            {
                    addoptions(ress)
                    z++
             var ress=roott.getElementsByTagName("existingvalues")[z].childNodes[0].nodeValue;
            } 
    }
 
    function removeall()
    {
 
        var ct=document.updateexisting.existingvalue.length;
        for(i=ct; i>=0; i--)    {    
            document.updateexisting.existingvalue.options[i]=null;
             }
    }
 
    function addoptions(reslt)
    {
 
        var ct1=document.createElement("OPTION");
        ct1.text=reslt;
        ct1.value=reslt;
        document.updateexisting.existingvalue.options.add(ct1);
 
    }
}    
 
xmlhttp.open("GET",urls,true);
xmlhttp.send();
}


function loadUpdatableFieldValues() {
<%
	
	List updatableFieldList 	= new ArrayList();
	
	
	updatableFieldList = (ArrayList)session.getAttribute("updatableColumns");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableFieldListOptions = '<select id="updatablecolumns" name="updatablecolumns" onchange="changeexistingvalues(document.updateexisting.updatablecolumns.value)" >';
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     updatableFieldListOptions += '<option><%= updatableFieldList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('updatablecolumns').outerHTML = updatableFieldListOptions;
	<%
	List collectionNamesList 	= new ArrayList();
	
	
	collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
	if (!collectionNamesList.isEmpty()) {
		%>
		var collectionNamesListOptions = '<select id="itemcollectionnames" name="itemcollectionnames" size="3" multiple="multiple">';
		<%		
	     for(int i = 0; i < collectionNamesList.size();i++)
	     { 
	    	 if (!collectionNamesList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     collectionNamesListOptions += '<option><%= collectionNamesList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	collectionNamesListOptions += '</select>';
	document.getElementById('itemcollectionnames').outerHTML = collectionNamesListOptions;
	
	changeexistingvalues("Incharge");
	}

</script>


<body onload="loadUpdatableFieldValues()">
<script type="text/javascript" src="js/adminfunctions.js"></script>
<%!
    
    public List removeDuplicates(List entireList){
	//For removing duplicate entries
	if (entireList != null) {
			int count = entireList.size();
			System.out.println("count.."+count);
		    for (int i = 0; i < count; i++) 
		    {
		        for (int j = i + 1; j < count; j++) 
		        {
		            if (entireList.get(i).equals(entireList.get(j)))
		            {
		            	entireList.remove(j--);
		                count--;
		            }
		        }
		    }
		    return entireList;
    }
	else
		return null;
}

	public List addElementsToList(List inputList,List outputList) {
		if (inputList != null) {
		for (int idx = 0; idx < inputList.size(); idx++) {
			outputList.add(inputList.get(idx).toString());
			}
		return outputList;
		}
		else
			return null;
	}
%>
 <form name = "updateexisting" action="updateExistingFields" method="post" onsubmit="return(validateForm(this));"> 

	  <div id="logo">
      		<img src="images/instaunite_logo.png"/>
      </div>
	  <h1>Update Screen (existing fields)</h1>	   	
       <hr>
       <table style="width:100%;">
  <tr>
    <td id="align"><h4> Choose field</h4></td>
    <td> <select id="updatablecolumns" name = "updatablecolumns" onchange="changeexistingvalues()"> 
             		<!-- <option> Incharge </option>
                    <option> Price </option>
                  	<option> Added </option> -->
             </select></td>
  </tr>
  <tr>
    <td id="align"><h4> Choose collection</h4></td>
    <td><select id="itemcollectionnames" size="3" multiple="multiple" name="itemcollectionnames" > 
             		<!-- <option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option> -->
             </select></td>
  </tr>
  <tr>
    <td id="align"><h4> Existing value </h4></td>
    <td><select id="existingvalue" name="existingvalue"> 
  </tr>
  <tr>
    <td id="align"><h4> New value  </h4></td>
    <td><input type="text" class="inptxt1" name="newvalue"/></td>
  </tr>
</table>
 <div class="inpsbtn" align="center">
       		<input type="submit" class="updatebutton" value="Update" />
 </div>
 </form>
</body>
</html>

