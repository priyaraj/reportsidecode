<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | Update Existing fields</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>

<body>
	  <div id="logo">
      		<img src="images/instaunite_logo.png"/>
      </div>
	  <h1>Update Screen (existing fields): new category</h1>	   	
       <hr>
       <table>
  <tr>
    <td id="align"><h4> New category</h4></td>
    <td><input type="text" class="inptxt1" name="reporttitle"/></td>
  </tr>
  <tr>
    <td id="align">&nbsp;</td>
    <td><input type="submit"  class="updatecategorybtn" value="Update categories"/></td>
  </tr>
  <tr>
    <td id="align">&nbsp;</td>
    <td><select id="tool" multiple="multiple" size="3"> 
             		<option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option>
             </select></td>
  </tr>
  <tr>
    <td id="align"><h4> Existing value</h4></td>
    <td> <select id="updatedrop"> 
             		<option> Miscellaneous </option>
                    <option> Powertool </option>
                    <option> Spanner </option>
                  	<option> Screwdriver </option>
          </select></td>
  </tr>
  <tr>
    <td id="align"><h4> New value</h4></td>
    <td> <select id="updatedrop"> 
                    <option> New category </option>
             		<option> Powertool </option>
                    <option> Spanner </option>
                  	<option> Screwdriver </option>
                    <option> Miscellaneous </option>
             </select></td>
  </tr>
</table>
<div class="inpsbtn" align="center">
       		<input type="submit" class="updatebutton" value="Submit"/>
</div>
</body>
</html>