<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mongodb.DB"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
 <%!
    
    public List removeDuplicates(List entireList){
	//For removing duplicate entries
	if (entireList != null) {
			int count = entireList.size();
			System.out.println("count.."+count);
		    for (int i = 0; i < count; i++) 
		    {
		        for (int j = i + 1; j < count; j++) 
		        {
		            if (entireList.get(i).equals(entireList.get(j)))
		            {
		            	entireList.remove(j--);
		                count--;
		            }
		        }
		    }
		    for (int i = 0; i < entireList.size();i++) {
		    	System.out.println("element.." + entireList.get(i).toString());
		    }
		    return entireList;
    }
	else
		return null;
}

	public List addElementsToList(List inputList,List outputList) {
		if (inputList != null) {
		for (int idx = 0; idx < inputList.size(); idx++) {
			outputList.add(inputList.get(idx).toString());
			}
		return outputList;
		}
		else
			return null;
	}
	
	public List addElementsToList(List inputList) {
    	List outputList = null;
		if (inputList != null) {
		for (int idx = 0; idx < inputList.size(); idx++) {
			System.out.println("element.." + inputList.get(idx).toString());
			if (!inputList.get(idx).toString().equalsIgnoreCase("")) {
				if (idx == 0) {
					outputList = new ArrayList();
				}
				outputList.add(inputList.get(idx).toString());
			}
			}
		return outputList;
		}
		else
			return null;
	}
	public List getSessionElementAndPopulateTheList(String contextType,String updatableColumn,String collectionName,HttpSession session) {
		List elementList = new ArrayList();
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
		if (collectionName.equalsIgnoreCase("items_collection")) {
			if (updatableColumn.equalsIgnoreCase("Incharge")) {
				elementList = (ArrayList)session.getAttribute("items_collection_d_value");
			}
			else if (updatableColumn.equalsIgnoreCase("price")) {
				elementList = (ArrayList)session.getAttribute("items_collection_c_value");
			}
			else if (updatableColumn.equalsIgnoreCase("ArticleNumber")) {
				elementList = (ArrayList)session.getAttribute("items_collection_a_value");
			}
			else if (updatableColumn.equalsIgnoreCase("ProductName")) {
				elementList = (ArrayList)session.getAttribute("items_collection_b_value");
			}
			else if (updatableColumn.equalsIgnoreCase("Missing_Item_Text")) {
				elementList = (ArrayList)session.getAttribute("items_collection_missing_text");
			}
			else if (updatableColumn.equalsIgnoreCase("Missing_Item_Color")) {
				elementList = (ArrayList)session.getAttribute("items_collection_missing_color");
			}
			else if (updatableColumn.equalsIgnoreCase("To_Service_Item_Text") ) {
				elementList = (ArrayList)session.getAttribute("items_collection_service_text");
			}			 
			else if (updatableColumn.equalsIgnoreCase("To_Service_Item_Color")) {
				elementList = (ArrayList)session.getAttribute("items_collection_service_color");
			}
			else if (updatableColumn.equalsIgnoreCase("Added_Item")) {
				elementList = (ArrayList)session.getAttribute("items_collection_added_stauts");
			}
			else if (updatableColumn.equalsIgnoreCase("Scanned_Item")) {
				elementList = (ArrayList)session.getAttribute("items_collection_scanned_status");
			}
			else if (updatableColumn.equalsIgnoreCase("Deleted_Item")) {
				elementList = (ArrayList)session.getAttribute("items_collection_deleted_status");
			}
			else if (updatableColumn.equalsIgnoreCase("Category")) {
				elementList = (ArrayList)session.getAttribute("items_collection_qualifier8_value");
				for (int i = 0; i < elementList.size();i++) {
					System.out.println("category.."+elementList.get(i).toString());
				}
			}
			else {
				elementList = (ArrayList)session.getAttribute(collectionName + "_" + updatableColumn);
				if (elementList == null) {  //When we login, this list will be null and that time we need to query and get
					DB customerDatabase = (DB)session.getAttribute("customerDb");
					try {
					elementList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,updatableColumn);
					}catch(Exception ex) {
						
					}
				}
			}
		}
		if (collectionName.equalsIgnoreCase("items_equipments_collection")) {
			if (updatableColumn.equalsIgnoreCase("Incharge")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_d_value");
			}
			else if (updatableColumn.equalsIgnoreCase("price")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_c_value");
			}
			else if (updatableColumn.equalsIgnoreCase("ArticleNumber")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_a_value");
			}
			else if (updatableColumn.equalsIgnoreCase("ProductName")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_b_value");
			}
			else if (updatableColumn.equalsIgnoreCase("Missing_Item_Text")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_missing_text");
			}
			else if (updatableColumn.equalsIgnoreCase("Missing_Item_Color")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_missing_color");
			}
			else if (updatableColumn.equalsIgnoreCase("To_Service_Item_Text") ) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_service_text");
			}			 
			else if (updatableColumn.equalsIgnoreCase("To_Service_Item_Color")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_service_color");
			}
			else if (updatableColumn.equalsIgnoreCase("Added_Item")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_added_stauts");
			}
			else if (updatableColumn.equalsIgnoreCase("Scanned_Item")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_scanned_status");
			}
			else if (updatableColumn.equalsIgnoreCase("Deleted_Item")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_deleted_status");
			}
			else if (updatableColumn.equalsIgnoreCase("Category")) {
				elementList = (ArrayList)session.getAttribute("items_equipments_collection_qualifier8_value");
			}
			else {
				elementList = (ArrayList)session.getAttribute(collectionName + "_" + updatableColumn);
				if (elementList == null) {  //When we login, this list will be null and that time we need to query and get
					DB customerDatabase = (DB)session.getAttribute("customerDb");
					try {
					elementList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,updatableColumn);
					}catch(Exception ex) {
						
					}
				}
			}
		}
		}
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			if (updatableColumn.equalsIgnoreCase("Reasons")) {
				elementList = (ArrayList)session.getAttribute("items_collection_reasons_value");
			}
			else if (updatableColumn.equalsIgnoreCase("Machine DownTime")) {
				elementList = (ArrayList)session.getAttribute("items_collection_machine_downtime_value");
			}
			else if (updatableColumn.equalsIgnoreCase("Action")) {
				elementList = (ArrayList)session.getAttribute("items_collection_action_value");
			} 
			else if (updatableColumn.equalsIgnoreCase("remoteunit")) {
				elementList = (ArrayList)session.getAttribute("items_collection_remoteunit_value");
			}
			else if (updatableColumn.equalsIgnoreCase("Machine Name")) {
				elementList = (ArrayList)session.getAttribute("items_collection_Machine_Name_value");
			}
			else if (updatableColumn.equalsIgnoreCase("Supervisor Name")) {
				elementList = (ArrayList)session.getAttribute("items_collection_Supervisor_Name_value");
			}
			else {
				elementList = (ArrayList)session.getAttribute(collectionName + "_" + updatableColumn);
				if (elementList == null) {  //When we login, this list will be null and that time we need to query and get
					DB customerDatabase = (DB)session.getAttribute("customerDb");
					try {
					elementList = MongoDBReportDAO.getDistinct(customerDatabase,collectionName,updatableColumn);
					}catch(Exception ex) {
						
					}
				}
			}
			
			
		}
		return elementList;
	}
%>
<%
            response.setContentType("text/xml");
            String updatableColumn=request.getParameter("updatablecolumns");
            System.out.println("updatableColumn.."+updatableColumn);
            //Adding contextType to find out the values from particular
            String contextType = (String)session.getAttribute("contextType");
            List collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
            List list 	= null;
        	List entireList = null;
        		for (int i = 0; i < collectionNamesList.size();i++) {
        			list = getSessionElementAndPopulateTheList(contextType,updatableColumn, collectionNamesList.get(i).toString(), session);
        			if (!collectionNamesList.get(i).toString().equalsIgnoreCase("items_equipments_collection")) {
       				if (list != null) {
        				entireList = addElementsToList(list);
        				}
        			}
        			else {
       				 if (list != null) {
 						if (entireList != null) {
 							entireList = addElementsToList(list,entireList);
 						}
 						else {
 							entireList = addElementsToList(list);
 						}
 					}
        			}
        		}  //for
        	entireList = removeDuplicates(entireList);
        	if (entireList != null) {
 		     out.println("<existing>");
                    for (int i = 0; i < entireList.size(); i++)
                    {     
                    	System.out.println("value.." + entireList.get(i).toString());
                        out.println("<existingvalues>"+ entireList.get(i).toString() + "</existingvalues>");
 
                    }
                    out.println("</existing>");
        	}
        	else {
        		out.println("<existing>");
        		out.println("<existingvalues>NULL</existingvalues>");
        		out.println("</existing>");
        	}
 
%>