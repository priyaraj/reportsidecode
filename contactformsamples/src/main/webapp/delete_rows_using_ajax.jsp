<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ponnivi.mongodb.model.ReportOptionsDetails"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBReportDAO"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | Delete Screen (rows)</title>
<script type="text/javascript" src="js/adminfunctions.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script type="text/javascript" src="js/adminfunctions.js"></script>
<script type="text/javascript">
function initialize() {
	<%
	
	List updatableFieldList 	= new ArrayList();
	
	
	updatableFieldList = (ArrayList)session.getAttribute("updatableColumns");
	if (!updatableFieldList.isEmpty()) {
		%>
		var updatableColumnValue = document.deleterow.updatablecolumns.value;
		var existingValueDropdown = document.deleterow.existingvalue;
		var conditionalDropdown = document.deleterow.conditions;
		var updatableFieldListOptions = '<select id="updatablecolumns" name="updatablecolumns" onchange="changeexistingvaluesandconditions(document.deleterow.updatablecolumns.value,document.deleterow.existingvalue,document.deleterow.conditions);" >';
		//var updatableFieldListOptions = '<select id="updatablecolumns" name="updatablecolumns" onchange="alert("hai");" >';
		//alert("updatableFieldListOptions.." + updatableFieldListOptions);
		<%		
	     for(int i = 0; i < updatableFieldList.size();i++)
	     { 
	    	 if (!updatableFieldList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     updatableFieldListOptions += '<option><%= updatableFieldList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	updatableFieldListOptions += '</select>';
	document.getElementById('updatablecolumns').outerHTML = updatableFieldListOptions;
	<%
	List collectionNamesList 	= new ArrayList();
	
	
	collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
	if (!collectionNamesList.isEmpty()) {
		%>
		var collectionNamesListOptions = '<select id="itemcollectionnames" name="itemcollectionnames" size="3" multiple="multiple">';
		<%		
	     for(int i = 0; i < collectionNamesList.size();i++)
	     { 
	    	 if (!collectionNamesList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     collectionNamesListOptions += '<option><%= collectionNamesList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	collectionNamesListOptions += '</select>';
	document.getElementById('itemcollectionnames').outerHTML = collectionNamesListOptions;
	changeexistingvaluesandconditions("<%= updatableFieldList.get(0) %>",document.deleterow.existingvalue,document.deleterow.conditions);
}

</script>
<body onload="initialize()">
<form name = "deleterow" action="deleteRows" method="post" onsubmit="return(validateForm(this));">


	  <div id="logo">
      		<img src="images/instaunite_logo.png"/>
      </div>
	  <h1>Delete Screen (rows)</h1>	   	
       <hr>
       <table style="width:100%;padding-top: 30px;">
  <tr>
    <td ><h4> Choose collection</h4></td>
    <td><select id="itemcollectionnames" size="3" multiple="multiple" name="itemcollectionnames"> 
             		<!-- <option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option> -->
             </select></td>
  </tr>
  <tr>
    <td id="align"></td>
    <td style="float: left;margin-top: 25px;"> <select id="updatablecolumns" name = "updatableColumns"> 
             		<!-- <option> Incharge </option>
                    <option> Price </option>
                  	<option> Added </option> -->
             </select></td>
             
             <td style="padding-top: 25px;float: left;margin-left: 10px;"> <select id="conditions" name="conditions"> 
             		<option> = </option>
                    <!-- <option> Price </option>
                  	<option> Added </option> -->
             </select></td>
             <td style="padding-top: 25px;float: left;margin-left: 10px;"><select id="existingvalue" name="existingvalue"></select></td>
  </tr>
  
</table>
<div class="inpsbtn" align="center">
       		<input type="submit" class="updatebutton" value="Submit"/>
</div>     
</form> 
</body>
</html>
