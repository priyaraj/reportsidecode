<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | Update new fields</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>

<script type="text/javascript" src="js/adminfunctions.js"></script>

</head>
<script>
function initialize() {
	<%
	List collectionNamesList 	= new ArrayList();
	
	
	collectionNamesList = (ArrayList)session.getAttribute("collectionNames");
	if (!collectionNamesList.isEmpty()) {
		%>
		var collectionNamesListOptions = '<select id="itemcollectionnames" name="itemcollectionnames" size="3" multiple="multiple">';
		<%		
	     for(int i = 0; i < collectionNamesList.size();i++)
	     { 
	    	 if (!collectionNamesList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     collectionNamesListOptions += '<option><%= collectionNamesList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	collectionNamesListOptions += '</select>';
	document.getElementById('itemcollectionnames').outerHTML = collectionNamesListOptions;
	
	var evidenceIdentifierListOptions = '<select id="evidenceIdentifier" name="evidenceIdentifier" size="3" multiple="multiple">';
	<%
	List evidenceIdentifierList = new ArrayList();
	String contextType = (String)session.getAttribute("contextType"); 
	evidenceIdentifierList = (ArrayList)session.getAttribute("items_collection_evidence_identifier");
	if (evidenceIdentifierList != null && !evidenceIdentifierList.isEmpty()) {
		%>
		
		<%		
	     for(int i = 0; i < evidenceIdentifierList.size();i++)
	     { 
	    	 if (!evidenceIdentifierList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     evidenceIdentifierListOptions += '<option><%= evidenceIdentifierList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	
	if (contextType.equalsIgnoreCase("Rotating_assets")) { //if we have similar collection for different apptype, they should be added here - Sep 3
	evidenceIdentifierList = (ArrayList)session.getAttribute("items_equipments_collection_evidence_identifier");
	if (evidenceIdentifierList != null && !evidenceIdentifierList.isEmpty()) {
		%>
		
		<%		
	     for(int i = 0; i < evidenceIdentifierList.size();i++)
	     { 
	    	 if (!evidenceIdentifierList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     evidenceIdentifierListOptions += '<option><%= evidenceIdentifierList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	}
	%>
	evidenceIdentifierListOptions += '</select>';
	document.getElementById('evidenceIdentifier').outerHTML = evidenceIdentifierListOptions;
}

</script>
<body onload="initialize()">
<form name = "updatenew" action="updateNewFields" method="get" onsubmit="return(validateForm(this));">
	  <div id="logo">
      		<img src="images/instaunite_logo.png"/>
      </div>
       <h1>Update Screen (new fields)</h1>	   	
       <hr>
      <table style="width:100%;">
  <tr>
    <td id="align1"><h4> Add New fields</h4></td>
    <td><input type="text"   id = "newfield" name="addnewfield"/></td>
  </tr>
<tr>
    <td id="align"><h4> Choose collection</h4></td>
    <td><select id="itemcollectionnames" size="3" multiple="multiple" name="itemcollectionnames"> 
             		<!-- <option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option> -->
             </select></td>
  </tr>
  <tr>
  <% if (contextType.equalsIgnoreCase("Rotating_assets")) {  %>
    <td id="align"><h4> Choose Toolbox/Equipment</h4></td>
    <% } else { %>
    	<td id="align"><h4> Choose identifier</h4></td>
    <%}%>
    <td><select id="evidenceIdentifier" size="3" multiple="multiple" name="evidenceIdentifier"> 
             		<!-- <option value=" All toolboxes"> All toolboxes</option>
                    <option value=" Some toolboxes">Some toolboxes</option>
                  	<option value=" One toolboxes">One toolbox</option> -->
             </select></td>
  </tr>
  
</table>
<div class="inpsbtn" align="center">
       		<input type="submit" class="updatebutton" value="Submit"/>
</div>
</form>      
</body>
</html>