<%@page import="com.mongodb.DBObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | custom report</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
</head>


<body>

	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
<h1>Custom Report</h1>
<h2><%= (String)request.getAttribute("reportTitle") %></h2>



 
    
<form>   
 <div align="center">
          <hr>
         <br>
<h3><%= (String)request.getAttribute("reportCondtions") %></h3>	
  <% 
 List<DBObject> report1DetailsList=new ArrayList<DBObject>();

 report1DetailsList = (ArrayList)request.getAttribute("report1Details");	 
Double totalPrice = 0.0;
if (report1DetailsList == null ) {
	%>
	<table class="nomatch">
 <tr>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Invalid date format entered</FONT></td>
 </tr>
 </table>
 <%
}
else if(report1DetailsList != null && !report1DetailsList.isEmpty()) {
	%>
    	   	

 <table class="description" >
 <tr >
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No.</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Remote Unit</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Machine</FONT></td> 
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Supervisor</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Reasons</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Machine Downtime</FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">Actions Taken</FONT></td>
 </tr>

 <%
 	int srNo = 0;
     for(DBObject info: report1DetailsList)
     { 
    	 srNo = srNo + 1;    	
     %>
     <tr BGCOLOR="F1F1F1">
     <td ><FONT COLOR="#333333">
     <%= srNo  %>
     </FONT></td>
      <td ><FONT COLOR="#333333">
     <%= info.get("Asset_Zzz") %>
     </FONT></td>
     <td><FONT COLOR="#333333">
     <%= info.get("Asset_Xxx") %>
     </FONT></td>
     <td><FONT COLOR="#333333">
     <%= info.get("Asset_Yyy") %>
     </FONT></td>
      <td><FONT COLOR="#333333">
     <%= info.get("Evidence_2") %>
     </FONT></td>
     <td><FONT COLOR="#333333">
     <%= info.get("Evidence_3") %>
     </FONT></td>                    
     <td align="right"><FONT COLOR="#333333">
     <%= info.get("Evidence_4") %>
     </FONT></td>
     </tr>
        <% 
     }
     

 

 }
else { %>
	
	<table class="nomatch">
 <tr>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No matching records found</FONT></td>
 </tr>
 </table>
<% }
 %>
 </table>
 
  </div>

</form>
</body>
</html>
