<%@page import="com.mongodb.AggregationOutput"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instaunite | predefined report</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
</head>


<body>
<%!
    
    public String getTopMost(AggregationOutput aggregationOutput){
		int idx = 0;
		String topRecord = "";
		 for (DBObject result : aggregationOutput.results()) { 
			 idx += 1;
			 if  (idx == 1) {
				 topRecord = (String)result.get("_id");
				 break;
		 	}
		 }
        return topRecord;
    }
%>

	<c:if test="${requestScope.error ne null}">
		<strong style="color: red;"><c:out
				value="${requestScope.error}"></c:out></strong>
	</c:if>
	<c:if test="${requestScope.success ne null}">
		<strong style="color: green;"><c:out
				value="${requestScope.success}"></c:out></strong>
	</c:if>

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
<h1>Weekly Machine Report</h1>
<%-- <h2><%= (String)request.getAttribute("reportTitle") %></h2> --%>
    
<form>   
 <div align="center">
          <hr>
         <br>
<%-- <h6>>&nbsp;&nbsp;<%= (String)request.getAttribute("reportCondtions") %></h6>	 --%>
<table align="center" class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333">Total DownTime:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= session.getAttribute("totalDownTime")  %> minutes</h6>
<h6><FONT COLOR="#333333">Average DownTime:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= session.getAttribute("averageDownTime")  %> minutes</h6>
  <% 
  
  AggregationOutput aggregationOutput = null;
  String fieldName = "";
  String heading = "";
  String caption = "";
  int idx = 0;
  caption = "Most Sighted Reason";
  aggregationOutput = (AggregationOutput)session.getAttribute("reasonOutput");
  String topMost = "";
  if (aggregationOutput == null ) {
		%>
		<table class="nomatch">
	 <tr>
	 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No data is observed</FONT></td>
	 </tr>
	 </table>
	 <%
	}
	else {
		topMost = getTopMost(aggregationOutput);
		%>    	   	

	 <table align="center" class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333"><%= caption %>:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= topMost  %></h6>
	 <%	 
	 
	 }

  aggregationOutput = (AggregationOutput)session.getAttribute("machinedowntimeOutput");
  caption = "Most Occured Downtime";
  if (aggregationOutput == null ) {
		%>
		<table class="nomatch">
	 <tr>
	 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No data is observed</FONT></td>
	 </tr>
	 </table>
	 <%
	}
	else  {	
		topMost = getTopMost(aggregationOutput);
	%>    	   
	 <table class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333"> <%= caption %>:</FONT>&nbsp;&nbsp; <%= topMost  %></h6>
	 <%	 	 
	 }
  
  aggregationOutput = (AggregationOutput)session.getAttribute("actionOutput");
  caption = "Most Taken Action";
  if (aggregationOutput == null ) {
		%>
		<table class="nomatch">
	 <tr>
	 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No data is observed</FONT></td>
	 </tr>
	 </table>
	 <%
	}
	else  {	
		topMost = getTopMost(aggregationOutput);
	%>    	   	

	 <table class="description" >
	 <tr></tr> 
<h6><FONT COLOR="#333333"><%= caption %>:</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%= topMost  %></h6>
	 <%	 
	 
	 }
	for (int i = 0; i < 3; i++) {
		switch(i) {
		case 0 :
			heading = "Top 5 Reasons";
			fieldName = "Reasons";			
		  	aggregationOutput = (AggregationOutput)session.getAttribute("reasonOutput");
			break;
		case 1 :
			heading = "Top 5 Machine Downtime";
			fieldName = "Machine DownTime";			
		  	aggregationOutput = (AggregationOutput)session.getAttribute("machinedowntimeOutput");
			break;
		case 2 :
			heading = "Top 5 Action Taken";
			fieldName = "Actions";			
		  	aggregationOutput = (AggregationOutput)session.getAttribute("actionOutput");
			break;
		}	  
  	

 if(aggregationOutput != null) {
	 idx = 0;
	%>  	   	
 <table align="center" class="description" >
 <tr></tr>
<h7><%= heading %> Data</h7> 
  <tr >
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF"><%= fieldName %></FONT></td>
 <td BGCOLOR="#2692FF"><FONT COLOR="#FFFFFF">No Of Occurences</FONT></td>
 </tr>
 <% 
 for (DBObject result : aggregationOutput.results()) {      
    	idx += 1;
	if(idx>5){
	break;}
     %>
     <tr BGCOLOR="#F1F1F1">      
     <td><FONT COLOR="#333333"><%= result.get("_id") %></FONT></td><td><FONT COLOR="#FF9326"> <%= result.get("times")  %></FONT></td>
     </tr>
        <% 
     } 
 } 
	} //for loop
 
 %>
 </table>
 
  </div>

</form>
</body>
</html>
