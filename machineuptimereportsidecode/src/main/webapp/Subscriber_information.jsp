<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBObject"%>
<%@page import="com.mongodb.MongoClient"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Instaunite | Subscriber Information</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script type="text/javascript">

var emailCheck = true;
function checkEmail() {

    var email = document.getElementById("Email");
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
    alert("'Please provide a valid email address");
    //email.focus;
    emailCheck = false;
    return false;
 }
}
function validateForm()
{
var x=document.getElementById("Name").value;
var y=document.getElementById("Email").value;
var z=document.getElementById("PhoneNumber").value;
var a=document.getElementById("AddressLine1").value;
var b=document.getElementById("City").value;
var c=document.getElementById("PostalCode").value;
var d=document.getElementById("FirstName").value;
 if (y==null || y=="")
{
	alert("Email Should not be empty");
	return false;
}
else {
	emailCheck = checkEmail();
	//alert("emailCheck.." + emailCheck);
	if (emailCheck == false) {
		return false;
	}
}
 if (x==null || x=="")
{
	alert("Name Should not be empty");
	return false;
}
if (d==null || d=="")
{
	alert("First Name Should not be empty");
	return false;
}
else if (z==null || z=="")
{
	alert("Phone Number Should not be empty");
	return false;
}
else if (a==null || a=="")
{
	alert("Address Line 1 Should not be empty");
	return false;
}
else if (b==null || b=="")
{
	alert("City Should not be empty");
	return false;
}
else if (c==null || c=="")
{
	alert("Postal Code Should not be empty");
	return false;
}  
}
</script>

<body>
	

	<div id="logo">
      <img src="images/instaunite_logo.png"/>
      </div>
      <h1>Subcriber Self Order Form</h1>	   	
       <hr>
       <form action="subscriber" method="post">
      <table style="width:100%;padding-top: 10px;float:left">
       <tr>
       <td id="left"><h10><u> End Customer Information</u></h10></td>
       </tr>
       <tr>
    <td  id="align" ><h4> * Email</h4></td>
<!--     <td><input type="text"   name="Email" id="Email" style="float: left;" onblur="getEndCustomerDetails();"/></td> -->
<td><input type="text"   name="Email" id="Email" style="float: left;" /></td>
  </tr>
       <tr>
    <td id="align"><h4> * Name </h4>
    </td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="Name" id="Name"/></td>
  </tr>
  <tr>
  <tr>
    <td  id="align" ><h4> * First Name</h4></td>
    <td><input type="text"   name="FirstName" id="FirstName" style="float: left;"/></td>
  </tr>
  
  <tr>
    <td  id="align" ><h4> * Phone Number</h4></td>
    <td><input type="text"   name="PhoneNumber" id="PhoneNumber" style="float: left;"/></td>
  </tr>
  </table>
   <table style="width:100%;padding-top: 10px;float:left">
       <tr>
     <td id="left">  <h10><u> Address Information</u></h10></td>
       </tr>
       <tr>
    <td id="align" > <h4> * Delivery Address Line 1</h4></td>
    <td style="float: left;margin-top: 20px;"><input type="text"  name="AddressLine1" id="AddressLine1"/></td>
  </tr>
  <tr>
  <tr>
    <td  id="align" ><h4> Delivery Address Line 2</h4></td>
    <td><input type="text"   name="AddressLine2" id="AddressLine2" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" > <h4> * City</h4></td>
    <td><input type="text"   name="City" id="City" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" > <h4> * Postal Code</h4></td>
    <td><input type="text"   name="PostalCode" id="PostalCode" style="float: left;"/></td>
  </tr>
  <tr>
    <td  id="align" ><h4> Other remarks</h4></td>
    <td><input type="text"   name="Otherremark" id="Otherremark" style="float: left;"/></td>
  </tr>
  </table>   
   <table style="width:100%;padding-top: 30px;float:left">
  <tr>
            <td><input name="Signin" type="submit" value="Back" class="sgncls_1" /></a></td>
            <td> <input name="Signin" type="submit" value="Next" onclick="return(validateForm());" class="sgncls_2" /></a></td>
            <!-- <td><input name="Signin" type="submit" value="Next"  class="sgncls_2" /></td> -->
            </tr>
    </table>        	
    </form>
</body>
</html>
