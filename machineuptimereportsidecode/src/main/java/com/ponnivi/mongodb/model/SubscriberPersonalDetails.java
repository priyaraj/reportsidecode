package com.ponnivi.mongodb.model;

public class SubscriberPersonalDetails {

	// id will be used for primary key in MongoDB
	// We could use ObjectId, but I am keeping it
	// independent of MongoDB API classes
	private String endCustomerId;
	
	private String ownerCustomerId;

	public String getOwnerCustomerId() {
		return ownerCustomerId;
	}

	public void setOwnerCustomerId(String ownerCustomerId) {
		this.ownerCustomerId = ownerCustomerId;
	}

	private String endCustomerName;
	
	private String endCustomerFirstName;

	private String endCustomerEmail;

	private String endCustomerPhoneNumber;

	private String endCustomerAddressLine1;
	
	private String endCustomerAddressLine2;

	private String endCustomerCity;
	
	private String endCustomerPostalCode;

	private String endCustomerOtherRemarks;

	private String endCustomerAdditionalEmail;

	private String endCustomerAttribute1;
	
	private String endCustomerAttribute2;
	
	private String endCustomerAttribute3;
	
	private String endCustomerAttribute4;
	
	private String endCustomerAttribute5;
	
	private String endCustomerAttribute6;
	
	private String endCustomerAttribute7;
	


	public String getEndCustomerId() {
		return endCustomerId;
	}

	public void setEndCustomerId(String endCustomerId) {
		this.endCustomerId = endCustomerId;
	}

	public String getEndCustomerFirstName() {
		return endCustomerFirstName;
	}

	public void setEndCustomerFirstName(String endCustomerFirstName) {
		this.endCustomerFirstName = endCustomerFirstName;
	}

	public String getEndCustomerEmail() {
		return endCustomerEmail;
	}

	public void setEndCustomerEmail(String endCustomerEmail) {
		this.endCustomerEmail = endCustomerEmail;
	}

	public String getEndCustomerPhoneNumber() {
		return endCustomerPhoneNumber;
	}

	public void setEndCustomerPhoneNumber(String endCustomerPhoneNumber) {
		this.endCustomerPhoneNumber = endCustomerPhoneNumber;
	}

	public String getEndCustomerAddressLine1() {
		return endCustomerAddressLine1;
	}

	public void setEndCustomerAddressLine1(String endCustomerAddressLine1) {
		this.endCustomerAddressLine1 = endCustomerAddressLine1;
	}

	public String getEndCustomerAddressLine2() {
		return endCustomerAddressLine2;
	}

	public void setEndCustomerAddressLine2(String endCustomerAddressLine2) {
		this.endCustomerAddressLine2 = endCustomerAddressLine2;
	}

	public String getEndCustomerCity() {
		return endCustomerCity;
	}

	public void setEndCustomerCity(String endCustomerCity) {
		this.endCustomerCity = endCustomerCity;
	}

	public String getEndCustomerPostalCode() {
		return endCustomerPostalCode;
	}

	public void setEndCustomerPostalCode(String endCustomerPostalCode) {
		this.endCustomerPostalCode = endCustomerPostalCode;
	}

	public String getEndCustomerOtherRemarks() {
		return endCustomerOtherRemarks;
	}

	public void setEndCustomerOtherRemarks(String endCustomerOtherRemarks) {
		this.endCustomerOtherRemarks = endCustomerOtherRemarks;
	}

	public String getEndCustomerAdditionalEmail() {
		return endCustomerAdditionalEmail;
	}

	public void setEndCustomerAdditionalEmail(String endCustomerAdditionalEmail) {
		this.endCustomerAdditionalEmail = endCustomerAdditionalEmail;
	}

	public String getEndCustomerAttribute1() {
		return endCustomerAttribute1;
	}

	public void setEndCustomerAttribute1(String endCustomerAttribute1) {
		this.endCustomerAttribute1 = endCustomerAttribute1;
	}

	public String getEndCustomerAttribute2() {
		return endCustomerAttribute2;
	}

	public void setEndCustomerAttribute2(String endCustomerAttribute2) {
		this.endCustomerAttribute2 = endCustomerAttribute2;
	}

	public String getEndCustomerAttribute3() {
		return endCustomerAttribute3;
	}

	public void setEndCustomerAttribute3(String endCustomerAttribute3) {
		this.endCustomerAttribute3 = endCustomerAttribute3;
	}

	public String getEndCustomerAttribute4() {
		return endCustomerAttribute4;
	}

	public void setEndCustomerAttribute4(String endCustomerAttribute4) {
		this.endCustomerAttribute4 = endCustomerAttribute4;
	}

	public String getEndCustomerAttribute5() {
		return endCustomerAttribute5;
	}

	public void setEndCustomerAttribute5(String endCustomerAttribute5) {
		this.endCustomerAttribute5 = endCustomerAttribute5;
	}

	public String getEndCustomerAttribute6() {
		return endCustomerAttribute6;
	}

	public void setEndCustomerAttribute6(String endCustomerAttribute6) {
		this.endCustomerAttribute6 = endCustomerAttribute6;
	}

	public String getEndCustomerAttribute7() {
		return endCustomerAttribute7;
	}

	public void setEndCustomerAttribute7(String endCustomerAttribute7) {
		this.endCustomerAttribute7 = endCustomerAttribute7;
	}

	public String getEndCustomerName() {
		return endCustomerName;
	}

	public void setEndCustomerName(String endCustomerName) {
		this.endCustomerName = endCustomerName;
	}


	public String getId() {
		return endCustomerId;
	}

	public void setId(String id) {
		this.endCustomerId = id;
	}
}
