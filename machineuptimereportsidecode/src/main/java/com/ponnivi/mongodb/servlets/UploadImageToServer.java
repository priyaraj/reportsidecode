package com.ponnivi.mongodb.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.helper.Utilities;

@WebServlet("/UploadImageToServer")
public class UploadImageToServer extends HttpServlet {

	private static final long serialVersionUID = -7060758261496829905L;
	static String imageFileName="", osName="";
	OutputStream out;
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
			System.out.println(".... in doPost...");
			MongoClient mongo = (MongoClient) request.getServletContext()
					.getAttribute("MONGO_CLIENT");
			String databaseName = (String)request.getServletContext()
					.getAttribute("PONNIVI_DATABASE");
			
	        InputStream in = request.getInputStream();
	        osName = Utilities.getUserOperatingSystem();    
	        if (osName.startsWith("Windows")) {
	        	//imageFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\image.jpg";
	        	imageFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\image.jpg";
	        }
	        else {
	        	imageFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/images/image.jpg";
	        }
	       	out = new FileOutputStream(imageFileName);
	       
	        Utilities.copy(in, out); //The function is below
	        out.flush();
	        out.close();
	        imageFileName = Utilities.removeAndCreateAptData(imageFileName,"image");
	    


	}
	
	
	}


