package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.model.OrderDetails;

@WebServlet("/finalorderinformation")
public class FinalOrderInformationServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String submitValue = request.getParameter("submit");
		System.out.println("submitValue.."+submitValue);
		HttpSession session = request.getSession();
		long orderCollectionCount = 0;
		String redirectingJsp = "";
		String endCustomerId = (String)session.getAttribute("endCustomerId");
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		String orderId = (String)session.getAttribute("orderId");
		if (submitValue.equalsIgnoreCase("Submit")) {
			//Creating orders collection when initiating for order_detail
			DBCollection ordersCollection = customerDatabase.getCollection("orders");
			BasicDBObject ordersDocument = new BasicDBObject();
			ordersDocument.put("orderid",orderId);
			ordersDocument.put("endcustomerid",endCustomerId);
			//ordersDocument.put("advancepaymentmade",advancepayment); Need to get that from checked value. if checked means yes
			ordersDocument.put("advancepaymentamount",request.getParameter("advancepaymentamount"));
			//Get subscriber information details as address details  -- Need to do
			ordersCollection.insert(ordersDocument);
			redirectingJsp = "/thankyou.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				  redirectingJsp); 
				  rd.forward(request, response);
		
	}
}
