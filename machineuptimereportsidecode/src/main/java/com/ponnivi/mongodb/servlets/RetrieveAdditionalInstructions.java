package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBServiceHistoryDAO;
import com.ponnivi.mongodb.model.ServiceHistory;

@WebServlet("/retrieveAdditionalInstructions")
public class RetrieveAdditionalInstructions extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		String databaseName = (String)request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		MongoDBServiceHistoryDAO serviceHistoryDAO = new MongoDBServiceHistoryDAO(mongo, databaseName);
		ServiceHistory serviceHistory = new ServiceHistory();

		List<ServiceHistory> serviceHistoryList = serviceHistoryDAO.readAllServiceHistory();

		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();

		Gson gson = new Gson();
		
        String additionalInstructions = gson.toJson(serviceHistoryList);
        
        out.println(additionalInstructions);
	}
}
