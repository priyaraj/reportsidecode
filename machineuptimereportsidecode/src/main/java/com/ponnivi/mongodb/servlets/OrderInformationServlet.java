package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.model.OrderDetails;

@WebServlet("/orderinformation")
public class OrderInformationServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String submitValue = request.getParameter("submit");
		System.out.println("submitValue.."+submitValue);
		HttpSession session = request.getSession();
		long orderCollectionCount = 0;
		String redirectingJsp = "";
		String customerId = (String)session.getAttribute("customerId");
		DB customerDatabase = (DB)session.getAttribute("customerDb");
		orderCollectionCount = MongoDBDAO.getCollectionCount("orders", customerDatabase);
		String orderEndValue="",orderId="";
		DBCollection collection;
		String endCustomerId = (String)session.getAttribute("endCustomerId");
		String newOrder = (String)session.getAttribute("newOrder");
		String lastOrderIdOfEndCustomer = "";
		if (newOrder.equalsIgnoreCase("yes")) {  //When we come from subsriber page, new order is created
			lastOrderIdOfEndCustomer = MongoDBDAO.getLastPrimaryKeyValue(customerDatabase, customerId + "_" + endCustomerId, "orders", "orderid");
			/*BasicDBObject searchObject = new BasicDBObject();
			searchObject.put("orderid", new BasicDBObject(
					"$regex", customerId + "_" + endCustomerId).append("$options", "i"));
			collection = customerDatabase.getCollection("orders");
			DBCursor cursor = collection.find(searchObject);
			
			while (cursor.hasNext()) {
				DBObject o = cursor.next();
				lastOrderIdOfEndCustomer = o.get("orderid").toString();				
			}
			System.out.println("lastOrderIdOfEndCustomer.."+lastOrderIdOfEndCustomer);	*/	
			if (!lastOrderIdOfEndCustomer.equalsIgnoreCase("") ) {
				orderEndValue = lastOrderIdOfEndCustomer.substring(lastOrderIdOfEndCustomer.length()-6, lastOrderIdOfEndCustomer.length());
				System.out.println("orderEndValue.."+orderEndValue);
				if (Integer.parseInt(orderEndValue) < 9) {
					orderId = customerId + "_" + endCustomerId + "_order" + "_00000" + (Integer.parseInt(orderEndValue) + 1); 
				}
				if (Integer.parseInt(orderEndValue) >= 9 && Integer.parseInt(orderEndValue) < 100) {
					orderId = customerId + "_" + endCustomerId + "_order" + "_0000" + (Integer.parseInt(orderEndValue) + 1);
				}
				if (Integer.parseInt(orderEndValue) >= 100 && Integer.parseInt(orderEndValue) < 1000) {
					orderId = customerId + "_" + endCustomerId + "_order" + "_000" + (Integer.parseInt(orderEndValue) + 1);
				}
			}
			else {  //if it is the first order for that subscriber for that customer
				orderId = customerId + "_" + endCustomerId + "_order" + "_000001";
			}
			
			
		/*if (orderCollectionCount == 0) {
			orderId = customerId + "_" + endCustomerId + "_order" + "_000001";
		}
		else {
			collection = customerDatabase.getCollection("orders");
			orderEndValue = MongoDBDAO.getMaxValue("orderid",collection);
			if (Integer.parseInt(orderEndValue) < 9) {
				orderId = customerId + "_" + endCustomerId + "_order" + "_00000" + (Integer.parseInt(orderEndValue) + 1); 
			}
			if (Integer.parseInt(orderEndValue) >= 9 && Integer.parseInt(orderEndValue) < 100) {
				orderId = customerId + "_" + endCustomerId + "_order" + "_0000" + (Integer.parseInt(orderEndValue) + 1);
			}
			if (Integer.parseInt(orderEndValue) >= 100 && Integer.parseInt(orderEndValue) < 1000) {
				orderId = customerId + "_" + endCustomerId + "_order" + "_000" + (Integer.parseInt(orderEndValue) + 1);
			}
		}*/
			System.out.println("orderId.."+orderId);
		session.setAttribute("orderId", orderId);
/*		//Creating orders collection when initiating for order_detail
		DBCollection ordersCollection = customerDatabase.getCollection("orders");
		BasicDBObject ordersDocument = new BasicDBObject();
		ordersDocument.put("orderid",orderId);
		ordersCollection.insert(ordersDocument);*/
		}
		//store order_details, -- Sep 21
		session.setAttribute("newOrder", "no");
		String equipmentName = (String)request.getParameter("equipmentname");
		String usage = (String)request.getParameter("usage");
		List assetsList = (ArrayList)session.getAttribute("wholeassets");
		int assetId = 0;
		String assetIdStr = "";
		try {
		for (int i = 0; i < assetsList.size();i++) {
			JSONObject jsonObject = (JSONObject)assetsList.get(i);				
				if (jsonObject.getString("description").equalsIgnoreCase(equipmentName)) {
					assetId = (int)Double.parseDouble(jsonObject.getString("asset_id").toString());
					assetIdStr = jsonObject.getString("asset_id").toString();
					break;
				}				
		}
		DB remoteRentalDatabase = (DB)session.getAttribute("remoterentaldb");
		DBCollection pricing = remoteRentalDatabase.getCollection("Pricing");
		String price = MongoDBDAO.getDetails(pricing, "Asset_Id", "number", assetIdStr, "Usage_Description", "text", usage, "number", "Price");
		String quantity = (String)request.getParameter("Quantity");
		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		String fromdateParamValue = (String)request.getParameter("startingdDate");
		fromdateParamValue = MongoDBDAO.getActualDate(fromdateParamValue, dateFormat, 1);
		String todateParamValue = (String)request.getParameter("endingdate");
		
		todateParamValue = MongoDBDAO.getActualDate(todateParamValue, dateFormat, 1);


		long noOfDays = -1;
		Date startDate = new Date(),endDate = new Date();
		try {
			startDate = dateFormat.parse(fromdateParamValue);
		
		 endDate = dateFormat.parse(todateParamValue);

		//time is always 00:00:00 so rounding should help to ignore the missing hour when going from winter to summer time as well as the extra hour in the other direction
	    noOfDays = Math.round((endDate.getTime() - startDate.getTime()) / (double) 86400000) + 1;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Double totalPrice = Double.parseDouble(price) * Integer.parseInt(quantity)*noOfDays;
		System.out.println("totalPrice for this order detail.."+totalPrice);
		//Creating order_detail collection

		OrderDetails orderDetails = new OrderDetails();
		orderDetails.setAssetName(equipmentName);
		orderDetails.setUsageDescription(usage);
		orderDetails.setQuantity(Integer.parseInt(quantity));
		orderDetails.setAmount(totalPrice);
		orderDetails.setFromDate(startDate);
		orderDetails.setEndDate(endDate);
		orderDetails.setNoOfDaysUsed((int)noOfDays);
		orderId = MongoDBDAO.saveOrderDetails(customerDatabase, customerId,orderDetails,session);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (submitValue.equalsIgnoreCase("Add other equipments")) {
			session.setAttribute("success", "Entered information is stored. Please enter information or else click on Next to finish" );
			redirectingJsp = "/order_information.jsp";					
		}
		if (submitValue.equalsIgnoreCase("Next")) {
			//store order -- Sep 21
			redirectingJsp = "/confirm_and_pay.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				  redirectingJsp); 
				  rd.forward(request, response);
		
	}
}
