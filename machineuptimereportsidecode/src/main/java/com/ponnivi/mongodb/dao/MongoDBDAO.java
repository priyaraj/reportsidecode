package com.ponnivi.mongodb.dao;

import java.io.FileReader;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.ponnivi.mongodb.helper.Utilities;
import com.ponnivi.mongodb.model.OrderDetails;
import com.ponnivi.mongodb.model.SubscriberPersonalDetails;

//DAO class for different MongoDB CRUD operations
//take special note of "id" String to ObjectId conversion and vice versa
//also take note of "_id" key for primary key
public class MongoDBDAO {

	private DBCollection col;
	static DB ponniviDb = null;
	static String contextReferenceExample;
	static String evidence_2;
	static String evidence_3;
	static String evidence_4;

	static Map<String, Object> map = new HashMap<String, Object>();
	static String reportId = "1";
	

	// Here we have inserted meta and data jsons
	public MongoDBDAO(MongoClient mongo, String dbName,
			String collectionName, String filePath) {
		// System.out.println("filePath.." + filePath);
		ponniviDb = mongo.getDB(dbName);

		DBCollection Collection = ponniviDb.getCollection(collectionName);

		// Get collectionName details and put

		// convert JSON to DBObject directly

		DBObject jsonDbObject = (DBObject) JSON.parse(Utilities
				.getJsonDetails(filePath));

		Collection.insert(jsonDbObject);

	}


	

	
	// Get distinct fields
		public static List getDistinct(DB ponniviDb, String collectionName,
				String distinctFieldName)
				throws UnknownHostException {

			//ponniviDb = mongo.getDB(dbName);

			DBCollection Collection = ponniviDb.getCollection(collectionName);

			// call distinct method and store the result in list l1
			List distinctList = Collection.distinct(distinctFieldName);

			// iterate through the list and print the elements
			for (int i = 0; i < distinctList.size(); i++) {
				System.out.println(distinctList.get(i));
			}

			return distinctList;

		}
		
		//Get distinct list based upon specific id 
		//Eg : specific id - customerId
		public static List getDistinct(DB ponniviDb, String collectionName,
				String distinctFieldName,String checkFieldName, String checkingValue)
				throws UnknownHostException {

			//ponniviDb = mongo.getDB(dbName);
			System.out.println("collectionName.."+collectionName);
			DBCollection Collection = ponniviDb.getCollection(collectionName);

			// call distinct method and store the result in list l1
			BasicDBObject searchObject = new BasicDBObject();
			searchObject
					.put(checkFieldName, checkingValue);
			List distinctList = Collection.distinct(distinctFieldName,searchObject);

			// iterate through the list and print the elements
			for (int i = 0; i < distinctList.size(); i++) {
				System.out.println(distinctList.get(i));
			}

			return distinctList;

		}


	
	public static String getCollectionSpecificDetails(DB ponniviDb, String collectionName,String fieldName,String fieldValue) {
		String result = "";
	//	ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);
			BasicDBObject searchObject = new BasicDBObject();
			searchObject.put(fieldName,
					fieldValue);
			DBCursor cursor = collection.find(searchObject);
			while (cursor.hasNext()) {
				// System.out.println(cursor.next());
				DBObject dbObject = cursor.next();
				if (collectionName.equalsIgnoreCase("customer_definition_collection")) {
					result = (String)dbObject.get("CustomerDbName");
				}
			}			
		}
		return result;
	}
	
	public static long getCollectionCount(String collectionName,DB ponniviDb) {
		//ponniviDb = mongo.getDB(dbName);
		long count = 0;
		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);

			count = collection.getCount();

		}
		return count;

	}
	
	public static String getMaxValue(String fieldName,DBCollection collection) {
		String maxValue = "";
		AggregationOutput output = null;
		DBObject groupFields = new BasicDBObject("_id",fieldName);
	    groupFields.put("times", new BasicDBObject("$sum",1));
	    DBObject group = new BasicDBObject("$group", groupFields);
	    DBObject sort = new BasicDBObject("$sort", new BasicDBObject("times",-1) );
	    DBObject limit = new BasicDBObject("$limit", 1 );		    
	    output = collection.aggregate(group, sort);

	    for (DBObject result : output.results()) {
			  System.out.println(result.get("_id") + ".." + result.get("times"));
			  maxValue = result.get("times").toString();
			  }
	    return maxValue;
	}
	public static String getMaxValueOfPrimaryKeys(String fieldName,DBCollection collection) {
		String maxValueOfPrimaryKey = "";
		DBCursor cursor = collection.find();
		while (cursor.hasNext()) {
			DBObject o = cursor.next();
			maxValueOfPrimaryKey = o.get(fieldName).toString();
		}
	    return maxValueOfPrimaryKey;
	}

	
	public static void saveSubscriberDetails(SubscriberPersonalDetails subscriberDetails,DB customerDatabase, HttpSession session) {
		BasicDBObject document = new BasicDBObject();
		//check whether any record is there first, if so, append the last value + 1 or else format will be subscriber_000001
		long collectionCount = getCollectionCount("subscriber", customerDatabase);
		String endCustomerIdEndValue = "",endCustomerId = "";
		DBCollection collection = customerDatabase.getCollection("subscriber");
		boolean existsAlready = false;
		if (collectionCount == 0) {
			endCustomerId = "subscriber" + "_000001";
		}
		else {
			//Before inserting into subscriber collection, check whether it is already inserted. emailid per ownercustomerid is unique and hence if same emailid is entered, we should not save in subscriber again
			endCustomerId = MongoDBDAO.getDetails(collection, "endcustomeremail", "text", subscriberDetails.getEndCustomerEmail(), "ownercustomerid", "text", subscriberDetails.getOwnerCustomerId(), "text", "endcustomerid");
			
			System.out.println("endCustomerId for given email" + endCustomerId);
			if (endCustomerId.equalsIgnoreCase("")) {  //since endCustomerId is not created for given emailid and customerid, going to create next subscriber id
			endCustomerIdEndValue = getMaxValue("endcustomerid",collection);
			
			if (Integer.parseInt(endCustomerIdEndValue) < 9) {
				endCustomerId = "subscriber" + "_00000" + (Integer.parseInt(endCustomerIdEndValue) + 1); 
			}
			if (Integer.parseInt(endCustomerIdEndValue) >= 9 && Integer.parseInt(endCustomerIdEndValue) < 100) {
				endCustomerId = "subscriber" + "_0000" + (Integer.parseInt(endCustomerIdEndValue) + 1);
			}
			if (Integer.parseInt(endCustomerIdEndValue) >= 100 && Integer.parseInt(endCustomerIdEndValue) < 1000) {
				endCustomerId = "subscriber" + "_000" + (Integer.parseInt(endCustomerIdEndValue) + 1);
			}
		}
			else {
				existsAlready = true;
			}
		}
		System.out.println("endCustomerId.." + endCustomerId);
		session.setAttribute("endCustomerId",endCustomerId);
		if (!existsAlready) {
		document.put("ownercustomerid", subscriberDetails.getOwnerCustomerId());
		document.put("endcustomerid", endCustomerId);
		document.put("endcustomername", subscriberDetails.getEndCustomerName());
		document.put("endcustomerfirstname", subscriberDetails.getEndCustomerFirstName());
		document.put("endcustomeremail", subscriberDetails.getEndCustomerEmail());
		document.put("endcustomerphonenumber", subscriberDetails.getEndCustomerPhoneNumber());
		
		document.put("endcustomeraddress1", subscriberDetails.getEndCustomerAddressLine1());
		document.put("endcustomeraddress2", subscriberDetails.getEndCustomerAddressLine2());
		document.put("endcustomercity", subscriberDetails.getEndCustomerCity());
		document.put("endcustomerpostalcode", subscriberDetails.getEndCustomerPostalCode());
		
		document.put("endcustomerotherremarks", subscriberDetails.getEndCustomerOtherRemarks());
		collection.insert(document);
		}
		
	}
	
	public static String getLastPrimaryKeyValue(DB customerDatabase,String searchValue,String collectionName,String retrievalField) {
		BasicDBObject searchObject = new BasicDBObject();
		searchObject.put("orderid", new BasicDBObject(
				"$regex", searchValue).append("$options", "i"));
		DBCollection collection = customerDatabase.getCollection(collectionName);
		DBCursor cursor = collection.find(searchObject);
		String lastPrimaryKeyValue = "";
		while (cursor.hasNext()) {
			DBObject o = cursor.next();
			lastPrimaryKeyValue = o.get(retrievalField).toString();				
		}
		System.out.println("lastPrimaryKeyValue.."+lastPrimaryKeyValue);	
		return lastPrimaryKeyValue;
	}
	
	public static String saveOrderDetails(DB customerDatabase,String customerId,OrderDetails orderDetails,HttpSession session) {
		BasicDBObject document = new BasicDBObject();
		//check whether any record is there first, if so, append the last value + 1 or else format will be subscriber_000001
		DBCollection collection = customerDatabase.getCollection("order_detail");
		long orderCollectionCount = 0;
		String orderDetailEndValue = "",orderDetailId = "",orderId="",orderEndValue="";
		String lastOrderDetailIdOfEndCustomer = "";
		orderId = (String)session.getAttribute("orderId");
		lastOrderDetailIdOfEndCustomer = MongoDBDAO.getLastPrimaryKeyValue(customerDatabase, orderId, "order_detail", "orderdetailid");
		if (!lastOrderDetailIdOfEndCustomer.equalsIgnoreCase("") ) {
			orderDetailEndValue = lastOrderDetailIdOfEndCustomer.substring(lastOrderDetailIdOfEndCustomer.length()-6, lastOrderDetailIdOfEndCustomer.length());
			System.out.println("orderDetailEndValue.."+orderDetailEndValue);
			if (Integer.parseInt(orderDetailEndValue) < 9) {
				orderDetailId = orderId + "_detail" + "_00000" + (Integer.parseInt(orderDetailEndValue) + 1); 
			}
			if (Integer.parseInt(orderDetailEndValue) >= 9 && Integer.parseInt(orderDetailEndValue) < 100) {
				orderDetailId = orderId + "_detail" + "_0000" + (Integer.parseInt(orderDetailEndValue) + 1);
			}
			if (Integer.parseInt(orderDetailEndValue) >= 100 && Integer.parseInt(orderDetailEndValue) < 1000) {
				orderDetailId = orderId + "_detail" + "_000" + (Integer.parseInt(orderDetailEndValue) + 1);
			}
		}
		else {  //if it is the first order for that subscriber for that customer
			orderDetailId = orderId + "_detail" + "_000001";
		}
		
		System.out.println("orderDetailId.." + orderDetailId);
		document.put("orderdetailid", orderDetailId);
		document.put("orderid", orderId);
		document.put("assetname", orderDetails.getAssetName());
		document.put("quantity", orderDetails.getQuantity());
		document.put("usagedescription", orderDetails.getUsageDescription());
		
		document.put("fromdate", orderDetails.getFromDate());
		document.put("enddate", orderDetails.getEndDate());
		document.put("noofdays",orderDetails.getNoOfDaysUsed());
		document.put("amount", orderDetails.getAmount());		

		collection.insert(document);
		
		return orderId;
	}
	
	public static List getEquipmentDetails(DBCollection collection,int categoryId,String fieldName1,String fieldName2, String fieldName3) {
		List equipmentTypeList = null;
		DBCursor cursor = null;
		BasicDBObject searchObject = new BasicDBObject();
		if (categoryId == 0) {
		cursor = collection.find();
		}
		else {
			searchObject.put("Asset_Category_Id",
					categoryId);
			cursor = collection.find(searchObject);
		}

		if (cursor.count() > 0) {
			equipmentTypeList = new ArrayList();
		while (cursor.hasNext()) {
			DBObject o = cursor.next();
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("id",o.get(fieldName1));
				jsonObject.put("description",o.get(fieldName2));
				if (!fieldName3.equalsIgnoreCase("")) {
					jsonObject.put("asset_id",o.get(fieldName3));
				}
			equipmentTypeList.add(jsonObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}

		return equipmentTypeList;
	}
	
	public static List<DBObject> getDetails(DBCollection collection,String fieldName1,String field1DataType, String fieldValue1) {
		List<DBObject> outputList = new ArrayList<DBObject>(); 
		DBCursor cursor = null;
		BasicDBObject searchObject1 = new BasicDBObject();
		
		BasicDBList resultList = new BasicDBList();
		if (field1DataType.equalsIgnoreCase("number")) {
			searchObject1.put(fieldName1,
					(int)Double.parseDouble(fieldValue1));
		}
		else {
			searchObject1.put(fieldName1,
					fieldValue1);
		}
		
			//resultList.add(searchObject1);
		
			
			cursor = collection.find(searchObject1);
		

		if (cursor.count() > 0) {
		while (cursor.hasNext()) {
			DBObject o = cursor.next();
			outputList.add(o);
		}
		}

		return outputList;
	}
		
	public static String getDetails(DBCollection collection,String fieldName1,String field1DataType, String fieldValue1, String fieldName2, String field2DataType, String fieldValue2, String outputFieldDataType, String outputField) {
		String output = "";
		DBCursor cursor = null;
		BasicDBObject searchObject1 = new BasicDBObject();
		BasicDBObject searchObject2 = new BasicDBObject();
		BasicDBList resultList = new BasicDBList();
		if (field1DataType.equalsIgnoreCase("number")) {
			searchObject1.put(fieldName1,
					(int)Double.parseDouble(fieldValue1));
		}
		else {
			searchObject1.put(fieldName1,
					fieldValue1);
		}
		if (field2DataType.equalsIgnoreCase("number")) {
			searchObject2.put(fieldName2,
					(int)Double.parseDouble(fieldValue2));
		}
		else {
			searchObject2.put(fieldName2,
					fieldValue2);
		}

			resultList.add(searchObject1);
			resultList.add(searchObject2);
			DBObject query = new BasicDBObject("$and", resultList);
			cursor = collection.find(query);
		

		if (cursor.count() > 0) {
		while (cursor.hasNext()) {
			DBObject o = cursor.next();
			if (outputFieldDataType.equalsIgnoreCase("number")) {
				output = String.valueOf(Double.parseDouble(o.get(outputField).toString()));
			}
			else {
			output = o.get(outputField).toString();
			}
		}
		}

		return output;
	}
	
	public static long daysBetween(Calendar startDate, Calendar endDate) {  
		  Calendar date = (Calendar) startDate.clone();  
		  long daysBetween = 0;  
		  while (date.before(endDate)) {  
		    date.add(Calendar.DAY_OF_MONTH, 1);  
		    daysBetween++;  
		  }  
		  return daysBetween;  
		}  
	
	public static String getActualDate(String todateParamValue,SimpleDateFormat dateFormat, int noOfDays) {
		String toDate = "";
		Calendar cal = Calendar.getInstance();    
		try {
			cal.setTime( dateFormat.parse(todateParamValue));
			cal.add( Calendar.DATE, noOfDays );    
			toDate=dateFormat.format(cal.getTime());    
			System.out.println("Actualdate.."+toDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toDate;
	}

	public static String getLinkNames(String paramName, String contextType) {
		String linkNameToMatch = "";
		if (contextType.equalsIgnoreCase("Rotating_assets")) {
		if (paramName.equalsIgnoreCase("price")) {
			linkNameToMatch = "C";
		}
		if (paramName.equalsIgnoreCase("Incharge")) {
			linkNameToMatch = "D";
		}
		if (paramName.equalsIgnoreCase("Category")) {
			linkNameToMatch = "Qualifier_8";
		}
		if (paramName.equalsIgnoreCase("To_Service_Item")) {
			linkNameToMatch = "Qualifier_1";
		}
		if (paramName.equalsIgnoreCase("Missing_Item")) {
			linkNameToMatch = "Qualifier_2";
		}
		if (paramName.equalsIgnoreCase("Added_Item")) {
			linkNameToMatch = "Evidence_5";
		}
		if (paramName.equalsIgnoreCase("Deleted_Item")) {
			linkNameToMatch = "Evidence_6";
		}
		}
		if (contextType.equalsIgnoreCase("machine_uptime")) {
			if (paramName.equalsIgnoreCase("Reasons")) {
				linkNameToMatch = "Evidence_2";
			}
			if (paramName.equalsIgnoreCase("Machine DownTime")) {
				linkNameToMatch = "Evidence_3";
			}
			if (paramName.equalsIgnoreCase("Action")) {				
				linkNameToMatch = "Evidence_4";
			}
			if (paramName.equalsIgnoreCase("Machine Name")) {
				linkNameToMatch = "Asset_Xxx";
			}
			if (paramName.equalsIgnoreCase("Supervisor Name")) {
				linkNameToMatch = "Asset_Yyy";
			}
			}
		// end for change
		return linkNameToMatch;

	}
}
