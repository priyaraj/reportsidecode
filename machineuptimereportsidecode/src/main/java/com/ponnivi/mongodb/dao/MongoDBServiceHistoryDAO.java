package com.ponnivi.mongodb.dao;

import java.util.ArrayList;
import java.util.List;



import org.bson.types.ObjectId;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.converter.ServiceHistoryConverter;
import com.ponnivi.mongodb.model.ServiceHistory;

//DAO class for different MongoDB CRUD operations
//take special note of "id" String to ObjectId conversion and vice versa
//also take note of "_id" key for primary key
public class MongoDBServiceHistoryDAO {

	private DBCollection col;
	
	

	public MongoDBServiceHistoryDAO(MongoClient mongo, String dbName) {
		this.col = mongo.getDB(dbName).getCollection("ServiceHistory");
		
	}

	public ServiceHistory createServiceHistory(ServiceHistory serviceHistory) {
		DBObject doc = ServiceHistoryConverter.toDBObject(serviceHistory);
		this.col.insert(doc);
		ObjectId id = (ObjectId) doc.get("_id");
		serviceHistory.setId(id.toString());
		return serviceHistory;
	}

	public void updateServiceHistory(ServiceHistory serviceHistory) {
		DBObject query = BasicDBObjectBuilder.start()
				.append("_id", new ObjectId(serviceHistory.getId())).get();
		this.col.update(query, ServiceHistoryConverter.toDBObject(serviceHistory));
	}

	public List<ServiceHistory> readAllServiceHistory() {
		List<ServiceHistory> data = new ArrayList<ServiceHistory>();
		DBCursor cursor = col.find();
		
		while (cursor.hasNext()) {
			DBObject doc = cursor.next();
			ServiceHistory serviceHistory = ServiceHistoryConverter.toServiceHistory(doc);
			
			data.add(serviceHistory);
		}
		return data;
	}

	public void deleteServiceHistory(ServiceHistory serviceHistory) {
		DBObject query = BasicDBObjectBuilder.start()
				.append("_id", new ObjectId(serviceHistory.getId())).get();
		this.col.remove(query);
	}

	public ServiceHistory readServiceHistory(ServiceHistory serviceHistory) {
		DBObject query = BasicDBObjectBuilder.start()
				.append("_id", new ObjectId(serviceHistory.getId())).get();
		DBObject data = this.col.findOne(query);
		return ServiceHistoryConverter.toServiceHistory(data);
	}

}
