/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
 * Please see the License.txt file for more information.*
 * All Rights Reserved.

 * 
 * <summary> MongoDBContentReportCode -- Service calls required for report preparation like db invocation, get collection, iterate collection etc.,

 * */
package com.Ponnivi;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class MongoDBContentReportCode {

	static MongoClient mongoClient = null;
	static DB ponniviDb = null;
	static String contextReferenceExample;
	static String evidence_2;
	static String evidence_3;
	static String evidence_4;
	// static String logoFileName;
	static String hostName;
	static String dbName;

	// static String metaJsonFileName,dataJsonFileName;

	// Get getMetaJsonFileName : eg : type - meta, appName - Rotating_assets
	/*
	 * public static String getMetaJsonFileName(String type,String appName) {
	 * metaJsonFileName = FileHelper.getFileName(type, appName); return
	 * metaJsonFileName; }
	 * 
	 * //Get getDataJsonFileName : eg : type - data, appName - Rotating_assets
	 * public static String getDataJsonFileName(String type,String appName) {
	 * dataJsonFileName = FileHelper.getFileName(type, appName); return
	 * dataJsonFileName; }
	 */

	// Get config.properties values. User has to set accordingly where the files
	// are kept. appName : Rotating_assets
	public static void initializeConfigData(String appName) {
		// metaJsonFileName =
		// MongoDBContentReportCode.getMetaJsonFileName("meta", appName);
		// dataJsonFileName =
		// MongoDBContentReportCode.getDataJsonFileName("data", appName);
		// logoFileName = FileHelper.getConfigValues("logo");
		hostName = FileHelper.getConfigValues("hostName");
		// dbName = FileHelper.getConfigValues("dbName");
	}

	// Used to get ponnivi database details
	// hostName and dbName are set in config.details. eg : hostName - localhost,
	// dbName - ponnivi
	public static DB getConnectionDetails(String dbName) {
		try {
			mongoClient = new MongoClient(hostName, 27017);
			ponniviDb = mongoClient.getDB(dbName);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			System.out.println("Unknownhost.." + e1.getMessage());
			// e1.printStackTrace();
		}
		return ponniviDb;
	}

	// Read and parse the json data
	private static String getJsonDetails(String filePath) {
		FileReader reader;
		JSONObject jsonObject = null;
		try {
			reader = new FileReader(filePath);
			JSONParser jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static long getCollectionCount(String collectionName, DB instauniteDb) {
		long count = 0;
		if (instauniteDb != null) {
			DBCollection collection = instauniteDb
					.getCollection(collectionName);
			count = collection.getCount();
		}
		return count;

	}

	// Here we have inserted meta and data jsons
	public MongoDBContentReportCode(DB instauniteDb, String collectionName,
			String filePath) {

		DBCollection Collection = instauniteDb.getCollection(collectionName);

		// Get collectionName details and put

		// convert JSON to DBObject directly

		DBObject jsonDbObject = (DBObject) JSON.parse(FileHelper
				.getJsonDetails(filePath));

		Collection.insert(jsonDbObject);

	}

}