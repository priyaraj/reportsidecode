/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
 * Please see the License.txt file for more information.*
 * All Rights Reserved.

 * 
 * <summary> //Header and footer style preparation

 * */
package com.Ponnivi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

//This is the file used to create jar file and uploaded to server

public class CreateEvidencesAndItemsFromDataJson {
	static DB instauniteDb = null;
	static String dbName;
	public static String getJsonDetails(String filePath) {
		FileReader reader;
		JSONObject jsonObject = null;

		try {

			reader = new FileReader(filePath);
			JSONParser jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(reader);
		} catch (FileNotFoundException e) {			
			//e.printStackTrace();
			System.out.println("Filenotfound exception" + e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException exception" + e.getMessage());
		} catch (ParseException e) {
			System.out.println("ParseException exception" + e.getMessage());
		}
		return jsonObject.toString();
	}

	public static void createEvidencesCollectionFromDataJson(DB ponniviDb,
			String collectionName, String filePath) {
		if (ponniviDb != null) {
			DBCollection Collection = ponniviDb.getCollection(collectionName);

			// Get collectionName details and put

			// convert JSON to DBObject directly
			DBObject jsonDbObject = (DBObject) JSON
					.parse(getJsonDetails(filePath));

			Collection.insert(jsonDbObject);
			System.out.println("Inserted into " + collectionName + " ..Done");
		} else {
			System.out.println("Please check your connection settings");
		}
	}

	public static void createItemsCollectionFromDataJson(
			String parentCollectionName, String childCollectionName,
			DB ponniviDb, String evidenceRecordSplitter, String contextType,
			String filePath) {

		String linkName = "Items";
		BasicDBObject items_collection = new BasicDBObject();
		String evidenceIdentifier = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "Evidence_Identifier", "");
		String LogoutTime = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "LogoutTime", "");
		String assetZzz = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "Asset_Zzz", "");
		String assetXxx = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "Asset_Xxx", "");
		String assetYyy = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "Asset_Yyy", "");
		String customerId = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "CustomerId", "");
		String customerDbName = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "CustomerDbName", "");
		String evidenceId = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "Evidence_Id", "");
		String seId = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "SE_Id", "");
		String assetId = getTagValues(parentCollectionName,
				"Evidence_Record", "", ponniviDb, "Asset_Id", "");
		if (ponniviDb != null) {
			DBCollection itemsCollection = ponniviDb
					.getCollection(childCollectionName);
			
			// Get collectionName details and put

			// convert JSON to DBObject directly

			DBObject jsonDbObject = (DBObject) JSON
					.parse(getJsonDetails(filePath));

			try {
				Map dataMap = jsonDbObject.toMap();
				Iterator iterator = dataMap.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
				//	System.out.println("The key is: " + mapEntry.getKey()
					//		+ ",value is :" + mapEntry.getValue());

					BasicDBObject dbObject = (BasicDBObject) jsonDbObject
							.get("Evidence_Record");
					/*
					 * BasicDBObject evidenceRecord = (BasicDBObject) dbObject
					 * .get(evidenceRecordSplitter);
					 */

					com.mongodb.BasicDBList dbListItems = (com.mongodb.BasicDBList) dbObject
							.get(linkName);
					Iterator iter = dbListItems.iterator();
					int srNo = 0;
					Double totalPrice = 0.0;
					while (iter.hasNext()) {
						BasicDBObject obj = (BasicDBObject) iter.next();

						boolean addtoList = false;
						com.mongodb.BasicDBObject specificDetailsList = null;
						com.mongodb.BasicDBObject evidenceQualifierList = null;

						specificDetailsList = (com.mongodb.BasicDBObject) obj
								.get("Specific_Details");
						if (contextType.equalsIgnoreCase("machine_uptime")) {
							items_collection.put("A","null");
							items_collection.put("B","null");
							items_collection.put("C","null");
							items_collection.put("D","null");
						}
						else {  //As of now this is for rotating assets. But needs change when doing for another app -- July 16
						List specificDetailsSetist = new ArrayList(); // As in
																		// specificdetails,
																		// we
																		// will
																		// have
																		// this
																		// 4
																		// tags
																		// --
																		// May
																		// 20
						specificDetailsSetist.add("A");
						specificDetailsSetist.add("B");
						specificDetailsSetist.add("C");
						specificDetailsSetist.add("D");

						List numericVariableList = getNumericVariables(
								"report_param_collection",
								"values_and_conditions", "numeric_variables",
								ponniviDb);
						boolean isNumeric = false;
						
						
						for (int i = 0; i < specificDetailsSetist.size(); i++) {
							if (numericVariableList
									.contains(specificDetailsSetist.get(i))) {
								items_collection
										.put(specificDetailsSetist.get(i)
												.toString(),
												Integer.parseInt(specificDetailsList
														.getString(specificDetailsSetist
																.get(i)
																.toString())));
							} else {
								items_collection
										.put(specificDetailsSetist.get(i)
												.toString(),
												specificDetailsList
														.getString(specificDetailsSetist
																.get(i)
																.toString()));
							}
						}
					}

						evidenceQualifierList = (com.mongodb.BasicDBObject) obj
								.get("Evidence_Qualifiers");
						items_collection.put("Qualifier_1",
								evidenceQualifierList.getString("Qualifier_1"));
						items_collection.put("Qualifier_2",
								evidenceQualifierList.getString("Qualifier_2"));
						items_collection.put("Qualifier_3",
								evidenceQualifierList.getString("Qualifier_3"));
						items_collection.put("Qualifier_4",
								evidenceQualifierList.getString("Qualifier_4"));

						items_collection.put("Qualifier_5",
								evidenceQualifierList.getString("Qualifier_5"));
						items_collection.put("Qualifier_6",
								evidenceQualifierList.getString("Qualifier_6"));
						items_collection.put("Qualifier_7",
								evidenceQualifierList.getString("Qualifier_7"));
						items_collection.put("Qualifier_8",
								evidenceQualifierList.getString("Qualifier_8"));

						items_collection.put("Qualifier_9",
								evidenceQualifierList.getString("Qualifier_9"));
						items_collection
								.put("Qualifier_10", evidenceQualifierList
										.getString("Qualifier_10"));

						com.mongodb.BasicDBObject evidencesList = (com.mongodb.BasicDBObject) obj
								.get("Evidences");
						items_collection.put("Evidence_1",
								evidencesList.getString("Evidence_1"));
						items_collection.put("Evidence_2",
								evidencesList.getString("Evidence_2"));
						items_collection.put("Evidence_3",
								evidencesList.getString("Evidence_3"));
						items_collection.put("Evidence_4",
								evidencesList.getString("Evidence_4"));

						items_collection.put("Evidence_5",
								evidencesList.getString("Evidence_5"));
						items_collection.put("Evidence_6",
								evidencesList.getString("Evidence_6"));

						items_collection.put("Evidence_7",
								evidencesList.getString("Evidence_7"));
						items_collection.put("Evidence_8",
								evidencesList.getString("Evidence_8"));
						items_collection.put("Evidence_9",
								evidencesList.getString("Evidence_9"));
						items_collection.put("Evidence_10",
								evidencesList.getString("Evidence_10"));

						items_collection.put("Context_Reference_Example",
								contextType);
						
						//Add Asset_XXX,YYY,ZZZ
						items_collection.put("Asset_Xxx",
								assetXxx);
						items_collection.put("Asset_Yyy",
								assetYyy);
						items_collection.put("Asset_Zzz",
								assetZzz);
						
						//Add others
						items_collection.put("CustomerId",
								customerId);
						items_collection.put("CustomerDbName",
								customerDbName);
						items_collection.put("Evidence_Id",
								evidenceId);
						items_collection.put("SE_Id",
								seId);
						items_collection.put("Asset_Id",
								assetId);

						DateFormat dateFormat = new SimpleDateFormat(
								"dd-MM-yy HH:mm:ss");
						Date logout = dateFormat.parse(LogoutTime);
						items_collection.put("LogoutTime", logout);

						items_collection.put("Evidence_Identifier",
								evidenceIdentifier);

						itemsCollection.insert(items_collection);
						System.out.println("Inserted into "
								+ childCollectionName + " ..Done");

					}

				}
			} catch (Exception e) {
				System.out.println("Exception thrown while creating itemcollection for.." + filePath + " and error is " + e.getMessage());
			} finally {
			}

		}

	}

	public static List getNumericVariables(String collectionName,
			String spliiter, String paramName, DB ponniviDb) {
		String connectorLink = "";

		List<String> optionsList = new ArrayList<String>();
		if (ponniviDb != null) {
			DBCollection optionsArrayCollection = ponniviDb
					.getCollection(collectionName);

			DBCursor optionsArrayCursor = optionsArrayCollection.find();

			BasicDBObject optionsArrayDbObject = (BasicDBObject) optionsArrayCursor
					.next();

			try {
				while (optionsArrayDbObject != null) {

					BasicDBObject valuesOrConditionsRecord = (BasicDBObject) optionsArrayDbObject
							.get(spliiter);
					com.mongodb.BasicDBList numericListItems = null;
					numericListItems = (com.mongodb.BasicDBList) valuesOrConditionsRecord
							.get(paramName);

					Iterator iter = numericListItems.iterator();
					while (iter.hasNext()) {
						BasicDBObject obj = (BasicDBObject) iter.next();
						// System.out
						// .println("string.." + obj.getString("Option"));
						optionsList.add(obj.getString("Option"));
					}

					break;
				}
				optionsArrayDbObject = (BasicDBObject) (optionsArrayCursor
						.hasNext() ? optionsArrayCursor.next() : null);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				optionsArrayCursor.close();
			}
		}
		return optionsList;
	}

	public static String getTagValues(String collectionName, String spliiter,
			String contextReferenceExample, DB ponniviDb, String paramName,
			String reportName) {

		String tagValue = "";
		String contextReference = "";
		if (collectionName.equalsIgnoreCase("report_param_collection")) {
			paramName = paramName + "_type";
		}

		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);

			DBCursor cursor = collection.find();

			BasicDBObject dbObject = (BasicDBObject) cursor.next();

			try {
				while (dbObject != null) {

					BasicDBObject record = (BasicDBObject) dbObject
							.get(spliiter);
					if (collectionName
							.equalsIgnoreCase("evidence_description_collection")) {
						tagValue = record.getString(contextReferenceExample);
						if (tagValue.equalsIgnoreCase(reportName)) {
							tagValue = record.getString(paramName);
						}
					} else if (collectionName
							.equalsIgnoreCase("evidences_collection")
							|| collectionName
									.equalsIgnoreCase("equipments_evidences_collection")) {
						com.mongodb.BasicDBObject dbTimeObject = (com.mongodb.BasicDBObject) record
								.get("Time_Location_Details");
						if (paramName.equalsIgnoreCase("Evidence_Identifier")) {							
							tagValue = dbTimeObject
									.getString("Evidence_Identifier");
						} else if (paramName.equalsIgnoreCase("LogoutTime")) {							
							tagValue = dbTimeObject.getString("LogoutTime");
						}
						else if (paramName.equalsIgnoreCase("LoginTime")) {							
							tagValue = dbTimeObject.getString("LoginTime");
						}
						else if (paramName.equalsIgnoreCase("Evidence_Location")) {							
							tagValue = dbTimeObject.getString("Evidence_Location");
						}

						else { // Need to get other items like above if required
								// -- June 2
							tagValue = record.getString(paramName);
						}
					}
					dbObject = (BasicDBObject) (cursor.hasNext() ? cursor
							.next() : null);
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				cursor.close();
			}

		}
		return tagValue;
	}
	
	//Initialize dbName and check whether report_param_collection available in that db
	public static void checkPreConditionsForDbAndOptionsArray(String fileName) {
		dbName = FileHelper.getDbNameFromJson(fileName);
		instauniteDb = MongoDBContentReportCode
				.getConnectionDetails(dbName);
		long collectionCount = MongoDBContentReportCode.getCollectionCount("report_param_collection", instauniteDb);
		if (collectionCount == 0) {
		String optionsArrayJsonFileName = FileHelper.getJsonFileNames("options_array");
		MongoDBContentReportCode optionsArrayDAO = new MongoDBContentReportCode(instauniteDb,"report_param_collection",optionsArrayJsonFileName);
		}
	}

	public static void main(String[] args) {
		MongoDBContentReportCode.initializeConfigData("Rotating_assets");
		
		String pathToScan = "";
		String osName = System.getProperty("os.name");
		if (osName.startsWith("Windows")) {
			//pathToScan = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\";
			pathToScan = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\";
		} else {
			pathToScan = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/";
		}
		String target_file; // fileThatYouWantToFilter
		File folderToScan = new File(pathToScan);

		File[] listOfFiles = folderToScan.listFiles();
		boolean found = true;
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				target_file = listOfFiles[i].getName();
				System.out.println("target_file which is getting moved.." + target_file);
				String parentCollectionName = "", childCollectionName = "";
				

				if (target_file
						.startsWith("datajson_rotatingassets_equipments")
						&& target_file.endsWith(".json")) {
					checkPreConditionsForDbAndOptionsArray(pathToScan
							+ target_file);
					parentCollectionName = "equipments_evidences_collection";
					childCollectionName = "items_equipments_collection";
					found = false; // To bypass below else if
				} else if ( ( target_file.startsWith("datajson_rotatingassets") || target_file.startsWith("datajson"))
						&& target_file.endsWith(".json") && (found)) {
					System.out.println("checking for machineuptime json..");
					checkPreConditionsForDbAndOptionsArray(pathToScan
							+ target_file);
					parentCollectionName = "evidences_collection";
					childCollectionName = "items_collection";
				}

				if (!parentCollectionName.equalsIgnoreCase("")
						&& !childCollectionName.equalsIgnoreCase("")) {
					CreateEvidencesAndItemsFromDataJson
							.createEvidencesCollectionFromDataJson(
									instauniteDb, parentCollectionName,
									pathToScan + target_file);
					
					String contextType = getTagValues(parentCollectionName,
							"Evidence_Record", "", instauniteDb,
							"Context_Type", "");
					
					CreateEvidencesAndItemsFromDataJson
							.createItemsCollectionFromDataJson(
									parentCollectionName, childCollectionName,
									instauniteDb, "Evidence_Record",
									contextType, pathToScan + target_file);
				
				}
			}
		}
	}

}
