/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014, 2015  Ponnivi Internet Systems Pvt. Ltd.,
 * Please see the License.txt file for more information.*
 * All Rights Reserved.

 * 
 * <summary> //Read the config.properties file and get the contents

 * */
package com.Ponnivi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//Filehelper class to get metajson and datajson filenames from //config.properties folder. Locations has to be configured //according to the user storage location
public class FileHelper {

	public static String getFileName(String type, String appName) {

		Properties prop = new Properties();
		String fileName = "";

		try {

			InputStream inputStream = FileHelper.class
					.getResourceAsStream("/com/Ponnivi/Resources/config.properties");
			// getClass().getClassLoader().getResourceAsStream("/com/Ponnivi/Resources/config.properties");

			prop.load(inputStream);
			if (appName.equals("Rotating_assets")) {
				if (type.equalsIgnoreCase("meta"))
					fileName = prop
							.getProperty("metajson.rotatingassets.filename");
				else if (type.equalsIgnoreCase("data"))
					fileName = prop
							.getProperty("datajson.rotatingassets.filename");
			}
			if (appName.equals("machine_uptime")) {
				if (type.equalsIgnoreCase("meta"))
					fileName = prop
							.getProperty("metajson.machineuptime.filename");
				else if (type.equalsIgnoreCase("data"))
					fileName = prop
							.getProperty("datajson.machineuptime.filename");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		File absolute = new File(fileName);

		System.out.println("fileName.." + absolute.getAbsolutePath());
		return absolute.getAbsolutePath();

	}
	
	public static String getJsonFileNames(String type) {
		String jsonFileName = "";
		String userOperatingSystem = System.getProperty("os.name");
		if (type.equalsIgnoreCase("meta")) {
			if (userOperatingSystem.startsWith("Windows")) {
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\metajson_rotatingassets.json";	
			}
			else {
			jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/metajson_rotatingassets.json";
			}			
		}
		if (type.equalsIgnoreCase("data")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\datajson_rotatingassets.json";
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\datajson_rotatingassets.json";				
			}
			else {
			jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/datajson_rotatingassets.json";
			}
		}
		if (type.equalsIgnoreCase("login")) {
			if (userOperatingSystem.startsWith("Windows")) {
				jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\userdetails.json";
			}
			else {
			jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/userdetails.json";
			}
		}
		if (type.equalsIgnoreCase("options_array")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\options_array.json";
				jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\options_array.json";
			}
			else {
				jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/options_array.json";
			}
		}
		return jsonFileName;		
	}

	public static String getConfigValues(String type) {

		Properties prop = new Properties();
		String configValue = "";
		
		InputStream inputStream = FileHelper.class
				.getResourceAsStream("/com/Ponnivi/Resources/config.properties");
		// getClass().getClassLoader().getResourceAsStream("/com/Ponnivi/Resources/config.properties");

		try {
			prop.load(inputStream);


		if (type.equalsIgnoreCase("logo"))
			configValue = prop.getProperty("logo.filename");
		else if (type.equalsIgnoreCase("hostname"))
			configValue = prop.getProperty("hostname");
		else if (type.equalsIgnoreCase("dbname"))
			configValue = prop.getProperty("dbname");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return configValue;

	}
	
	public static String getDbNameFromJson(String JsonFileName) {
		org.json.JSONObject obj;
		String dbName = null;
		try {
			obj = new org.json.JSONObject(getJsonDetails(JsonFileName));
			obj = obj.getJSONObject("Evidence_Record");
			dbName = obj.getString("CustomerDbName");	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return dbName;

}
	
	//Read and parse the json data
		public static String getJsonDetails(String filePath) {
			FileReader reader;
			JSONObject jsonObject = null;
		
			try {
				
				reader = new FileReader(filePath);	 	 
	         JSONParser jsonParser = new JSONParser();
	         jsonObject = (JSONObject) jsonParser.parse(reader);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         return jsonObject.toString();
		}

}